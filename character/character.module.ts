import { ReliableAlarmModule } from '@app/reliable-alarm';
import { forwardRef, Module } from '@nestjs/common';
import { EnvironmentModule } from '../../globals/environment/environment.module';
import { StakingWhitelistService } from '../../staking-whitelist/staking-whitelist.service';
import { UserLogicModule } from '../../user/logics/user-logic.module';
import { CharacterController } from './character.controller';
import { CharacterService } from './character.service';

@Module({
  imports: [
    ReliableAlarmModule,
    forwardRef(() => UserLogicModule),
    EnvironmentModule,
  ],
  controllers: [CharacterController],
  providers: [CharacterService, StakingWhitelistService],
  exports: [CharacterService],
})
export class CharacterModule {}
