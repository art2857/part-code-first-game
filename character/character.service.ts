import { ReliableAlarmService } from '@app/reliable-alarm';
import {
  BadRequestException,
  ConflictException,
  ForbiddenException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
  OnApplicationBootstrap,
} from '@nestjs/common';
import {
  prepareCurrencies,
  moreCurrencies,
  multiCurrencies,
} from '../../../../../libs/currencies';
import { getRandomArrayItem } from '../../../../../libs/helpers';
import { isValidObjectId } from 'mongoose';
import { BuffsModel } from '../../buffs/entities/global-buffs.entity';
import { BuffsEnum } from '../../buffs/global-buffs.constants';
import { EnvironmentEntity } from '../../globals/environment/environment.entity';
import { EnvironmentService } from '../../globals/environment/environment.service';
import { UserLogicService } from '../../user/logics/user-logic.service';
import { AttackTypeEnum } from '../battle/constants/attack-type.enum';
import { Abilities } from './constants/ability/ability';
import { AbilityEnum } from './constants/ability/ability.enum';
import { ArchetypeEnum } from './constants/character/archetype.enum';
import { ClansEnum } from './constants/clans/clans.enum';
import { WeaponEnum } from './constants/weapons/weapon.enum';
import {
  IWeaponState,
  IWeaponStateAttack,
} from './constants/weapons/weapon.types';
import { CharacterModel } from './entities/character.entity';
import { StakingWhitelistService } from '../../staking-whitelist/staking-whitelist.service';
import { STAKING_LEVEL_CAP } from '../../staking-whitelist/staking-whitelist.constant';

@Injectable()
export class CharacterService implements OnApplicationBootstrap {
  env: EnvironmentEntity;
  constructor(
    @Inject('BuffsModel')
    private buffsModel: typeof BuffsModel,
    @Inject('CharacterModel')
    private characterModel: typeof CharacterModel,
    private readonly reliableAlarm: ReliableAlarmService,
    @Inject(forwardRef(() => UserLogicService))
    private readonly userService: UserLogicService,
    private readonly environmentService: EnvironmentService,
    private readonly stakingWhitelistService: StakingWhitelistService,
  ) {
    this.env = this.environmentService.environment;
  }

  init() {
    this.reliableAlarm.reliableAlarmLoad('character', this);
  }

  async onApplicationBootstrap() {
  }

  async getStateCharacterBattle(
    archetype: ArchetypeEnum,
    clan: ClansEnum,
    level: number,
    wallet?: string,
  ) {
    if (
      archetype == ArchetypeEnum.Turel ||
      archetype == ArchetypeEnum.Lapetus ||
      archetype == ArchetypeEnum.ArmoredShibaInu
    ) {
      clan = ClansEnum.Empty;
    }
    const clanState = this.env.Clans[clan];

    // Stats weapon
    const weapon = this.env.ArchetypeWeapon[archetype];

    const weaponStats: IWeaponState = {
      [AttackTypeEnum.BaseAttack]: {
        accuracy: 1,
        count: 1,
        modAttack: 1,
        energy: 1,
        reload: 0,
      },
    };

    if (weapon) {
      const acrhetypeWeapon: WeaponEnum = this.env.ArchetypeWeapon[archetype];
      const weaponsStats: IWeaponState = this.env.WeaponsStats[acrhetypeWeapon];

      const weaponsStatsBaseAttack = weaponsStats[AttackTypeEnum.BaseAttack];
      const weaponsStatsHeavyAttack = weaponsStats[AttackTypeEnum.HeavyAttack];

      const characterSetWeaponStats = (
        typeAttack: AttackTypeEnum,
        source: IWeaponStateAttack,
      ) => {
        if (source) {
          const clanAccuracy = clanState.accuracy ? clanState.accuracy : 0;

          weaponStats[typeAttack] = {
            modAttack: source.modAttack,
            accuracy: source.accuracy + clanAccuracy,
            count: source.count,
            energy: source.energy,
            reload: source.reload,
            buffs: source.buffs,
          };
        }
      };

      characterSetWeaponStats(
        AttackTypeEnum.BaseAttack,
        weaponsStatsBaseAttack,
      );
      characterSetWeaponStats(
        AttackTypeEnum.HeavyAttack,
        weaponsStatsHeavyAttack,
      );
    }

    // Stats character
    const baseHealth = this.env.BaseHealth[level];
    let baseAttack = this.env.BaseAttack[level];

    const archetypeMod = this.env.ArchetypeMod[archetype]
      ? this.env.ArchetypeMod[archetype]
      : { pet: true, modHealth: 1, modArmor: 0 };

    const clanHealth = clanState.health ? clanState.health : 0;
    const clanArmor = clanState.armor ? clanState.armor : 0;
    const clanAttack = clanState.attack ? clanState.attack : 0;

    baseAttack += clanAttack;

    if (wallet) {
      const buffs = await this.buffsModel.find({ userWallet: wallet });
      for (const buff of buffs) {
        if (buff.name == BuffsEnum.SquadsAttack) {
          baseAttack += buff.value;
        }
      }
    }

    return {
      pet: archetypeMod.pet,
      health: (baseHealth + clanHealth) * archetypeMod.modHealth,
      armor: (baseHealth + clanArmor) * archetypeMod.modArmor,

      modHealth: archetypeMod.modHealth,
      modArmor: archetypeMod.modArmor,

      shield: 0,

      attack: baseAttack,

      weapon,
      weaponStats,

      energy: this.env.BattleAttackerEnergy,
      maxEnergy: this.env.BattleMaxEnergy,
    };
  }

  async create(
    userWallet: string,
    archetype: ArchetypeEnum,
    level?: number,
    clan?: ClansEnum,
    abilities: { name: AbilityEnum; level: number }[] = [],
  ) {
    const character = new this.characterModel();
    character.userWallet = userWallet;
    character.archetype = archetype;

    if (level) {
      character.level = level;
    } else {
      character.level = 1;
      level = 1;
    }

    character.characterMap = {
      energy: this.env.MapMaxEnergy,
    };

    if (!clan) {
      clan = getRandomArrayItem([
        ClansEnum.Alium,
        ClansEnum.HyperJump,
        ClansEnum.Liquidifty,
        ClansEnum.NFTb,
        ClansEnum.UltiArena,
      ]);
    }

    character.clan = clan;

    const characterBattle = await this.getStateCharacterBattle(
      archetype,
      clan,
      character.level,
      character.userWallet,
    );

    const characterAbility = character.abilities;

    const archetypeAbility = this.env.ArchetypeAbility[character.archetype];

    if (archetypeAbility) {
      const convert = (
        ability: (AbilityEnum | AbilityEnum[])[],
        passive: boolean,
        pet: boolean,
      ) => {
        ability.forEach((abilityName) => {
          if (!Array.isArray(abilityName)) {
            abilityName = [abilityName];
          }

          characterAbility.push({
            branches: abilityName.map((abilityName) => {
              const needLevel = this.env.Abilities[abilityName].needLevel;

              const a = abilities.find((a) => {
                return a.name == abilityName;
              });

              let level =
                a?.level != undefined ? a.level : passive || pet ? 1 : 0;

              if (level < 0) {
                level = 0;
              }
              if (level > this.env.Abilities[abilityName].leveling.length) {
                level = this.env.Abilities[abilityName].leveling.length;
              }

              return {
                name: abilityName,
                level,
                needLevel,
                passive,
              };
            }),
          });
        });
      };

      const passiveAbility = archetypeAbility.passive;
      const activeAbility = archetypeAbility.active;

      if (passiveAbility) {
        convert(passiveAbility, true, characterBattle.pet);
      }
      if (activeAbility) {
        convert(activeAbility, false, characterBattle.pet);
      }
    }

    const characterCreated = await this.characterModel.create(character);

    return characterCreated;
  }

  async addExp(characterId: string, exp: number) {
    if (!isValidObjectId(characterId))
      throw new BadRequestException(`Invalid characterId`);

    return this.characterModel.findOneAndUpdate(
      { _id: characterId },
      { $inc: { exp: exp } },
    );
  }

  async upLevel(characterId: string, wallet?: string) {
    if (!isValidObjectId(characterId))
      throw new BadRequestException(`Invalid characterId`);

    const filter: { _id: string; userWallet?: string } = { _id: characterId };
    if (wallet) {
      filter.userWallet = wallet;
    }

    const character = await this.characterModel.findOne(filter);

    if (!character)
      throw new NotFoundException(`Not found character ${characterId}`);

    if (character.ownerId)
      throw new NotFoundException(`Pet cannot level up`);

    if (character.level == Object.keys(this.env.LevelUpCost).length || character.level >= 5 )
      throw new ConflictException('Character max level');

    const levelUpCost = this.env.LevelUpCost[character.level + 1];

    const user = await this.userService.getUser(wallet);
    if (!user) throw new ForbiddenException('No such user exists');

    if (character.exp < levelUpCost.exp)
      throw new ConflictException('Lack of experience');

    const cost = prepareCurrencies(levelUpCost.currencies);
    const userCurrencies = prepareCurrencies(user.currencies);

    // Хватает ли ресурсов для постройки
    if (!moreCurrencies(userCurrencies, cost))
      throw new BadRequestException('Not enough resources');

    character.level++;

    if (this.env.LevelPoints.includes(character.level)) {
      character.points++;
    }

    await this.characterModel.updateOne({ _id: character._id }, character); //await character.save();
    if (character.level === STAKING_LEVEL_CAP)
      await this.stakingWhitelistService.addToWhitelist(wallet);

    // Снятие средств с пользователя
    await this.userService.changeCurrencies(wallet, multiCurrencies(cost, -1));

    return { level: character.level, point: character.points, cost };
  }

  getClans() {
    return this.env.Clans;
  }

  async upAbility({
    abilityName,
    wallet,
    characterId,
  }: {
    abilityName: AbilityEnum;
    wallet: string;
    characterId: string;
  }) {
    if (!isValidObjectId(characterId))
      throw new BadRequestException(
        `Error value characterId ${characterId} as _id`,
      );

    const character = await this.characterModel.findOne({
      _id: characterId,
      userWallet: wallet,
    });

    if (!character) throw new NotFoundException(`Character not found`);

    let abilityIndex: number;
    let abilityBranchIndex: number;

    const characterAbilities = character.abilities;
    characterAbilities.forEach((ability, index) => {
      ability.branches.forEach((ability, subindex) => {
        if (ability.name == abilityName) {
          abilityIndex = index;
          abilityBranchIndex = subindex;
        }
      });
    });

    const characterBranchAbilities = character.abilities[abilityIndex].branches;

    let findAbilityIndex: number;

    characterBranchAbilities.find((ability, index) => {
      if (ability.level > 0) {
        findAbilityIndex = index;
        return true;
      }
    });

    if (findAbilityIndex != undefined) {
      if (findAbilityIndex != abilityBranchIndex)
        throw new ConflictException(
          `abilityBranchIndex ${abilityBranchIndex} does not match abilityIndex ${findAbilityIndex}`,
        );
    }

    const characterAbility = characterBranchAbilities[abilityBranchIndex];

    if (!characterAbility)
      throw new NotFoundException(
        `Indexed ${abilityIndex} Indexed Branch ${abilityBranchIndex} ability does not exists`,
      );


    if (!characterAbility)
      throw new NotFoundException(
        `Indexed ${abilityIndex} ability does not exists`,
      );

    if (character.level < characterAbility.needLevel)
      throw new ConflictException('Character level too low');

    const cost = this.env.ArchetypeAbilityCost[characterAbility.needLevel];

    const user = await this.userService.getUser(wallet);

    const userCurrencies = prepareCurrencies(user.currencies);

    // Хватает ли ресурсов для постройки
    if (!moreCurrencies(userCurrencies, cost ? cost : {}))
      throw new BadRequestException('Not enough resources');

    const ability = Abilities[characterAbility.name];
    //console.log(characterAbility.level, ability.leveling.length);

    if (characterAbility.level == ability.leveling.length)
      throw new ConflictException('Max level ability');

    if (character.points <= 0)
      throw new ConflictException('Not enough development points');

    characterAbility.level++;

    character.points--;

    // Снятие средств с пользователя
    await this.userService.changeCurrencies(wallet, multiCurrencies(cost, -1));

    await this.characterModel.updateOne(
      { _id: characterId },
      { $set: { points: character.points, abilities: character.abilities } },
    );

    return true;
  }

  async get({
    wallet,
    skip,
    limit,
    _id,
  }: {
    wallet?: string;
    skip?: number;
    limit?: number;
    _id?: string;
  }) {
    if (skip == undefined) skip = 0;
    if (limit == undefined) limit = 25;

    if (_id) {
      if (!isValidObjectId(_id)) throw new BadRequestException(`Invalid _id`);

      const character = await this.characterModel.findOne({
        _id,
        userWallet: wallet,
      });

      return Object.assign(character.toObject(), {
        characterBattle: await this.getStateCharacterBattle(
          character.archetype,
          character.clan,
          character.level,
          character.userWallet,
        ),
      });
    }

    if (wallet) {
      const characters = await this.characterModel
        .find({ userWallet: wallet })
        .skip(skip)
        .limit(limit)
        .exec();

      return Promise.all(
        characters.map(async (character) => {
          return Object.assign(character.toObject(), {
            characterBattle: await this.getStateCharacterBattle(
              character.archetype,
              character.clan,
              character.level,
              character.userWallet,
            ),
          });
        }),
      );
    }
    const characters = await this.characterModel
      .find({})
      .skip(skip)
      .limit(limit)
      .exec();

    return Promise.all(
      characters.map(async (character) => {
        return Object.assign(character.toObject(), {
          characterBattle: await this.getStateCharacterBattle(
            character.archetype,
            character.clan,
            character.level,
            character.userWallet,
          ),
        });
      }),
    );
  }
}
