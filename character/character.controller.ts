import {
  applyDecorators,
  Body,
  Controller,
  Get,
  Post,
  Query,
} from '@nestjs/common';
import { ApiBody, ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { AuthBusy, Auth } from '../../../../auth/src/logics/auth.decorator';
import { User } from '../../../../auth/src/logics/user.decorator';
import { UpAbilityDTO, UpLevelDTO } from './character.dto';
import { CharacterService } from './character.service';

@ApiTags('character')
@Controller('character')
export class CharacterController {
  constructor(private readonly characterService: CharacterService) {}

  @applyDecorators(
    ApiOperation({ summary: 'Возвращает всех персонажей' }),
    ApiQuery({ required: false, name: 'skip', type: Number }),
    ApiQuery({ required: false, name: 'limit', type: Number }),
    ApiQuery({ required: false, name: 'id', type: String }),
  )
  @Get('/get')
  @AuthBusy()
  async get(
    @User('wallet') wallet: string,
    @Query('skip') skip: string,
    @Query('limit') limit: string,
    @Query('id') _id: string,
  ) {
    if (!skip) skip = '0';
    if (!limit) limit = '25';

    return {
      characters: await this.characterService.get({
        wallet,
        skip: Number(skip),
        limit: Number(limit),
        _id,
      }),
    };
  }

  @applyDecorators(ApiOperation({ summary: 'Возвращает информацию о кланах' }))
  @Get('/getClans')
  @Auth()
  async getClans() {
    return this.characterService.getClans();
  }

  @applyDecorators(
    ApiOperation({ summary: 'Повысить уровень' }),
    ApiBody({ type: UpLevelDTO }),
  )
  @Post('upLevel')
  @AuthBusy()
  async upLevel(
    @User('wallet') wallet: string,
    @Body() { characterId }: UpLevelDTO,
  ) {
    return this.characterService.upLevel(characterId, wallet);
  }

  @applyDecorators(
    ApiOperation({ summary: 'Повысить уровень способности' }),
    ApiBody({ type: UpAbilityDTO }),
  )
  @Post('upAbility')
  @AuthBusy()
  async upAbility(
    @User('wallet') wallet: string,
    @Body()
    {
      characterId,
      abilityName,
    }: UpAbilityDTO,
  ) {
    return this.characterService.upAbility({
      characterId,
      wallet,
      abilityName,
    });
  }
}
