// weapons
export enum WeaponEnum {
  // Персонажи
  CyberRod = 'CyberRod',
  GaussMusket = 'GaussMusket',
  HeavyAssaultRifle = 'HeavyAssaultRifle',
  MultipurposeRifleStaff = 'MultipurposeRifleStaff',
  PowerGauntlets = 'PowerGauntlets',
  PowerKatana = 'PowerKatana',
  ShortPowerSwordAndWallshield = 'ShortPowerSwordAndWallshield',
  SwordAndPistol = 'SwordAndPistol',
  TechStaff = 'TechStaff',
  TheLaptome = 'TheLaptome',

  // Питомцы
  Nyanko = 'Nyanko',
  Lapetus = 'Lapetus',
  ClockworkParrot = 'ClockworkParrot',
  CyberneticSpirit = 'CyberneticSpirit',
  MultipurposeCompanionDrone = 'MultipurposeCompanionDrone',
  ArmoredShibaInu = 'ArmoredShibaInu',
  MechanicalOctopus = 'MechanicalOctopus',
  MehanicalAngel = 'MehanicalAngel',
  StreetCat = 'StreetCat',
  CyberneticBat = 'CyberneticBat',

  // Нейтральные
  Turel = 'Turel',
}
