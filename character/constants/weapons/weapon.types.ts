import { AttackTypeEnum } from '../../../battle/constants/attack-type.enum';
import { RuleEnum } from '../ability/ability.types';

export type IWeaponStateAttack = {
  modAttack: number;
  accuracy: number;
  count: number;
  energy: number;
  reload: number;

  buffs?: RuleEnum[];
};

export interface IWeaponState {
  [AttackTypeEnum.BaseAttack]: IWeaponStateAttack;
  [AttackTypeEnum.HeavyAttack]?: IWeaponStateAttack;
}

export type IBattleWeaponStateAttack = IWeaponStateAttack & {
  remaining: number;
};

export interface IBattleWeaponState {
  [AttackTypeEnum.BaseAttack]: IBattleWeaponStateAttack;
  [AttackTypeEnum.HeavyAttack]?: IBattleWeaponStateAttack;
}

