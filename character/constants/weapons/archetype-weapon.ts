import { ArchetypeEnum } from '../character/archetype.enum';
import { WeaponEnum } from './weapon.enum';

export const ArchetypeWeapon: Partial<Record<ArchetypeEnum, WeaponEnum>> = {
  [ArchetypeEnum.Mercenaries]: WeaponEnum.HeavyAssaultRifle,
  [ArchetypeEnum.CyberSamurai]: WeaponEnum.PowerKatana,

  [ArchetypeEnum.Lapetus]: WeaponEnum.Lapetus,
  [ArchetypeEnum.ArmoredShibaInu]: WeaponEnum.ArmoredShibaInu,

  [ArchetypeEnum.Turel]: WeaponEnum.Turel,
};
