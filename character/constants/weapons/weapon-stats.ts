import { AttackTypeEnum } from '../../../battle/constants/attack-type.enum';
import { RuleEnum } from '../ability/ability.types';
import { WeaponEnum } from './weapon.enum';
import { IWeaponState } from './weapon.types';

export const WeaponsStats: Partial<Record<WeaponEnum, IWeaponState>> = {
  // Персонажи
  [WeaponEnum.HeavyAssaultRifle]: {
    [AttackTypeEnum.BaseAttack]: {
      modAttack: 0.8,
      accuracy: 0.5,
      count: 5,
      energy: 2,
      reload: 0,
    },
    [AttackTypeEnum.HeavyAttack]: {
      modAttack: 0.8,
      accuracy: 0.5,
      count: 5,
      energy: 3,
      reload: 1,
      buffs: [RuleEnum.MercenaryHeavyAttack],
    },
  },
  [WeaponEnum.PowerKatana]: {
    [AttackTypeEnum.BaseAttack]: {
      modAttack: 1.25,
      accuracy: 0.8,
      count: 2,
      energy: 2,
      reload: 0,
    },
    [AttackTypeEnum.HeavyAttack]: {
      modAttack: 2.5,
      accuracy: 1,
      count: 1,
      energy: 3,
      reload: 1,
    },
  },

  // Нейтральные
  [WeaponEnum.Turel]: {
    [AttackTypeEnum.BaseAttack]: {
      modAttack: 2.5,
      accuracy: 0.8,
      count: 1,
      energy: 2,
      reload: 0,
      // buffs: () => {},
    },
  },

  // Питомцы
  [WeaponEnum.Lapetus]: {
    [AttackTypeEnum.BaseAttack]: {
      modAttack: 1.25,
      accuracy: 0.8,
      count: 1,
      energy: 2,
      reload: 0,
    },
  },

  [WeaponEnum.ArmoredShibaInu]: {
    [AttackTypeEnum.BaseAttack]: {
      modAttack: 1.25,
      accuracy: 0.8,
      count: 1,
      energy: 2,
      reload: 0,
    },
  },
};
