export enum ClansEnum {
  Empty = 'Empty',
  Liquidifty = 'Liquidifty',
  HyperJump = 'HyperJump',
  NFTb = 'NFTb',
  UltiArena = 'UltiArena',
  Alium = 'Alium',
}
