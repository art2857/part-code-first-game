import { ClansEnum } from './clans.enum';

export const Clans: Partial<
  Record<
    ClansEnum,
    { armor?: number; health?: number; attack?: number; accuracy?: number }
  >
> = {
  [ClansEnum.Empty]: {},
  [ClansEnum.Liquidifty]: { accuracy: 0.1 },
  [ClansEnum.HyperJump]: { accuracy: 0.2 },
  [ClansEnum.NFTb]: { armor: 10 },
  [ClansEnum.UltiArena]: { armor: 20, health: -25 },
  [ClansEnum.Alium]: { attack: 5 },
};
