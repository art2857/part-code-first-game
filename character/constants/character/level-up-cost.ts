import { Currencies } from '../../../../user/entities/user.constant';

export const LevelUpCost: { exp: number; currencies: Currencies }[] = [];

LevelUpCost[1] = { exp: 0, currencies: {} };
LevelUpCost[2] = { exp: 20, currencies: { cybr: 40 } };
LevelUpCost[3] = { exp: 60, currencies: { cybr: 160 } };
LevelUpCost[4] = { exp: 110, currencies: { cybr: 250 } };
LevelUpCost[5] = { exp: 170, currencies: { cybr: 360 } };
LevelUpCost[6] = { exp: 240, currencies: { cybr: 490 } };
LevelUpCost[7] = { exp: 320, currencies: { cybr: 640 } };
LevelUpCost[8] = { exp: 410, currencies: { cybr: 810 } };
LevelUpCost[9] = { exp: 510, currencies: { cybr: 1000 } };
LevelUpCost[10] = { exp: 620, currencies: { cybr: 1210 } };
LevelUpCost[11] = { exp: 740, currencies: { cybr: 1440 } };
LevelUpCost[12] = { exp: 860, currencies: { cybr: 1440 } };
LevelUpCost[13] = { exp: 990, currencies: { cybr: 1690 } };
LevelUpCost[14] = { exp: 1130, currencies: { cybr: 1960 } };
LevelUpCost[15] = { exp: 1270, currencies: { cybr: 1960 } };
LevelUpCost[16] = { exp: 1420, currencies: { cybr: 2250 } };
LevelUpCost[17] = { exp: 1580, currencies: { cybr: 2560 } };
LevelUpCost[18] = { exp: 1740, currencies: { cybr: 2560 } };
LevelUpCost[19] = { exp: 1910, currencies: { cybr: 2890 } };
LevelUpCost[20] = { exp: 2080, currencies: { cybr: 2890 } };
