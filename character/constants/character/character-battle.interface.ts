import { IWeaponState } from '../weapons/weapon.types';

export interface ICharacterBattle {
  pet: boolean; // Можно ли атаковать противника, если true - то нельзя

  health: number;
  armor: number;

  modHealth: number;
  modArmor: number;

  shield: number;

  attack: number;

  weapon: string;
  weaponStats: IWeaponState;

  energy: number;
}
