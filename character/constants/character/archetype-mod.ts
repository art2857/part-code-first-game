import { ArchetypeEnum } from './archetype.enum';

export const ArchetypeMod: Partial<
  Record<ArchetypeEnum, { modHealth: number; modArmor: number; pet: boolean }>
> = {
  [ArchetypeEnum.Mercenaries]: {
    modHealth: 0.7,
    modArmor: 1 / 7.5,
    pet: false,
  },
  [ArchetypeEnum.CyberSamurai]: {
    modHealth: 0.75,
    modArmor: 1 / 9,
    pet: false,
  },

  [ArchetypeEnum.Turel]: { modHealth: 1, modArmor: 0, pet: false },
};
