import { prop } from '@typegoose/typegoose';

export class CharacterMap {
  @prop({ required: true, type: Number })
  energy: number;
}
