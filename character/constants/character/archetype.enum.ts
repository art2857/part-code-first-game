export enum ArchetypeEnum {
  // Персонажи
  Mercenaries = 'Mercenaries',
  CyberSamurai = 'CyberSamurai',

  // Питомцы
  Nyanko = 'Nyanko',
  Lapetus = 'Lapetus',
  ClockworkParrot = 'ClockworkParrot',
  CyberneticSpirit = 'CyberneticSpirit',
  MultipurposeCompanionDrone = 'MultipurposeCompanionDrone',
  ArmoredShibaInu = 'ArmoredShibaInu',
  MechanicalOctopus = 'MechanicalOctopus',
  MehanicalAngel = 'MehanicalAngel',
  StreetCat = 'StreetCat',
  CyberneticBat = 'CyberneticBat',

  // Нейтральные
  Turel = 'Turel',
}
