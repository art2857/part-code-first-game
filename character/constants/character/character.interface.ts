import { ICharacterBattle } from './character-battle.interface';
import { CharacterMap } from './character-map.interface';

export interface ICharacter {
  _id?: string;
  characterMap: CharacterMap;
  characterBattle: ICharacterBattle;
}
