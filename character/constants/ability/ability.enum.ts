// Ability

export enum AbilityEnum {
  MercenaryPassive = 'MercenaryPassive',
  Marksman = 'Marksman',
  MercenaryVersatility = 'Versatility',
  MercenaryPiercingShells = 'PiercingShells',

  CyberSamuraiPassive = 'CyberSamuraiPassive',
  CyberSamuraiBattleStance = 'BattleStance',
  CyberSamuraiPrecisionStance = 'PrecisionStance',
  CyberSamuraiBlockingStance = 'BlockingStance',

  Lapetus = 'Lapetus',
  ArmoredShibaInu = 'ArmoredShibaInu',
}
