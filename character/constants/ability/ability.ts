import { BattleEventsEnum } from '../../../battle/constants/battle-events';
import { BattleBuffsEnum } from '../../../battle/constants/battle-buffs.enum';
import { AbilityEnum } from './ability.enum';
import {
  ArchetypeAbilityType,
  BattleRuleType,
  BuffTargetEnum,
  EventSourceEnum,
  EventTargetEnum,
  RuleEnum,
} from './ability.types';

export const Abilities: Partial<Record<AbilityEnum, ArchetypeAbilityType>> = {
  [AbilityEnum.MercenaryPassive]: {
    target: false,
    needLevel: 0,
    reload: 0,
    energy: 0,
    steps: null,
    leveling: [],
  },
  [AbilityEnum.Marksman]: {
    target: false,
    needLevel: 2,
    reload: 5,
    energy: 1,
    steps: 3,
    leveling: [],
  },
  [AbilityEnum.MercenaryVersatility]: {
    target: false,
    needLevel: 5,
    reload: 5,
    energy: 1,
    steps: 3,
    leveling: [],
  },
  [AbilityEnum.MercenaryPiercingShells]: {
    target: true,
    needLevel: 5,
    reload: 5,
    energy: 1,
    steps: 3,
    leveling: [],
  },

  [AbilityEnum.CyberSamuraiPassive]: {
    target: false,
    needLevel: 0,
    reload: 0,
    energy: 0,
    steps: null,
    leveling: [],
  },
  [AbilityEnum.CyberSamuraiBattleStance]: {
    target: false,
    needLevel: 2,
    reload: 5,
    energy: 1,
    steps: 3,
    leveling: [],
  },
  [AbilityEnum.CyberSamuraiPrecisionStance]: {
    target: false,
    needLevel: 5,
    reload: 5,
    energy: 1,
    steps: 3,
    leveling: [],
  },
  [AbilityEnum.CyberSamuraiBlockingStance]: {
    target: false,
    needLevel: 5,
    reload: 5,
    energy: 1,
    steps: 3,
    leveling: [],
  },

  [AbilityEnum.Lapetus]: {
    target: false,
    needLevel: 0,
    reload: 4,
    energy: 2,
    steps: 2,
    leveling: [
      [
        {
          type: 'Trigger',
          event: BattleEventsEnum.Attack,
          source: EventSourceEnum.Enemies,
          target: EventTargetEnum.Friendly,
          buffs: [
            {
              type: 'Buff',
              name: BattleBuffsEnum.BaseAttack,
              target: BuffTargetEnum.Source,
            },
          ],
        },
      ],
    ],
  },
  [AbilityEnum.ArmoredShibaInu]: {
    target: false,
    needLevel: 0,
    reload: 4,
    energy: 2,
    steps: 2,
    leveling: [
      [
        {
          type: 'Trigger',
          event: BattleEventsEnum.Attack,
          source: EventSourceEnum.Friendly,
          target: EventTargetEnum.Target,
          buffs: [
            {
              type: 'Buff',
              name: BattleBuffsEnum.BaseAttack,
              target: BuffTargetEnum.Target,
            },
          ],
        },
      ],
    ],
  },
};

export const Rules: Partial<Record<RuleEnum, BattleRuleType>> = {
  Defence: {
    type: 'Trigger',
    event: BattleEventsEnum.Definition,
    source: EventSourceEnum.Self,

    triggers: [
      {
        type: 'Trigger',
        event: BattleEventsEnum.Count,
        value: 1,
        buffs: [
          {
            type: 'Buff',
            name: BattleBuffsEnum.AddShield,
            steps: 1,
            target: BuffTargetEnum.RuleSource,
          },
        ],
      },
    ],
  },
  MercenaryHeavyAttack: {
    type: 'Trigger',
    // При ударе
    event: BattleEventsEnum.SubAttack,
    source: EventSourceEnum.Self,
    target: EventTargetEnum.Target,

    buffs: [
      // Уменьшение точности противника при каждом попадании
      {
        type: 'Buff',
        name: BattleBuffsEnum.AddAccuracy,
        value: `({eventValue, triggerValue}) => {
          if (triggerValue.miss == false) {
            return -0.1;
          } else {
            return 0;
          }
        }`,
        steps: 1,
        target: BuffTargetEnum.Target,
      },
    ],
  },
};

// MercenaryPassive
Abilities[AbilityEnum.MercenaryPassive].leveling.push([
  {
    type: 'Trigger',
    event: BattleEventsEnum.Random,
    value: 0.25,

    triggers: [
      {
        type: 'Trigger',
        event: BattleEventsEnum.PreAttack,
        source: EventSourceEnum.Self,
        target: EventTargetEnum.Target,

        buffs: [
          {
            type: 'Buff',
            name: BattleBuffsEnum.Empty,
            steps: null,
            individual: true,
            value: `({eventValue, triggerValue}) => { triggerValue.modAttack *= 2; }`,
            target: BuffTargetEnum.RuleSource,
          },
        ],
      },
    ],
  },
]);

// Mercenary1
for (let i = 0; i < 3; i++) {
  Abilities[AbilityEnum.Marksman].leveling.push([
    {
      type: 'Trigger',
      event: BattleEventsEnum.Definition,
      source: EventSourceEnum.Self,
      target: EventTargetEnum.Self,

      triggers: [
        {
          type: 'Trigger',
          event: BattleEventsEnum.Count,
          value: 1,

          buffs: [
            {
              type: 'Buff',
              name: BattleBuffsEnum.AddAccuracy,
              steps: 3,
              value: 0.2 + 0.05 * i,
              target: BuffTargetEnum.RuleSource,
            },
          ],
        },
      ],
    },
  ]);
}

// Mercenary2
for (let i = 0; i < 3; i++) {
  Abilities[AbilityEnum.MercenaryVersatility].leveling.push([
    {
      type: 'Trigger',
      // С шансом 0.1
      event: BattleEventsEnum.Random,
      value: 0.1 * (i + 1),

      triggers: [
        {
          type: 'Trigger',
          // При ударе
          event: BattleEventsEnum.Attack,
          source: EventSourceEnum.Self,
          target: EventTargetEnum.Target,

          buffs: [
            // Восстановление ОД
            {
              type: 'Buff',
              name: BattleBuffsEnum.RecoveryAP,
              value: 1,
              steps: null, // Навсегда
              target: BuffTargetEnum.RuleSource,
            },
          ],
        },
      ],
    },
  ]);

  Abilities[AbilityEnum.MercenaryPiercingShells].leveling.push([
    {
      type: 'Trigger',
      event: BattleEventsEnum.Definition,
      source: EventSourceEnum.Self,
      target: EventTargetEnum.Target,

      triggers: [
        {
          type: 'Trigger',
          event: BattleEventsEnum.Count,
          value: 1,

          // уменьшение восстановления щитов противника
          buffs: [
            {
              type: 'Buff',
              name: BattleBuffsEnum.MultiRecoveryShield,
              value: 1 - 0.1 * i,
              steps: 3,
              target: BuffTargetEnum.Target,
            },
          ],
        },
      ],
    },
  ]);
}

// CyberSamuraiPassive
Abilities[AbilityEnum.CyberSamuraiPassive].leveling.push([
  {
    type: 'Trigger',
    event: BattleEventsEnum.Attack,
    source: EventSourceEnum.Self,
    target: EventTargetEnum.Target,

    triggers: [
      {
        type: 'Trigger',
        event: BattleEventsEnum.MainRuleBuffsReset,
        value: `({eventValue, triggerValue}) => { return triggerValue.full == 0; }`,
        //calculation: `(value, eventValue) => { if (eventValue > 0) { return value + 1; } else { return 0; } }`,

        buffs: [
          {
            type: 'Buff',
            name: BattleBuffsEnum.MultiDamage,
            steps: null,
            value: 1.1,
            target: BuffTargetEnum.RuleSource,
          },
        ],
      },
    ],
  },
]);

// CyberSamurai1
for (let i = 0; i < 3; i++) {
  Abilities[AbilityEnum.CyberSamuraiBattleStance].leveling.push([
    {
      type: 'Trigger',
      event: BattleEventsEnum.Count,
      value: 1,
      source: EventSourceEnum.Self,
      target: EventTargetEnum.Self,

      buffs: [
        {
          type: 'Buff',
          name: BattleBuffsEnum.MultiDamage,
          steps: 3,
          value: 1 + 0.2 + 0.05 * i,
        },
      ],
    },
  ]);
}

// CyberSamurai2

for (let i = 0; i < 3; i++) {
  Abilities[AbilityEnum.CyberSamuraiPrecisionStance].leveling.push([
    {
      type: 'Trigger',
      event: BattleEventsEnum.Count,
      value: 1,
      source: EventSourceEnum.Self,
      target: EventTargetEnum.Self,

      buffs: [
        // Дополнительная точность на 3 хода
        {
          type: 'Buff',
          name: BattleBuffsEnum.AddAccuracy,
          value: 0.1 * i,
          steps: 3,
        },
      ],
    },
  ]);

  Abilities[AbilityEnum.CyberSamuraiBlockingStance].leveling.push([
    {
      type: 'Trigger',
      // При ударе
      event: BattleEventsEnum.Attack,
      source: EventSourceEnum.Self,
      target: EventTargetEnum.Target,

      buffs: [
        // Даёт щиты
        {
          type: 'Buff',
          name: BattleBuffsEnum.RecoveryShield,
          steps: null,
          individual: true,
          target: BuffTargetEnum.RuleSource,
          // В зависимости от нанесённого урона
          value: `({eventValue, triggerValue}) => 0.1 * ${i} * triggerValue.full`,
        },
      ],
    },
  ]);
}
