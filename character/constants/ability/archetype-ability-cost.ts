import { Currencies } from '../../../../user/entities/user.constant';

// Стоимость прокачки зависит от уровня, на котором открывался талант:
export const ArchetypeAbilityCost: Currencies[] = [];

ArchetypeAbilityCost[2] = { mepa: 6 };
ArchetypeAbilityCost[5] = { mepa: 54 };
ArchetypeAbilityCost[10] = { mepa: 181.5 };
ArchetypeAbilityCost[15] = { mepa: 294 };
ArchetypeAbilityCost[20] = { mepa: 433.5 };
