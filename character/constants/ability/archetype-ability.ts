import { AbilityEnum } from './ability.enum';
import { ArchetypeEnum } from '../character/archetype.enum';

export const ArchetypeAbility: Record<
  string,
  {
    passive?: Array<AbilityEnum | AbilityEnum[]>;
    active?: Array<AbilityEnum | AbilityEnum[]>;
  }
> = {
  [ArchetypeEnum.Mercenaries]: {
    passive: [AbilityEnum.MercenaryPassive],
    active: [
      AbilityEnum.Marksman,
      [AbilityEnum.MercenaryVersatility, AbilityEnum.MercenaryPiercingShells],
    ],
  },
  [ArchetypeEnum.CyberSamurai]: {
    passive: [AbilityEnum.CyberSamuraiPassive],
    active: [
      AbilityEnum.CyberSamuraiBattleStance,
      [AbilityEnum.CyberSamuraiPrecisionStance, AbilityEnum.CyberSamuraiBlockingStance],
    ],
  },
  [ArchetypeEnum.Lapetus]: { active: [AbilityEnum.Lapetus] },
  [ArchetypeEnum.ArmoredShibaInu]: { active: [AbilityEnum.ArmoredShibaInu] },
};
