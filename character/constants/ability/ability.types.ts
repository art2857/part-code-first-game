import { BattleBuffsEnum } from '../../../battle/constants/battle-buffs.enum';
import { BattleEventsEnum } from '../../../battle/constants/battle-events';

export enum BuffTargetEnum {
  RuleSource = 'RuleSource', // На того кто использовал способность
  RuleTarget = 'RuleTarget', // На того на кого направлена способность

  Source = 'Source', // На того кто вызвал триггер
  Target = 'Target', // На того на кого направлен триггер

  RuleFriendly = 'RuleFriendly',
  RuleEnemies = 'RuleEnemies',

  // OtherSelf = 'OtherSelf', // На всех кроме себя
  // OtherTarget = 'OtherTarget', // На всех кроме выбранного
  // OtherSelfTarget = 'OtherSelfTarget', // На всех кроме себя и выбранного
  All = 'All', // На всех
}

export type BattleBuffType = {
  type: 'Buff';
  name: BattleBuffsEnum; // Имя эффекта
  steps?: number; // Длительность влияние эффекта
  value?: number | string /*| ((eventValue: number) => number)*/; // Сила эффекта
  target?: BuffTargetEnum; // Цели эффекта

  individual?: boolean; // True: Если способность на верхнем уровне закончиться, то данный эффект останется

  ids?: Array<{ targetId: string; buffId: number }>;
};

// Кто произвёл событие
export enum EventSourceEnum {
  Self = 'Self', // Я
  Friendly = 'Friendly',
  Enemies = 'Enemies',
  Any = 'Any', // Любой
}
// На кого направленно событие
export enum EventTargetEnum {
  Self = 'Self', // На меня
  Target = 'Target', // На того на кого направлено событие
  OtherSelf = 'OtherSelf', // На всех кроме меня
  OtherTarget = 'OtherTarget', // На всех кроме выбранного
  OtherSelfTarget = 'OtherSelfTarget', // На всех кроме меня и выбранного
  Friendly = 'Friendly',
  Enemies = 'Enemies',
  All = 'All', // На всех
}

export type TriggerType = {
  type: 'Trigger';
  event: BattleEventsEnum; // Тип события при котором срабатывает триггер
  source?: EventSourceEnum; // Если указано данное поле, то источником этого события должен быть characterId
  target?: EventTargetEnum; // Если указано данное поле, то целью этого события должен быть targetId
  calculation?: string; // Функция обработки значений value = f(value, eventValue)
  value?: number | string /*| ((eventValue: number) => number)*/; // Некоторый параметр используемый для триггера

  triggers?: Array<TriggerType>;
  buffs?: Array<BattleBuffType>;
};

export type BattleRuleType = BattleBuffType | TriggerType;

export type ArchetypeAbilityType = {
  needLevel: number; // Необходимый уровень для начала прокачки способности

  energy: number; // Затраты очков действия
  reload: number; // Длительность перезарядки способности

  target: boolean; // Направленная ли способность

  steps: number; // Длительность способности

  leveling: // Развитие способности
  Array<BattleRuleType>[];
};

export enum RuleEnum {
  Defence = 'Defence',
  MercenaryHeavyAttack = 'MercenaryHeavyAttack',
}
