import { getModelForClass, modelOptions, prop } from '@typegoose/typegoose';
import { BeAnObject } from '@typegoose/typegoose/lib/types';
import { Document } from 'mongoose';
import { DATABASE_CONNECTION_TOKEN } from '../../../globals/database/constants/database-connection-token';
import { AbilityEnum } from '../constants/ability/ability.enum';
import { ArchetypeEnum } from '../constants/character/archetype.enum';
import { CharacterMap } from '../constants/character/character-map.interface';
import { CharacterStateEnum } from '../constants/character/character-state.enum';
import { ClansEnum } from '../constants/clans/clans.enum';

export class CharacterAbility {
  @prop({ required: true, enum: Object.values(AbilityEnum) })
  name: AbilityEnum;

  @prop({ required: true, type: Number })
  level: number;

  @prop({ required: true, type: Number })
  needLevel: number;

  @prop({ required: true, type: Boolean })
  passive: boolean;
}

export class CharacterBranchAbilities {
  @prop({ required: true, type: CharacterAbility, _id: false })
  branches: CharacterAbility[];
}

@modelOptions({
  schemaOptions: { collection: 'characters', versionKey: false },
})
export class CharacterEntity {
  // User
  @prop({ required: true, type: String })
  userWallet: string;

  @prop({ required: false, type: String, default: null })
  nftId?: string;

  // State
  @prop({
    required: true,
    enum: Object.values(CharacterStateEnum),
    default: CharacterStateEnum.available,
  })
  state: CharacterStateEnum;

  @prop({ required: false, type: String, default: null })
  squadId?: string;

  @prop({ required: false, type: String, default: null })
  ownerId?: string;

  @prop({ required: true, type: Number, default: 1 })
  level: number;

  @prop({ required: true, type: String })
  clan: ClansEnum;

  @prop({ required: true, type: Number, default: 0 })
  exp: number;

  @prop({ required: true, type: Number, default: 0 })
  lvlexp: number;

  @prop({ required: true, type: Number, default: 0 })
  points: number;

  @prop({ required: true, enum: Object.values(ArchetypeEnum) })
  archetype: ArchetypeEnum;

  @prop({
    required: true,
    type: [CharacterBranchAbilities],
    default: [],
    _id: false,
  })
  abilities: CharacterBranchAbilities[];

  // Map
  @prop({ required: true, type: CharacterMap, _id: false })
  characterMap: CharacterMap;
}

export const CharacterModel = getModelForClass(CharacterEntity);

export type DocumentCharacter = Document<any, BeAnObject, CharacterEntity> &
  CharacterEntity;

export const CharacterModelProvider = {
  provide: 'CharacterModel',
  useFactory: () => CharacterModel,
  inject: [DATABASE_CONNECTION_TOKEN],
};
