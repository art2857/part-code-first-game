import { ApiProperty } from '@nestjs/swagger';
import { AbilityEnum } from './constants/ability/ability.enum';

export class UpLevelDTO {
  @ApiProperty() characterId: string;
}

export class UpAbilityDTO {
  @ApiProperty() characterId: string;
  @ApiProperty({ enum: AbilityEnum }) abilityName: AbilityEnum;
}
