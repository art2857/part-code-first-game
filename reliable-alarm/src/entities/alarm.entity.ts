import { getModelForClass, modelOptions, prop } from '@typegoose/typegoose';
import { DATABASE_CONNECTION_TOKEN } from '../../../../apps/cc-game-back/src/globals/database/constants/database-connection-token';

export enum AlarmEnum {
  timeout = 'timeout',
  interval = 'interval',
}

@modelOptions({
  schemaOptions: { collection: 'reliableAlarms', versionKey: false },
})
export class ReliableAlarmEntity {
  @prop({
    required: true,
    enum: Object.values(AlarmEnum),
    default: AlarmEnum.timeout,
  })
  type: AlarmEnum;

  @prop({ required: true, type: String })
  name: string;

  @prop({ required: true, type: Number })
  duration: number;

  @prop({ required: true, type: Number })
  callTime: number;

  @prop({ required: true, type: String })
  callFunction: string;

  @prop({ required: false, type: String })
  data?: string;
}

export const ReliableAlarmModel = getModelForClass(ReliableAlarmEntity);

export const ReliableAlarmModelProvider = {
  provide: 'ReliableAlarmModel',
  useFactory: () => ReliableAlarmModel,
  inject: [DATABASE_CONNECTION_TOKEN],
};
