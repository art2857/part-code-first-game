import { Inject, Injectable } from '@nestjs/common';
import { AlarmEnum, ReliableAlarmModel } from './entities/alarm.entity';

export type AlarmType = {
  _id: string;
  type: AlarmEnum;
  callFunction: string;
  duration: number;
  callTime: number;
  data: string;
  alarm: NodeJS.Timeout | NodeJS.Timer;
};

const Alarms: Map<string, AlarmType> = new Map(); // { alarmId: { alarm: NodeJs.Timeout,... } }

@Injectable()
export class ReliableAlarmService {
  constructor(
    @Inject('ReliableAlarmModel')
    private reliableAlarmModel: typeof ReliableAlarmModel,
  ) {}

  /**
   * Возобновляет работу таймеров сервиса
   * @param name - имя контекста(сервиса)
   * @param context - контекст выполнения (сервис)
   * @returns
   */
  async reliableAlarmLoad(name: string, context: any) {
    const alarms = await this.reliableAlarmModel.find({ name });
    const promises = [];

    for (const alarm of alarms) {
      if (!Alarms.has(alarm._id)) {
        promises.push(
          this.reliableSetAlarm({
            context,
            name,
            type: alarm.type,
            callFunction: alarm.callFunction,
            duration: alarm.duration,
            nextTick: alarm.callTime - Date.parse(Date()),
            existsId: alarm._id,
            data: alarm.data,
          }),
        );
      }
    }

    return Promise.all(promises);
  }

  /**
   * Устанавливает таймер выполнения
   * @param options contex - контекст выполнения (сервис)
   * @param options name - имя контекста (сервиса)
   * @param options type - тип таймаута (единоразово, повторение)
   * @param options callFunction - вызов функции
   * @param options duration - период через который сработает таймер
   * @param options nextTick - через сколько произойдёт первое срабатывание
   * @param options existsId - назачение id таймера
   * @param options data - дополнительные данные о таймере
   * @returns
   */
  async reliableSetAlarm(options: {
    context?: any;
    name: string;
    type?: AlarmEnum;
    callFunction: string;
    duration: number;
    nextTick?: number;
    existsId?: string;
    data?: string;
  }) {
    if (options.type === undefined) {
      options.type = AlarmEnum.timeout;
    }
    let alarmInfo: any;

    if (options.existsId) {
      alarmInfo = await this.reliableAlarmModel.findOne({
        _id: options.existsId,
      });
    } else {
      const alarm = new this.reliableAlarmModel();
      alarm.name = options.name;
      alarm.type = options.type;
      alarm.callFunction = `${options.callFunction}`;
      alarm.duration = options.duration;
      alarm.data = options.data;
      alarm.callTime = Date.parse(Date()) + options.duration;
      alarmInfo = await alarm.save();
    }

    let nodeAlarm: NodeJS.Timeout | NodeJS.Timer;
    if (options.type == AlarmEnum.timeout) {
      nodeAlarm = setTimeout(
        async () => {
          eval(
            `(context)=>{async function callFunction(){${alarmInfo.callFunction}} (callFunction.bind(context))()}`,
          )(options.context);

          await this.reliableClearAlarm(String(alarmInfo._id));
        },
        options.nextTick ? options.nextTick : options.duration,
      );
    } else {
      nodeAlarm = setTimeout(
        async () => {
          eval(
            `(context)=>{async function callFunction(){${alarmInfo.callFunction}} (callFunction.bind(context))()}`,
          )(options.context);

          await this.reliableAlarmModel.updateOne(
            { _id: String(alarmInfo._id) },
            { $set: { callTime: Date.parse(Date()) + options.duration } },
          );

          const _id = String(alarmInfo._id);
          const alarm = Alarms.get(_id);
          clearTimeout(alarm.alarm);

          nodeAlarm = setInterval(async () => {
            eval(
              `(context)=>{async function callFunction(){${alarmInfo.callFunction}} (callFunction.bind(context))()}`,
            )(options.context);

            await this.reliableAlarmModel.updateOne(
              { _id: String(alarmInfo._id) },
              { $set: { callTime: Date.parse(Date()) + options.duration } },
            );
          }, options.duration);

          alarm.type = AlarmEnum.interval;
          alarm.alarm = nodeAlarm;
        },
        options.nextTick ? options.nextTick : options.duration,
      );
    }

    const alarm: AlarmType = {
      _id: String(alarmInfo._id),
      type: AlarmEnum.timeout, //alarmInfo.type,
      callFunction: options.callFunction,
      duration: options.duration,
      callTime: alarmInfo.callTime,
      data: options.data,
      alarm: nodeAlarm,
    };

    Alarms.set(String(alarmInfo._id), alarm);

    return alarm;
  }

  async reliableAlarmGet(id: string) {
    return;
  }

  async reliableAlarmLastCall(id: string) {
    return;
  }

  async reliableAlarmNextCall(id: string) {
    return;
  }

  async reliableClearAlarm(id: string, remove = true): Promise<any> {
    const _id = String(id);
    const alarm = Alarms.get(_id);

    if (alarm) {
      if (alarm.type == AlarmEnum.timeout) {
        clearTimeout(alarm.alarm);
      } else {
        clearInterval(alarm.alarm);
      }
      Alarms.delete(_id);
    }
    if (remove) return this.reliableAlarmModel.deleteOne({ _id });
  }

  async reliableClearAlarms(remove = false): Promise<any> {
    Alarms.forEach((alarm) => {
      if (alarm.type == AlarmEnum.timeout) {
        clearTimeout(alarm.alarm);
      } else {
        clearInterval(alarm.alarm);
      }
      Alarms.delete(String(alarm._id));
    });

    if (remove) return this.reliableAlarmModel.deleteMany({});
  }

  async reliableAlarmFindOne(filter = {}) {
    return this.reliableAlarmModel.findOne(filter);
  }

  async reliableAlarmFindMany(filter = {}) {
    return this.reliableAlarmModel.find(filter);
  }
}
