import { Module } from '@nestjs/common';
import { config } from './../../config';
import { ReliableAlarmService } from './reliable-alarm.service';

config();

@Module({
  imports: [],
  providers: [
    ReliableAlarmService,
    // {
    //   provide: 'ReliableAlarmModel',
    //   useFactory: () => ReliableAlarmModel,
    //   inject: [DATABASE_CONNECTION_TOKEN],
    // },
  ],
  exports: [ReliableAlarmService],
})
export class ReliableAlarmModule {}
