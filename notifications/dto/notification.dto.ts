import { ApiProperty } from '@nestjs/swagger';
import { NotificationsTypeEnum } from '../notifications.constants';

export class NotificationDTO {
  @ApiProperty({
    required: true,
    enum: NotificationsTypeEnum,
    default: NotificationsTypeEnum.default,
  })
  type?: NotificationsTypeEnum;

  @ApiProperty({ required: false })
  title?: string;

  @ApiProperty({ required: false })
  description?: string;

  @ApiProperty({ required: false, type: Object })
  data?: Object;

  constructor(
    type: NotificationsTypeEnum,
    title: string,
    description?: string,
  ) {
    this.type = type;
    this.title = title;
    this.description = description;
  }
}

export class NotificationsViewOptions {
  @ApiProperty({ required: false, default: null })
  type: number | null = null;
  @ApiProperty({ required: false, default: 0 })
  offset = 0;
  @ApiProperty({ required: false, default: 25 })
  limit = 25;
}
