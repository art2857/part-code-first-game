export enum NotificationStateEnum {
  notViewed = 'notViewed',
  viewed = 'viewed',
  closed = 'closed',
  expired = 'expired',
  system = 'system',
}

export enum NotificationsTypeEnum {
  alphaBonusUser = 'alphaBonusUser',

  default = 'default',
  empty = 'empty',
  remove = 'remove',
  createUser = 'createUser', // Создан пользователь

  build = 'build', // Начало строительства
  buildSuccess = 'buildSuccess', // Постройка достроена
  buildCancle = 'buildCancle', // Строительство отменено

  squadReadyCapture = 'squadReadyCapture', // Отряд готов к захвату
  squadReadyRob = 'squadReadyRob', // Отряд готов к грабежу

  squadDefenceCapture = 'squadDefenceCapture', // Оповещение о том что хотят захватить район
  squadDefenceRob = 'squadDefenceRob', // Оповещение о том что хотят ограбить район

  districtCaptured = 'districtCaptured', // Район захвачен
  districtRobed = 'districtRobed', // Район ограблен

  battle = 'battle',

  battleWin = 'battleWin',
  battleLose = 'battleLose',
}

export enum OrderEnum {
  first = 'first',
  recent = 'recent',
}

export enum NotificationTypeSendEnum {
  All = 'All',
  Self = 'Self',
  Other = 'Other',
}
