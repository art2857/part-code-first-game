import { getModelForClass, modelOptions, prop } from '@typegoose/typegoose';
import { DATABASE_CONNECTION_TOKEN } from '../../globals/database/constants/database-connection-token';
import {
  NotificationStateEnum,
  NotificationsTypeEnum,
  NotificationTypeSendEnum,
} from '../notifications.constants';

@modelOptions({
  schemaOptions: { collection: 'notifications', versionKey: false },
})
export class NotificationsEntity {
  @prop({ required: false, type: String, default: null })
  userWallet?: string;

  @prop({
    required: false,
    enum: Object.values(NotificationsTypeEnum),
    default: NotificationsTypeEnum.default,
  })
  type: NotificationsTypeEnum;

  @prop({ required: false, type: Boolean, default: false })
  temporary: boolean;

  @prop({ required: false, type: Number })
  expired: number;

  @prop({
    required: true,
    enum: Object.values(NotificationTypeSendEnum),
    default: NotificationTypeSendEnum.All,
  })
  send: NotificationTypeSendEnum;

  @prop({ required: false, type: String })
  title?: string;

  @prop({ required: false, type: String })
  description?: string;

  @prop({
    required: true,
    enum: Object.values(NotificationStateEnum),
    default: NotificationStateEnum.notViewed,
  })
  state: NotificationStateEnum;

  @prop({
    required: false,
    type: Object,
    default: null,
  })
  data: Object;

  @prop({
    required: true,
    type: Number,
  })
  date: number;
}

export const NotificationsModel = getModelForClass(NotificationsEntity);

export const NotificationsModelProvider = {
  provide: 'NotificationsModel',
  useFactory: () => NotificationsModel,
  inject: [DATABASE_CONNECTION_TOKEN],
};
