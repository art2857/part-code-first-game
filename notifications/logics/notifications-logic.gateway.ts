import { Injectable, Logger } from '@nestjs/common';

import { NotificationDTO } from '../dto/notification.dto';
import { JwtService } from '@nestjs/jwt';
import { NotificationTypeSendEnum } from '../notifications.constants';

import { Server as WebSocketServer, WebSocket } from 'ws';
import { config } from '../../../../../libs/config';
import { NotificationsEntity } from '../entities/notifications.entity';
import { StatisticService } from '../../statistics/statistic.service';

interface IUser {
  wallet: string;
  exp: number;
}

interface NotificationsSocket extends WebSocket {
  refresh?: NodeJS.Timeout;
  user?: IUser;
}
const { JWT_SECRET } = config();
// @WebSocketGateway({ namespace: '/notifications', cors: '*:*' })
@Injectable()
export class NotificationsLogicGateway {
  // implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
  // @WebSocketServer() wss: Server;

  private jwt: JwtService = new JwtService({ secret: JWT_SECRET });
  private logger: Logger = new Logger('NotificationLogger');
  //users: Map<string, NotificationsSocket> = new Map();
  users: Map<string, WebSocket> = new Map();
  wss: WebSocketServer<WebSocket>;

  constructor(
    private readonly statisticService: StatisticService,
  ){
    console.log('statistic',this.statisticService);
  }

  init(firts: boolean) {
    if (firts) return;
    try {
      if (!this.wss) {
        this.wss = new WebSocketServer({
          path: '/ws',
          port: 8080,
        });

        this.wss.on('connection', (ws) => {
          console.log('connection!');

          const client: NotificationsSocket = ws;

          ws.on('message', (message) => {
            console.log('received: %s', message);

            try {
              const json = JSON.parse(message.toString());
              switch (json.type) {
                case 'auth': {
                  const jwt = json.data;

                  try {
                    const user: IUser = this.jwt.verify(jwt);
                    // userWallet.wallet = user.wallet
                    const refresh = client.refresh;
                    client.user = user;

                    if (refresh) {
                      clearTimeout(refresh);
                    }

                    const exp = user.exp * 1000 - Date.parse(Date());

                    client.refresh = setTimeout(() => {
                      client.close();
                    }, exp);

                    this.logger.log(`Authorization: ${jwt}`);

                    this.users.set(user.wallet, client);

                    client.send(
                      JSON.stringify({ type: 'auth', data: user.wallet }),
                    );
                    console.log('Authorized:', user.wallet);
                    return;
                  } catch (e) {
                    this.logger.log(`Invalid authorization ${jwt}`);
                    client.send(JSON.stringify({ type: 'authInvalid' }));
                    console.log(e);
                  }
                  break;
                }
              }
              
            } catch (e) {}
          });

          ws.on('close', () => {
            console.log('closed');
          });

          ws.send(JSON.stringify({ type: 'connected' }));
        });

        this.wss.on('close', function close() {
          console.log('ws disconnected');
        });
      }
    } catch (err) {
      console.log(err);
    }
  }

  async send(options: {
    typeSend?: NotificationTypeSendEnum;
    notification: NotificationsEntity | NotificationDTO;
    wallet?: string;
  }) {
    console.log('send wallet:', options.wallet,);

    if (!options.typeSend) {
      options.typeSend = NotificationTypeSendEnum.All;
    }

    if (options.typeSend === NotificationTypeSendEnum.Self) {
      const json = { type: 'notification', data: options.notification };
      const client = this.users.get(options.wallet);
      if (client?.send) {
        client.send(JSON.stringify(json));
      }
    }

    if (options.typeSend === NotificationTypeSendEnum.Other) {
      this.users.forEach((client, userWallet) => {
        if (userWallet !== options.wallet) {
          const json = { type: 'notification', data: options.notification };
          
          const client = this.users.get(userWallet);
          if (client?.send) {
            client.send(JSON.stringify(json));
          }
        }
      });
    }

    if (options.typeSend === NotificationTypeSendEnum.All) {
      this.users.forEach((client, userWallet) => {
        const json = { type: 'notification', data: options.notification };
        if (client?.send) {
          client.send(JSON.stringify(json));
        }
      });
    }
  }
}
