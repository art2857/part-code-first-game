import { Module } from '@nestjs/common';
import { NotificationsLogicGateway } from './notifications-logic.gateway';
import { NotificationsLogicService } from './notifications-logic.service';
import { config } from '../../../../../libs/config';
import { StatisticModule } from '../../statistics/statistic.module';

@Module({
  providers: [NotificationsLogicService, NotificationsLogicGateway],
  exports: [NotificationsLogicService, NotificationsLogicGateway],
  imports:[StatisticModule],
})
export class NotificationsLogicModule {}
