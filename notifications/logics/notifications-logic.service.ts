import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { DocumentType } from '@typegoose/typegoose';
import { BeAnObject } from '@typegoose/typegoose/lib/types';
import { Document, FilterQuery, isValidObjectId } from 'mongoose';
import { NotificationDTO } from '../dto/notification.dto';
import {
  NotificationsEntity,
  NotificationsModel,
} from '../entities/notifications.entity';
import {
  NotificationStateEnum,
  NotificationsTypeEnum,
  NotificationTypeSendEnum,
  OrderEnum,
} from '../notifications.constants';
import { NotificationsLogicGateway } from './notifications-logic.gateway';

@Injectable()
export class NotificationsLogicService {
  timer: NodeJS.Timer;
  constructor(
    private readonly notificationGateway: NotificationsLogicGateway,
    @Inject('NotificationsModel')
    private notificationsModel: typeof NotificationsModel,
  ) {
    //this.init();
  }

  init() {
    if (this.timer) {
      clearInterval(this.timer);
    }
    this.timer = setInterval(async () => {
      await this.notificationsModel.deleteMany({
        expired: { $lte: Date.now() / 1000 },
      });
    }, 60000);
  }

  async clearAll(): Promise<any> {
    return this.notificationsModel.deleteMany();
  }

  async create(options: {
    typeSend?: NotificationTypeSendEnum;
    temporary?: boolean;
    expired?: number;
    message: NotificationDTO;
    wallet?: string;
  }): Promise<NotificationsEntity> {
    if (!options.typeSend) {
      options.typeSend = NotificationTypeSendEnum.All;
    }
    const notification = new NotificationsModel();

    const expired = options.expired
      ? Date.now() / 1000 + options.expired
      : options.temporary
      ? Date.now() / 1000 + 5 * 60
      : undefined;

    notification.userWallet = options.wallet;
    notification.temporary = options.temporary;
    if (expired != undefined) {
      notification.expired = Math.floor(expired);
    }
    notification.send = options.typeSend;
    notification.type = options.message.type;
    notification.title = options.message.title;
    notification.description = options.message.description;
    notification.data = options.message.data;
    notification.date = Date.parse(Date()) / 1000;

    return notification.save();
  }

  async findOne(
    filter: FilterQuery<DocumentType<NotificationsEntity, BeAnObject>>,
  ) {
    return this.notificationsModel.findOne(filter);
  }

  async find(
    filter: FilterQuery<DocumentType<NotificationsEntity, BeAnObject>>,
  ) {
    return this.notificationsModel.find(filter);
  }

  async remove(
    filter: FilterQuery<DocumentType<NotificationsEntity, BeAnObject>>,
    send = NotificationTypeSendEnum.Self,
    wallet?: string,
  ): Promise<any> {
    const notifications = await this.notificationsModel.find({ filter });
    await this.notificationsModel.deleteMany(filter);

    const ids = notifications.map(
      (notification: Document<any, BeAnObject, NotificationsEntity>) => {
        return notification._id;
      },
    );

    await this.send({
      save: false,
      message: {
        type: NotificationsTypeEnum.remove,
        title: '',
        description: '',
        data: { ids: ids },
      },
      wallet,
      typeSend: send,
    });

    return notifications;
  }

  async send(options: {
    save?: boolean;
    typeSend: NotificationTypeSendEnum;
    message: NotificationDTO;
    wallet?: string;
    temporary?: boolean;
    expired?: number;
  }) {
    console.log('.Service Send start');
    let notification: NotificationsEntity;
    if (options.save == undefined || options.save == true) {
      notification = await this.create({
        message: options.message,
        wallet: options.wallet,
        typeSend: options.typeSend,
        temporary: options.temporary,
        expired: options.expired,
      });
    }

    if (process.env.NODE_ENV == 'test') return;

    return this.notificationGateway.send({
      notification: notification ?? options.message,
      wallet: options.wallet,
      typeSend: options.typeSend,
    });
  }

  async setState({
    ids,
    type,
    state,
    userWallet,
    skip,
    limit,
    order,

    stateSet,
  }: {
    ids?: string[];
    type?: NotificationsTypeEnum[];
    state?: NotificationStateEnum[];
    userWallet: string;
    skip: number;
    limit: number;
    order: OrderEnum;

    stateSet: NotificationStateEnum;
  }): Promise<any> {
    if (stateSet == NotificationStateEnum.system)
      throw new BadRequestException(`Can't set status 'system'`);

    return this.createRequestView({
      ids,
      type,
      state,
      userWallet,
      skip,
      limit,
      order,
    })
      .updateMany({}, { $set: { state: stateSet } })
      .exec();
  }

  createRequestView({
    ids,
    type,
    state,
    userWallet,
    skip,
    limit,
    order,
  }: {
    ids?: string[];
    type?: NotificationsTypeEnum[];
    state?: NotificationStateEnum[];
    userWallet: string;
    skip: number;
    limit: number;
    order: OrderEnum;
  }) {
    limit = Math.min(limit, 100);

    let request = this.notificationsModel.find({
      $or: [{ userWallet: null }, { userWallet }],
    });

    if (ids.length > 0) {
      for (const id of ids) {
        if (!isValidObjectId(id))
          throw new BadRequestException(
            `Error value notificationId ${id} as _id`,
          );
      }
      request = request.where('_id').in([...ids]);
    }

    if (type.length > 0) {
      request = request.where('type').in([...type]);
    }

    if (state.length > 0) {
      request = request.where('state').in([...state]);
    }

    request
      .sort({ _id: order == OrderEnum.first ? 1 : -1 })
      .skip(skip)
      .limit(limit);

    return request;
  }

  async view({
    ids,
    type,
    state,
    userWallet,
    skip,
    limit,
    order,
  }: {
    ids?: string[];
    type?: NotificationsTypeEnum[];
    state?: NotificationStateEnum[];
    userWallet: string;
    skip: number;
    limit: number;
    order: OrderEnum;
  }): Promise<{ count: number; notifications: NotificationsEntity[] }> {
    const count = await this.notificationsModel.count({
      $or: [{ userWallet: null }, { userWallet }],
    });

    const response = {
      count,
      notifications: await this.createRequestView({
        ids,
        type,
        state,
        userWallet,
        skip,
        limit,
        order,
      }).exec(),
    };

    // Удаление одноразовых оповещений
    const request = this.createRequestView({
      ids,
      type,
      state,
      userWallet,
      skip,
      limit,
      order,
    });

    const temps = await request.where({ temporary: true }).exec();

    if (temps.length > 0) {
      const tempsIds = temps.map((temp) => {
        return { _id: temp._id };
      });

      await this.notificationsModel.deleteMany({ $or: [...tempsIds] });
    }

    return response;
  }
}
