import { Module } from '@nestjs/common';
import { NotificationsController } from './notifications.controller';
import { NotificationsLogicModule } from './logics/notifications-logic.module';


@Module({
  imports: [NotificationsLogicModule],
  controllers: [NotificationsController],
  
})
export class NotificationsModule {}
