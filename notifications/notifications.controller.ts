import { applyDecorators, Controller, Get, Post, Query } from '@nestjs/common';
import { NotificationsLogicService } from './logics/notifications-logic.service';

import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Auth, AuthBusy } from '../../../auth/src/logics/auth.decorator';
import {
  NotificationStateEnum,
  NotificationsTypeEnum,
  OrderEnum,
} from './notifications.constants';
import { User } from '../../../auth/src/logics/user.decorator';

@ApiTags('notifications')
@Controller('/notifications')
export class NotificationsController {
  constructor(
    private readonly notificationsService: NotificationsLogicService,
  ) {}

  @applyDecorators(
    ApiOperation({ summary: 'Очистить все оповещения (Только для тестов)' }),
  )
  @Post('/clearAll')
  @AuthBusy()
  async clearAll() {
    return this.notificationsService.clearAll();
  }

  @applyDecorators(ApiOperation({ summary: 'Установить статус оповещения' }))
  @ApiQuery({ required: false, name: 'ids', type: String, isArray: true })
  @ApiQuery({
    required: false,
    name: 'type',
    enum: NotificationsTypeEnum,
    isArray: true,
  })
  @ApiQuery({
    required: false,
    name: 'state',
    enum: NotificationStateEnum,
    isArray: true,
  })
  @ApiQuery({ required: false, name: 'skip', type: Number })
  @ApiQuery({ required: false, name: 'limit', type: Number })
  @ApiQuery({ required: true, name: 'order', enum: OrderEnum })
  @ApiQuery({
    required: true,
    name: 'stateSet',
    enum: NotificationStateEnum,
  })
  @Post('/setState')
  @AuthBusy()
  async setState(
    @User('wallet') userWallet: string,
    @Query('ids') ids: string[] | string,
    @Query('type') type: string[] | string,
    @Query('state') state: string[] | string,
    @Query('skip') skip: string,
    @Query('limit') limit: string,
    @Query('order') order?: string,

    @Query('stateSet') stateSet?: string,
  ) {
    if (!skip) skip = '0';
    if (!limit) limit = '25';

    if (!ids) {
      ids = [];
    }
    if (!type) {
      type = [];
    }
    if (!state) {
      state = [];
    }

    if (!Array.isArray(ids)) {
      ids = [ids];
    }
    if (!Array.isArray(type)) {
      type = [type];
    }
    if (!Array.isArray(state)) {
      state = [state];
    }

    return this.notificationsService.setState(
      {
        ids: <string[]>ids,

        type: <NotificationsTypeEnum[]>type,
        state: <NotificationStateEnum[]>state,
        userWallet: userWallet,
        skip: Number(skip),
        limit: Number(limit),
        order: <OrderEnum>order,
        stateSet: <NotificationStateEnum>stateSet,
      },
    );
  }

  @applyDecorators(ApiOperation({ summary: 'Просмотреть историю оповещений' }))
  @ApiQuery({ required: false, name: 'ids', type: String, isArray: true })
  @ApiQuery({
    required: false,
    name: 'type',
    enum: NotificationsTypeEnum,
    isArray: true,
  })
  @ApiQuery({
    required: false,
    name: 'state',
    enum: NotificationStateEnum,
    isArray: true,
  })
  @ApiQuery({ required: false, name: 'skip', type: Number })
  @ApiQuery({ required: false, name: 'limit', type: Number })
  @ApiQuery({ required: true, name: 'order', enum: OrderEnum })
  @Get('/view')
  @Auth()
  async view(
    @User('wallet') userWallet: string,
    @Query('ids') ids: string[] | string,
    @Query('type') type: string[] | string,
    @Query('state') state: string[] | string,
    @Query('skip') skip: string,
    @Query('limit') limit: string,
    @Query('order') order?: string,
  ) {
    if (!skip) skip = '0';
    if (!limit) limit = '25';

    if (!ids) {
      ids = [];
    }
    if (!type) {
      type = [];
    }
    if (!state) {
      state = [];
    }

    if (!Array.isArray(ids)) {
      ids = [ids];
    }
    if (!Array.isArray(type)) {
      type = [type];
    }
    if (!Array.isArray(state)) {
      state = [state];
    }

    return this.notificationsService.view({
      ids: <string[]>ids,

      type: <NotificationsTypeEnum[]>type,
      state: <NotificationStateEnum[]>state,
      userWallet: userWallet,
      skip: Number(skip),
      limit: Number(limit),
      order: <OrderEnum>order,
    });
  }
}
