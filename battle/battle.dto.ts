import { ApiProperty } from '@nestjs/swagger';
import { AbilityEnum } from '../character/constants/ability/ability.enum';
import { ArchetypeEnum } from '../character/constants/character/archetype.enum';
import { ClansEnum } from '../character/constants/clans/clans.enum';

export class AttackDTO {
  //@ApiProperty() battleId: string;
  @ApiProperty() characterId: string;
  @ApiProperty() targetId: string;
  //@ApiProperty({ enum: AttackTypeEnum }) typeAttack: AttackTypeEnum;
}

export class DefenceDTO {
  @ApiProperty() characterId: string;
}

export class AbilityArchytypeDTO {
  @ApiProperty({ enum: AbilityEnum }) ability: AbilityEnum;
  @ApiProperty() characterId: string;
  @ApiProperty({ required: false }) targetId?: string;
}

export class EndStepDTO {
}

export class UserSquadCharacterAbilityDTO {
  @ApiProperty({ enum: AbilityEnum })
  name: AbilityEnum;

  @ApiProperty({ type: Number })
  level: number;
}

export class UserSquadCharacterDTO {
  @ApiProperty({ enum: ArchetypeEnum })
  archetype: ArchetypeEnum;

  @ApiProperty({ type: Number })
  level: number;

  @ApiProperty({ enum: ClansEnum })
  clan?: ClansEnum;

  @ApiProperty({ type: UserSquadCharacterAbilityDTO, isArray: true })
  abilities: UserSquadCharacterAbilityDTO[];
}

export class UserSquadDTO {
  @ApiProperty({ type: String })
  wallet: string;

  @ApiProperty({ type: Boolean })
  autoBattle: boolean;

  @ApiProperty({ type: UserSquadCharacterDTO, isArray: true })
  characters: UserSquadCharacterDTO[];
}

export class BattleTestDTO {
  @ApiProperty({ type: UserSquadDTO })
  attacker: UserSquadDTO;

  @ApiProperty({ type: UserSquadDTO })
  defensive: UserSquadDTO;

  @ApiProperty({ type: Number })
  autoEndStep: number;
}
