// События боя
export enum BattleEventsEnum {
  Definition = 'Definition', // Пустой триггер определяющий когда срабатывает событие (от кого и на кого)
  Random = 'Random', // Случайность
  Count = 'Count', // Количество срабатываний
  Friendly = 'Friendly', // Только на дружественных
  Enemies = 'Enemies', // Только на вражеских
  RandomCharacters = 'RandomCharacters', // Выбирает случайные цели
  Calculation = 'Calculation', //

  PreAttack = 'PreAttack', //
  Attack = 'Attack', // Атака противника
  BaseAttack = 'BaseAttack', // Обычная атака противника
  HeavyAttack = 'HeavyAttack', // Сильная атака противника

  PreSubAttack = 'PreSubAttack', //
  SubAttack = 'SubAttack', // Удар атаки

  Skip = 'Skip',
  Defence = 'Defence',

  Miss = 'Miss', // Промах атакой
  MissFull = 'MissFull', // Промах всей атакой

  TakeDamage = 'TakeDamage', // Получение урона
  TakeDamageHealth = 'TakeDamageHealth', // Получение урона по здоровью
  TakeDamageArmor = 'TakeDamageArmor', // Получение урона по броне
  TakeDamageShield = 'TakeDamageShield', // Получение урона по щиту

  Recover = 'Recover', // Восстановление
  RecoverHealth = 'RecoverHealth', // Восстановление здоровья AddHealth = 'AddHealth',
  RecoverArmor = 'RecoverArmor', // Восстановление брони AddArmor = 'AddArmor',
  RecoverShield = 'RecoverShield', // Восстановление щита AddShield = 'AddShield',
  RecoveryAP = 'RecoveryAP', // Восстановление ОД

  UseAbility = 'UseAbility', // Использования способности персонажа
  CreateRule = 'CreateRule',
  MainRuleBuffsReset = 'MainRuleBuffsReset',
  RemoveRule = 'RemoveRule',

  BuffCreate = 'BuffCreate', // Новый эффект
  BuffExpired = 'BuffExpired', // Эффект закончился

  CharacterDeath = 'CharacterDeath', // Смерть персонажа

  StartStep = 'StartStep', // Старт хода
  EndStep = 'EndStep', // Конец хода
  EndBattle = 'EndBattle', // Сражение закончилось
}
