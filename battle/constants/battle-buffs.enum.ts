export enum BattleBuffsEnum {
  // Mercenary1 = 'Mercenary1',
  // Mercenary2 = 'Mercenary2',
  // CyberSamurai1 = 'CyberSamurai1',
  // CyberSamurai2 = 'CyberSamurai2',

  Empty = 'Empty',

  Laceration = 'Laceration', // Каждый последующий удар увеличивает урон, при промахе эффект сбрасывается
  RecoveryAP = 'RecoveryAP', // Восстанавливает ОД
  RecoveryHealth = 'RecoveryHealth', // Восстановливает здоровье
  RecoveryArmor = 'RecoveryArmor', // Восстановливает защиту
  RecoveryShield = 'RecoveryShield', // Восстановливает щиты

  BaseAttack = 'BaseAttack',
  HeavyAttack = 'HeavyAttack',
  Defence = 'Defence',
  Skip = 'Skip',

  // Добавляет * после некоторого времени пропадает
  AddHealth = 'AddHealth',
  AddArmor = 'AddArmor',
  AddDamage = 'AddDamage',
  AddAccuracy = 'AddAccuracy',
  AddShield = 'AddShield',

  MultiHealth = 'MultiHealth',
  MultiArmor = 'MultiArmor',
  MultiDamage = 'MultiDamage',
  MultiAccuracy = 'MultiDamage',
  MultiShield = 'MultiDamage',

  MultiRecoveryHealth = 'MultiRecoveryHealth', // Множитель восстановления здоровья
  MultiRecoveryArmor = 'MultiRecoveryArmor', // Множитель восстановления защиты
  MultiRecoveryShield = 'MultiRecoveryShield', // Множитель восстановления щитов
}
