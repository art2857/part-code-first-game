import { prop } from '@typegoose/typegoose';
import {} from '../../character/constants/ability/ability';
import { AbilityEnum } from '../../character/constants/ability/ability.enum';
import {
  BattleRuleType,
  RuleEnum,
} from '../../character/constants/ability/ability.types';
import { BattleBuffsEnum } from './battle-buffs.enum';

export class BattleMainRule {
  @prop({ required: true, type: String })
  wallet: string;

  @prop({ required: true, type: String })
  characterId: string;

  @prop({ required: false, type: String })
  targetId?: string;

  @prop({
    required: false,
    enum: [
      ...Object.values(BattleBuffsEnum),
      ...Object.values(AbilityEnum),
      ...Object.values(RuleEnum),
    ],
  })
  name?: BattleBuffsEnum | AbilityEnum | RuleEnum;

  @prop({ required: true, type: Object })
  rules: BattleRuleType[];

  @prop({ required: true, type: Number })
  steps: number;

  @prop({ required: false, type: String })
  value?: number | string;
}
