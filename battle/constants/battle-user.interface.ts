import { IBattleCharacter } from './battle-character.interface';

export interface IBattleUser {
  wallet: string;
  squadId: string;
  autoBattle: boolean;
  squad: Record<string, IBattleCharacter>;
}
