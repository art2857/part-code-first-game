import { ArchetypeEnum } from '../../character/constants/character/archetype.enum';
import { ICharacterBattle } from '../../character/constants/character/character-battle.interface';
import { BattleCharacterStateEnum } from './battle-character-state.enum';
import { BattleCharacterAbilities } from './battle-character-abilities.type';
import { BattleBuffsEnum } from './battle-buffs.enum';
import { IBattleWeaponState } from '../../character/constants/weapons/weapon.types';

export type BattleCharacterBuffType = {
  buffId: number;
  name: BattleBuffsEnum;
  value: number;
  steps: number;
  individual: boolean;
};

export interface IBattleCharacter extends ICharacterBattle {
  _id?: string;

  level: number;
  userWallet: string;
  archetype: ArchetypeEnum;

  state: BattleCharacterStateEnum;

  ownerId?: string;

  abilities: BattleCharacterAbilities; // Record<string, { value: number; reload: number }>;
  buffs: BattleCharacterBuffType[];

  weaponStats: IBattleWeaponState;

  original: ICharacterBattle;
}
