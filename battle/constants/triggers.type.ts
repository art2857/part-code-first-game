import { AttackTypeEnum } from './attack-type.enum';
import { SubAttackType } from './battle.constants';

export type TriggerValueAttack = {
  typeAttack: AttackTypeEnum;
  full: number;
  fullDamageHealth: number;
  fullDamageArmor: number;
  fullDamageShield: number;
  attacks: SubAttackType[];
};
