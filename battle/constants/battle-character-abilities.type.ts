import { AbilityEnum } from '../../character/constants/ability/ability.enum';

export type BattleCharacterAbility = {
  name: AbilityEnum; // Название способности

  level: number; // Уровень способности

  energy: number; // Затраты очков действия
  reload: number; // Длительность перезарядки
  remaining: number; // Осталось до перезарядки

  target: boolean;

  passive: boolean;
};

export type BattleCharacterAbilities = Record<string, BattleCharacterAbility>;
