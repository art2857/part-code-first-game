export const Neutral = 'neutral';

export type SubAttackType = {
  damage: number;
  damageHealth: number;
  damageArmor: number;
  damageShield: number;
  miss: boolean;
};
