export enum BattleCharacterStateEnum {
  default = 'default',
  step = 'step',
  defence = 'defence',
  skip = 'skip',
}
