import { AttackTypeEnum } from '../constants/attack-type.enum';
import { SubAttackType } from '../constants/battle.constants';

export type BattleResponseAttack = {
  battleId: string;
  type: AttackTypeEnum;

  characterId: string;
  targetId: string;

  full: number;
  fullDamageHealth: number;
  fullDamageArmor: number;
  fullDamageShield: number;
  attacks: SubAttackType[];

  energy: number;
};
