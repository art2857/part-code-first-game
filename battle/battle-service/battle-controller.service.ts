import { forwardRef, Inject, NotFoundException } from '@nestjs/common';
import { NotificationsLogicService } from '../../../notifications/logics/notifications-logic.service';

import { CharacterModel } from '../../character/entities/character.entity';
import { SquadModel } from '../../squad/entities/squad.entity';
import { BattleMainRule } from '../constants/battle-main-rule.type';
import { BattleStateEnum } from '../constants/battle-state.enum';

import { BattleModel, DocumentBattle } from '../entities/battle.entity';
import { MapLogicService } from '../../../map/logics/map-logic.service';
import { CellModel } from '../../../map/entities/cell.entity';
import { BattleServiceBase } from './battle-base.service';
import { EnvironmentService } from '../../../globals/environment/environment.service';
import { SquadService } from '../../squad/squad.service';
import { CharacterService } from '../../character/character.service';

export class BattleServiceController extends BattleServiceBase {
  constructor(
    @Inject(forwardRef(() => SquadService))
    protected readonly squadService: SquadService,
    @Inject(forwardRef(() => MapLogicService))
    protected readonly mapService: MapLogicService,
    @Inject('CellModel')
    protected cellModel: typeof CellModel,
    @Inject('CharacterModel')
    protected characterModel: typeof CharacterModel,
    @Inject('SquadModel')
    protected squadModel: typeof SquadModel,
    @Inject('BattleModel')
    protected battleModel: typeof BattleModel,
    @Inject(forwardRef(() => CharacterService))
    protected readonly characterService: CharacterService,
    protected readonly notificationService: NotificationsLogicService,
    protected readonly evnironmentService: EnvironmentService,
  ) {
    super(
      squadService,
      mapService,
      cellModel,
      characterModel,
      squadModel,
      battleModel,
      characterService,
      notificationService,
      evnironmentService,
    );
  }

  controllerBattle(battle: DocumentBattle) {
    if (!battle) throw new NotFoundException(`controllerBattle empty battle`);

    const inaction = { current: 0, end: 0 };

    if (this.inaction[battle._id]) {
      inaction.current = Math.floor(
        (Date.now() - this.inaction[battle._id].start) / 1000,
      );
      inaction.end = Math.floor(
        (this.inaction[battle._id].end - this.inaction[battle._id].start) /
          1000,
      );
    }

    const _battle: {
      _id: string;
      step: number;
      state: BattleStateEnum;
      winnerWallet: string;
      rules: BattleMainRule[];
      battleUsers?: {
        attacker?: {
          wallet: string;
          squadId: string;
          autoBattle: boolean;
          squad: any; 
        };
        defensive?: {
          wallet: string;
          squadId: string;
          autoBattle: boolean;
          squad: any; 
        };
      };
      inaction: { current: number; end: number };
    } = {
      _id: battle._id,
      step: battle.step,
      state: battle.state,
      winnerWallet: battle.winnerWallet,
      rules: battle.rules,
      inaction,
    };

    const battleUsersKeys = Object.keys(battle.battleUsers);

    _battle.battleUsers = {};

    _battle.battleUsers.attacker = {
      wallet: battle.battleUsers[battleUsersKeys[0]].wallet,
      squadId: battle.battleUsers[battleUsersKeys[0]].squadId,
      autoBattle: battle.battleUsers[battleUsersKeys[0]].autoBattle,
      squad: Object.values(battle.battleUsers[battleUsersKeys[0]].squad).map(
        (character: any) => {
          if (character.abilities) {
            character.abilities = Object.values(character.abilities);
          }
          return character;
        },
      ),
    };

    _battle.battleUsers.defensive = {
      wallet: battle.battleUsers[battleUsersKeys[1]].wallet,
      squadId: battle.battleUsers[battleUsersKeys[1]].squadId,
      autoBattle: battle.battleUsers[battleUsersKeys[1]].autoBattle,
      squad: Object.values(battle.battleUsers[battleUsersKeys[1]].squad).map(
        (character: any) => {
          if (character.abilities) {
            character.abilities = Object.values(character.abilities);
          }
          return character;
        },
      ),
    };

    return _battle;
  }

  async controllerGetStep(wallet: string) {
    const battle = await this.battleModel.findOne({
      [`battleUsers.${wallet}.wallet`]: wallet,
      state: BattleStateEnum.Battle,
    });

    if (!battle) throw new NotFoundException('Not found current battle');

    const { step, stepCycle, battleWallet } = await this.getStep(battle);

    if (battleWallet == wallet) return { step, stepCycle, battleWallet };

    return { step, stepCycle, battleWallet: 'unknow' };
  }
}
