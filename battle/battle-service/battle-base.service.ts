import {
  BadRequestException,
  ConflictException,
  ForbiddenException,
  forwardRef,
  ImATeapotException,
  Inject,
  NotFoundException,
} from '@nestjs/common';
import { CellModel } from '../../../map/entities/cell.entity';

import { Pos } from '../../../map/map.constants';
import { MapLogicService } from '../../../map/logics/map-logic.service';
import { NotificationDTO } from '../../../notifications/dto/notification.dto';
import {
  NotificationsTypeEnum,
  NotificationTypeSendEnum,
} from '../../../notifications/notifications.constants';
import { NotificationsLogicService } from '../../../notifications/logics/notifications-logic.service';
import { CompareOID, getRandomArrayItem } from '../../../../../../libs/helpers';
import { isObjectIdOrHexString, isValidObjectId } from 'mongoose';

import { Abilities, Rules } from '../../character/constants/ability/ability';
import { AbilityEnum } from '../../character/constants/ability/ability.enum';
import {
  BattleRuleType,
  BuffTargetEnum,
  EventSourceEnum,
  EventTargetEnum,
  RuleEnum,
} from '../../character/constants/ability/ability.types';
import { ArchetypeEnum } from '../../character/constants/character/archetype.enum';
import { ClansEnum } from '../../character/constants/clans/clans.enum';

import {
  CharacterModel,
  DocumentCharacter,
} from '../../character/entities/character.entity';
import { SquadModel } from '../../squad/entities/squad.entity';
import { SquadStateEnum } from '../../squad/squad.constants';
import { AttackTypeEnum } from '../constants/attack-type.enum';
import { BattleBuffsEnum } from '../constants/battle-buffs.enum';
import { BattleCharacterAbilities } from '../constants/battle-character-abilities.type';
import { BattleCharacterStateEnum } from '../constants/battle-character-state.enum';
import {
  BattleCharacterBuffType,
  IBattleCharacter,
} from '../constants/battle-character.interface';
import { BattleEventsEnum } from '../constants/battle-events';
import { BattleMainRule } from '../constants/battle-main-rule.type';
import { BattleStateEnum } from '../constants/battle-state.enum';
import { IBattleUser } from '../constants/battle-user.interface';
import { Neutral, SubAttackType } from '../constants/battle.constants';
import { TriggerValueAttack } from '../constants/triggers.type';

import { BattleModel, DocumentBattle } from '../entities/battle.entity';
import { BattleResponseAttack } from './battle-response.type';
import { Currencies } from '../../../user/entities/user.constant';
import { EnvironmentService } from '../../../globals/environment/environment.service';
import { EnvironmentEntity } from '../../../globals/environment/environment.entity';
import { SquadService } from '../../squad/squad.service';
import { CharacterService } from '../../character/character.service';
import {
  IBattleWeaponState,
  IBattleWeaponStateAttack,
} from '../../character/constants/weapons/weapon.types';

export class BattleServiceBase {
  inaction: Record<
    string, // BattleId
    { start: number; end: number; timer: NodeJS.Timeout }
  > = {};
  env: EnvironmentEntity;
  constructor(
    @Inject(forwardRef(() => SquadService))
    protected readonly squadService: SquadService,
    @Inject(forwardRef(() => MapLogicService))
    protected readonly mapService: MapLogicService,
    @Inject('CellModel')
    protected cellModel: typeof CellModel,
    @Inject('CharacterModel')
    protected characterModel: typeof CharacterModel,
    @Inject('SquadModel')
    protected squadModel: typeof SquadModel,
    @Inject('BattleModel')
    protected battleModel: typeof BattleModel,
    @Inject(forwardRef(() => CharacterService))
    protected readonly characterService: CharacterService,
    protected readonly notificationService: NotificationsLogicService,
    protected readonly environmentService: EnvironmentService,
  ) {
    //this.init();
    this.env = this.environmentService.environment;
  }

  init() {
    (async () => {
      const battles = await this.battleModel.find({
        state: BattleStateEnum.Battle,
      });
      for (const battle of battles) {
        this.refreshInaction(battle._id);
      }
    })();

    (async () => {
      const battles = await this.battleModel.find({});

      battles.forEach(async (battle) => {
        try {
          const { battleWallet } = await this.getStep(battle);

          if (battleWallet == Neutral) {
            await this.neutralStep(battle._id);
          }
        } catch (err) {
          console.log(err);
        }
      });
    })();
  }

  //#region BASE

  async create(
    _squads: { _id: string; autoBattle: boolean }[],
    type?:
      | NotificationsTypeEnum.squadReadyCapture
      | NotificationsTypeEnum.squadReadyRob
      | NotificationsTypeEnum.empty,
    position?: Pos,
    autoEndStep: number = this.env.BattleAutoEndStep * 1000,
  ) {
    const squadsIds = Array.from(new Set(_squads.map((_squad) => _squad._id)));

    const battle = new BattleModel();
    battle.data = { type, position };
    battle.battleUsers = {};

    for (let squadIndex = 0; squadIndex < squadsIds.length; squadIndex++) {
      const squadId = squadsIds[squadIndex];
      const autoBattle = _squads.find((_squad) => {
        return _squad._id == squadId;
      })?.autoBattle
        ? true
        : false;

      if (!isValidObjectId(squadId))
        throw new BadRequestException(`Error value squadId ${squadId} as _id`);

      const squadParametrs = await this.squadModel.findOne({ _id: squadId });
      if (!squadParametrs)
        throw new NotFoundException(`Squad id '${squadId}' not found`);

      const charactersIds = squadParametrs.charactersIds;

      const squad: Record<string, IBattleCharacter> = {};

      for (const characterId of charactersIds) {
        const character = await this.characterModel.findOne({
          _id: characterId,
        });
        if (!character)
          throw new NotFoundException(
            `Character id '${characterId}' not found`,
          );

        const abilities: BattleCharacterAbilities = {};

        const characterAbilities = character.abilities;
        if (characterAbilities) {
          characterAbilities.forEach((characterAbility) => {
            //if (Array.isArray(characterAbility)) {
            const { branches } = characterAbility;

            for (const ability of branches) {
              const { name, level, passive } = ability;

              const { reload, energy, target } = this.env.Abilities[name];

              if (level > 0) {
                abilities[name] = {
                  name,

                  level,

                  energy,
                  reload,

                  target,

                  passive,
                  remaining: 0,
                };
              }
            }
          });
        }

        const characterBattle =
          await this.characterService.getStateCharacterBattle(
            character.archetype,
            character.clan,
            character.level,
            character.userWallet,
          );

        const weaponStats: IBattleWeaponState = {
          baseAttack: Object.assign(
            { remaining: 0 },
            characterBattle.weaponStats.baseAttack,
          ),
        };

        if (characterBattle.weaponStats.heavyAttack) {
          weaponStats.heavyAttack = Object.assign(
            { remaining: 0 },
            characterBattle.weaponStats.heavyAttack,
          );
        }

        squad[character._id] = {
          _id: character._id,

          level: character.level,
          userWallet: character.userWallet,
          archetype: character.archetype,

          state: BattleCharacterStateEnum.default,

          ownerId: character.ownerId,

          abilities,
          buffs: [],

          ...characterBattle,

          weaponStats,

          original: characterBattle,
        };

        if (squadIndex > 0) {
          squad[character._id].energy = 4;
          squad[character._id].original.energy = 4;
        }
      }

      battle.battleUsers[squadParametrs.userWallet] = <IBattleUser>{
        wallet: squadParametrs.userWallet,
        squadId: squadId,
        autoBattle,
        squad: squad,
      };
    }

    await battle.save();

    const _battle = await this.battleModel.findOne({ _id: battle._id });

    const _battleUsers = _battle.battleUsers;

    for (const _battleUser of Object.values(_battleUsers)) {
      const _squad = Object.values(_battleUser.squad);

      for (const _character of _squad) {
        const _abilities = _character.abilities;

        if (_abilities) {
          for (const _ability of Object.values(_abilities)) {
            if (_ability.passive == true) {
              await this.abilityArchytype(
                _battleUser.wallet,
                _battle,
                _ability.name,
                _character._id,
                undefined, //_character._id,
                true,
              );
            }
          }
        }
      }
    }

    await this.battleModel.updateOne({ _id: _battle._id }, _battle);

    if (autoEndStep > 0) {
      this.refreshInaction(_battle._id, autoEndStep);
    }

    return _battle;
  }

  async battleTest(
    attacker: {
      wallet: string;
      autoBattle: boolean;

      characters: {
        archetype: ArchetypeEnum;
        level: number;
        clan?: ClansEnum;
        abilities: { name: AbilityEnum; level: number }[];
      }[];
    },
    defensive: {
      wallet: string;
      autoBattle: boolean;

      characters: {
        archetype: ArchetypeEnum;
        level: number;
        clan?: ClansEnum;
        abilities: { name: AbilityEnum; level: number }[];
      }[];
    },
    autoEndStep: number,
  ) {
    try {
      const createSquad = async (settingsSquad: {
        wallet: string;

        characters: {
          archetype: ArchetypeEnum;
          level: number;
          clan?: ClansEnum;
          abilities: { name: AbilityEnum; level: number }[];
        }[];
      }) => {
        const characters: DocumentCharacter[] = [];

        const { wallet: settingsWallet, characters: settingsCharacters } =
          settingsSquad;

        for (const settingsCharacter of settingsCharacters) {
          const character = await this.characterService.create(
            settingsWallet,
            settingsCharacter.archetype,
            settingsCharacter.level,
            settingsCharacter.clan,
            settingsCharacter.abilities,
          );

          characters.push(character);
        }

        const squad = await this.squadService.create(
          settingsWallet,
          characters.map((character) => character._id),
        );

        return { characters, squad };
      };

      const { characters: attackerCharacters, squad: attackerSquad } =
        await createSquad(attacker);
      const { characters: defensiveCharacters, squad: defensiveSquad } =
        await createSquad(defensive);

      const battle = await this.create(
        [
          { _id: attackerSquad._id, autoBattle: attacker.autoBattle },
          { _id: defensiveSquad._id, autoBattle: defensive.autoBattle },
        ],
        NotificationsTypeEnum.empty,
        undefined,
        autoEndStep,
      );

      await Promise.all([
        this.squadService.remove(attackerSquad._id, true),
        this.squadService.remove(defensiveSquad._id, true),
      ]);

      return battle;
    } catch (err) {}
  }

  refreshInaction(
    battleId: string,
    incationTime = this.env.BattleAutoEndStep * 1000,
  ) {
    this.removeInaction(battleId);

    this.inaction[battleId] = {
      start: Date.now(),
      end: Date.now() + incationTime,
      timer: setTimeout(async () => {
        await this.endStep(battleId);
      }, incationTime),
    };
  }

  removeInaction(battleId: string) {
    if (this.inaction[battleId]?.timer) {
      clearTimeout(this.inaction[battleId].timer);
    }
    delete this.inaction[battleId];
  }

  async getBattleById(battleId: string) {
    battleId = String(battleId);
    if (!isValidObjectId(battleId))
      throw new BadRequestException(`Error value battleId ${battleId} as _id`);

    return this.battleModel.findOne({ _id: battleId });
  }

  async getBattle(battleId: string) {
    return this.battleModel.findOne({ _id: battleId });
  }

  async getCurrentBattle(
    wallet: string,
    autoBattle?: boolean,
    errors: boolean = true,
  ) {
    const filter: any = {
      [`battleUsers.${wallet}.wallet`]: wallet,
      state: BattleStateEnum.Battle,
    };

    if (autoBattle !== undefined) {
      filter[`battleUsers.${wallet}.autoBattle`] = autoBattle;
    }

    const battle = await this.battleModel.findOne(filter);

    if (errors) {
      if (!battle) throw new NotFoundException('Not found current battle');
    }

    return battle;
  }

  async getBattles(wallet: string, autoBattle?: boolean) {
    const filter: any = {
      [`battleUsers.${wallet}.wallet`]: wallet,
      state: BattleStateEnum.Battle,
    };

    if (autoBattle !== undefined) {
      filter[`battleUsers.${wallet}.autoBattle`] = autoBattle;
    }

    const battles = await this.battleModel.find(filter);

    return battles;
  }

  async getHistoryBattles(wallet: string) {
    const battles = await this.battleModel.find({
      [`battleUsers.${wallet}.wallet`]: wallet,
    });

    return battles;
  }

  async overloadBattle(_battle: string | DocumentBattle): Promise<{
    battle: DocumentBattle;
    battleId: string;
    type: 'string' | 'model';
  }> {
    let battleId: string;
    let battle: DocumentBattle;

    if (typeof _battle == 'string' || isObjectIdOrHexString(_battle)) {
      battleId = _battle.toString();

      if (!isValidObjectId(battleId)) {
        throw new BadRequestException(
          `Error value battleId ${battleId} as _id`,
        );
      }

      battle = await this.getBattleById(battleId);
      return { battle, battleId, type: 'string' };
    } else if (_battle.constructor.name == 'model') {
      battle = <DocumentBattle>_battle;

      if (!battle) {
        throw new NotFoundException(`Battle id '${battleId}' not found`);
      }

      battleId = battle._id.toString();
      return { battle, battleId, type: 'model' };
    }
    throw new Error('"_battle" error type');
  }

  async getStep(
    battleId: string,
    offset?: number,
  ): Promise<{
    step: number;
    stepCycle: number;
    battleWallet: string;
  }>;
  async getStep(
    battle: DocumentBattle,
    offset?: number,
  ): Promise<{
    step: number;
    stepCycle: number;
    battleWallet: string;
  }>;

  async getStep(_battle: string | DocumentBattle, offset = 0) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    const step = battle.step + offset;
    const battleUsers = battle.battleUsers;
    const stepCycle = step % Object.keys(battleUsers).length;
    const battleWallet = Object.keys(battleUsers)[stepCycle];

    return {
      step,
      stepCycle,
      battleWallet,
    };
  }

  async stepIs(
    battleId: string,
    wallet: string,
    offset?: number,
  ): Promise<boolean>;
  async stepIs(
    battle: DocumentBattle,
    wallet: string,
    offset?: number,
  ): Promise<boolean>;

  async stepIs(_battle: string | DocumentBattle, wallet: string, offset = 0) {
    if (typeof _battle == 'string') {
      const { battleWallet, step, stepCycle } = await this.getStep(
        _battle,
        offset,
      );
      return battleWallet == wallet;
    }
    if (_battle.constructor.name == 'model') {
      const { battleWallet, step, stepCycle } = await this.getStep(
        _battle,
        offset,
      );
      return battleWallet == wallet;
    }
  }

  async getCharacter(
    battle: DocumentBattle,
    characterId: string,
    wallet?: string,
    lived?: boolean,
    errors?: boolean,
  ): Promise<IBattleCharacter>;
  async getCharacter(
    battleId: string,
    characterId: string,
    wallet?: string,
    lived?: boolean,
    errors?: boolean,
  ): Promise<IBattleCharacter>;

  async getCharacter(
    _battle: string | DocumentBattle,
    characterId: string,
    wallet?: string,
    lived?: boolean,
    errors: boolean = true,
  ): Promise<IBattleCharacter> {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    if (!isValidObjectId(characterId)) {
      if (errors) {
        throw new BadRequestException(
          `Error value characterId ${characterId} as _id`,
        );
      } else {
        return null;
      }
    }

    let battleUser: IBattleUser;
    if (wallet) {
      battleUser = battle.battleUsers[wallet];
    } else {
      battleUser = Object.values(battle.battleUsers).find((battleUser) => {
        const squad = battleUser.squad;
        const character = squad[characterId];

        return character;
      });
    }

    if (!battleUser) {
      if (errors) {
        throw new NotFoundException(
          `getCharacter: User wallet '${wallet}' not found`,
        );
      } else {
        return null;
      }
    }

    const character = battleUser.squad[characterId];

    if (!character) {
      if (errors) {
        throw new NotFoundException(`Character id '${characterId}' not found`);
      } else {
        return null;
      }
    }

    if (lived) {
      if (character.health > 0) {
        return character;
      } else {
        if (errors) {
          throw new ConflictException(`Character "${character._id}" is death`);
        } else {
          return null;
        }
      }
    } else {
      return character;
    }
  }

  getCharacters(battle: DocumentBattle) {
    const battleUsers = battle.battleUsers;

    const characters: IBattleCharacter[] = [];

    for (const battleUser of Object.values(battleUsers)) {
      const battleUserSquad = battleUser.squad;

      characters.push(...Object.values(battleUserSquad));
    }

    return characters;
  }

  async getUserCharacters(
    battleId: string,
    userWallet?: string,
    lived?: boolean,
  ): Promise<IBattleCharacter[]>;
  async getUserCharacters(
    battle: DocumentBattle,
    userWallet?: string,
    lived?: boolean,
  ): Promise<IBattleCharacter[]>;

  async getUserCharacters(
    _battle: string | DocumentBattle,
    userWallet?: string,
    lived?: boolean,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    const battleUsers = battle.battleUsers;

    if (!userWallet) {
      userWallet = (await this.getStep(battle)).battleWallet;
    }

    const battleUser = battleUsers[userWallet];
    if (!battleUser)
      throw new NotFoundException(`Not found battleUser ${userWallet}`);

    const squad = battleUser.squad;

    if (lived) {
      return Object.values(squad).filter((character) => {
        return character.health > 0;
      });
    } else {
      return Object.values(squad);
    }
  }

  getField(wallet: string, characterId?: string, field?: string) {
    if (field) {
      return `battleUsers.${wallet}.squad.${characterId}.${field}`;
    } else {
      if (characterId) {
        return `battleUsers.${wallet}.squad.${characterId}`;
      }
      return `battleUsers.${wallet}`;
    }
  }

  async isUserLive(wallet: string, battleId: string): Promise<boolean>;
  async isUserLive(wallet: string, battle: DocumentBattle): Promise<boolean>;

  async isUserLive(wallet: string, _battle: string | DocumentBattle) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    const battleUser = battle.battleUsers[wallet];
    if (!battleUser)
      throw new NotFoundException(`Not found user battle wallet "${wallet}"`);

    const squad = battleUser.squad;

    return Object.values(squad).some((character) => {
      return character.health > 0 && !character.pet;
    });
  }

  async endBattle(
    battleId: string,
    winnerWallet?: string,
  ): Promise<{ winnerWallet: string; profit: Currencies }>;
  async endBattle(
    battle: DocumentBattle,
    winnerWallet?: string,
  ): Promise<{ winnerWallet: string; profit: Currencies }>;

  async endBattle(_battle: string | DocumentBattle, winnerWallet?: string) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    if (!winnerWallet) {
      const { battleWallet } = await this.getStep(battle, 1);
      winnerWallet = battleWallet;
    }

    this.removeInaction(battleId);

    let profit: Currencies = {};

    const expWin = this.env.BattleWinExp;
    const expLose = this.env.BattleLoseExp;

    if (battle.data.type != NotificationsTypeEnum.empty) {
      const battleUsers = battle.battleUsers;

      battle.state = BattleStateEnum.Finished;
      battle.winnerWallet = winnerWallet;

      // Группы вышли из схватки
      await this.squadModel.updateMany(
        {
          $or: Object.values(battleUsers).map((battleUser) => {
            return { _id: battleUser.squadId };
          }),
        },
        { $set: { state: SquadStateEnum.default } },
      );

      if (winnerWallet == Object.values(battleUsers)[1]?.wallet) {
        await this.squadModel.updateOne(
          { _id: Object.values(battleUsers)[1]?.squadId },
          { $set: { state: SquadStateEnum.defence } },
        );
      }
      
      // Награда в виде опыта
      const [userWallet, battleUser] = Object.entries(battleUsers)[0];
      if (userWallet != Neutral && battleUser.autoBattle == false) {
        const squad = battleUser.squad;

        const exp =
          userWallet == winnerWallet
            ? this.env.BattleWinExp
            : this.env.BattleLoseExp;

        for (const character of Object.values(squad)) {
          await this.characterService.addExp(character._id, exp);
        }
      }

      // Награда в виде ресурсов
      if (winnerWallet != Neutral) {
        if (battleUsers[winnerWallet].autoBattle == false) {
          const data = battle.data;
          const cell = await this.mapService.getCell(data.position);

          if (data.type == NotificationsTypeEnum.squadReadyCapture) {
            cell.userWallet = winnerWallet;

            await this.cellModel.updateOne(
              { _id: cell._id },
              { $set: { userWallet: cell.userWallet } },
            );
          }

          if (
            data.type == NotificationsTypeEnum.squadReadyCapture ||
            data.type == NotificationsTypeEnum.squadReadyRob
          ) {
            profit = await this.mapService.collectProfitToUser(
              cell.position,
              winnerWallet,
            );
          }
        }
      }
    } else {
      await this.battleModel.deleteOne({ _id: battleId });
    }

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battleWin,
        data: {
          type: BattleEventsEnum.EndBattle,
          battleId,
          wallet: winnerWallet,
          exp: expWin,
          profit,
        },
      },
      'Self',
    );

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battleLose,
        data: {
          type: BattleEventsEnum.EndBattle,
          battleId,
          wallet: winnerWallet,
          exp: expLose,
          profit,
        },
      },
      'Enemies',
    );

    await this.battleModel.updateOne(
      { _id: battleId },
      battle,
    );

    return { winnerWallet, profit };
  }

  //#endregion

  //#region LOGIC

  async battleNotification(
    battle: DocumentBattle,
    message: NotificationDTO,
    send: 'Self' | 'Enemies' | 'All',
  ) {
    const { battleWallet } = await this.getStep(battle);

    if (send == 'Self') {
      await this.notificationService.send({
        message,
        typeSend: NotificationTypeSendEnum.Self,
        wallet: battleWallet,
        save: true,
        temporary: true,
      });
    } else {
      for (const battleUserWallet of Object.keys(battle.battleUsers)) {
        if (
          battleUserWallet != Neutral &&
          (send == 'All' ||
            (send == 'Enemies' && battleUserWallet != battleWallet))
        ) {
          await this.notificationService.send({
            message,
            typeSend: NotificationTypeSendEnum.Self,
            wallet: battleUserWallet,
            save: true,
            temporary: true,
          });
        }
      }
    }
  }

  async characterRegenerationEnergy(
    battleId: string,
    characterId: string,
    wallet: string,
    value?: number,
  ): Promise<DocumentBattle>;
  async characterRegenerationEnergy(
    battle: DocumentBattle,
    characterId: string,
    wallet: string,
    value?: number,
  ): Promise<DocumentBattle>;

  async characterRegenerationEnergy(
    _battle: string | DocumentBattle,
    characterId: string,
    wallet: string,
    value = this.env.BattleRegenerationEnergy,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    const character = await this.getCharacter(battle, characterId, wallet);
    character.energy = Math.min(
      character.energy + value,
      this.env.BattleMaxEnergy,
    );

    const characterField = `battleUsers.${wallet}.squad.${characterId}`;

    if (type == 'string') {
      await this.battleModel.updateOne(
        { _id: battleId },
        { $set: { [`${characterField}.energy`]: character.energy } },
      );
    }

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battle,
        title: `empty`,
        data: {
          type: BattleEventsEnum.RecoveryAP,
          battleId,

          characterId,
          value,
        },
      },
      'Self',
    );

    return battle;
  }

  async userClearState(
    battleId: string,
    wallet: string,
  ): Promise<DocumentBattle>;
  async userClearState(
    battle: DocumentBattle,
    wallet: string,
  ): Promise<DocumentBattle>;

  async userClearState(_battle: string | DocumentBattle, wallet: string) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    const battleUser = battle.battleUsers[wallet];
    if (!battleUser)
      throw new NotFoundException(`Not found user battle wallet "${wallet}"`);

    const squad = battleUser.squad;

    Object.values(squad).forEach((battleCharacter) => {
      battleCharacter.state = BattleCharacterStateEnum.default;
    });

    const userField = `battleUsers.${wallet}`;

    if (type == 'string') {
      await this.battleModel.updateOne(
        { _id: battleId },
        { $set: { [`${userField}.squad`]: squad } },
      );
    }

    return battle;
  }

  async userRegenerationEnergy(
    battleId: string,
    wallet: string,
  ): Promise<DocumentBattle>;
  async userRegenerationEnergy(
    battle: DocumentBattle,
    wallet: string,
  ): Promise<DocumentBattle>;

  async userRegenerationEnergy(
    _battle: string | DocumentBattle,
    wallet: string,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    const battleUser = battle.battleUsers[wallet];
    if (!battleUser)
      throw new NotFoundException(`Not found user battle wallet "${wallet}"`);

    const squad = battleUser.squad;

    Object.values(squad).forEach((battleCharacter) => {
      battleCharacter.energy = Math.min(
        battleCharacter.energy + this.env.BattleRegenerationEnergy,
        this.env.BattleMaxEnergy,
      );
    });

    const userField = `battleUsers.${wallet}`;

    if (type == 'string') {
      await this.battleModel.updateOne(
        { _id: battleId },
        { $set: { [`${userField}.squad`]: squad } },
      );
    }

    return battle;
  }

  async recoveryHealth(
    battleId: string,
    characterId: string,
    health: number,
  ): Promise<DocumentBattle>;
  async recoveryHealth(
    battle: DocumentBattle,
    characterId: string,
    health: number,
  ): Promise<DocumentBattle>;
  async recoveryHealth(
    _battle: string | DocumentBattle,
    characterId: string,
    health: number,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    let multi = 1;
    const character = await this.getCharacter(battle, characterId);

    const buffs = character.buffs;

    for (const buff of buffs) {
      if (buff.name == BattleBuffsEnum.MultiRecoveryHealth) {
        multi *= buff.value;
      }
    }

    character.health += health * multi;

    await this.trigger({
      battle,
      event: BattleEventsEnum.RecoverHealth,
      characterId,
      targetId: characterId,
      value: health,
    });

    if (type == 'string') {
      await this.battleModel.updateOne(
        { _id: battleId },
        {
          $set: {
            [this.getField(character.userWallet, characterId, 'health')]:
              character.health,
          },
        },
      );
    }

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battle,
        title: `empty`,
        data: {
          type: BattleEventsEnum.RecoverHealth,
          battleId,

          characterId,
          health: health * multi,
        },
      },
      'All',
    );

    return battle;
  }

  async recoveryArmor(
    battleId: string,
    characterId: string,
    armor: number,
  ): Promise<DocumentBattle>;
  async recoveryArmor(
    battle: DocumentBattle,
    characterId: string,
    armor: number,
  ): Promise<DocumentBattle>;
  async recoveryArmor(
    _battle: string | DocumentBattle,
    characterId: string,
    armor: number,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    let multi = 1;
    const character = await this.getCharacter(battle, characterId);

    const buffs = character.buffs;

    for (const buff of buffs) {
      if (buff.name == BattleBuffsEnum.MultiRecoveryArmor) {
        multi *= buff.value;
      }
    }
    character.armor += armor * multi;

    await this.trigger({
      battle,
      event: BattleEventsEnum.RecoverArmor,
      characterId,
      targetId: characterId,
      value: armor,
    });

    if (type == 'string') {
      await this.battleModel.updateOne(
        { _id: battleId },
        {
          $set: {
            [this.getField(character.userWallet, characterId, 'armor')]:
              character.armor,
          },
        },
      );
    }

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battle,
        title: `empty`,
        data: {
          type: BattleEventsEnum.RecoverArmor,
          battleId,

          characterId,
          armor: armor * multi,
        },
      },
      'All',
    );

    return battle;
  }

  async recoveryShield(
    battleId: string,
    characterId: string,
    shield: number,
  ): Promise<DocumentBattle>;
  async recoveryShield(
    battle: DocumentBattle,
    characterId: string,
    shield: number,
  ): Promise<DocumentBattle>;
  async recoveryShield(
    _battle: string | DocumentBattle,
    characterId: string,
    shield: number,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    let multi = 1;
    const character = await this.getCharacter(battle, characterId);

    const buffs = character.buffs;

    for (const buff of buffs) {
      if (buff.name == BattleBuffsEnum.MultiRecoveryShield) {
        multi *= buff.value;
      }
    }
    character.shield += shield * multi;

    await this.trigger({
      battle,
      event: BattleEventsEnum.RecoverShield,
      characterId,
      targetId: characterId,
      value: shield,
    });

    if (type == 'string') {
      await this.battleModel.updateOne(
        { _id: battleId },
        {
          $set: {
            [this.getField(character.userWallet, characterId, 'shield')]:
              character.shield,
          },
        },
      );
    }

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battle,
        title: `empty`,
        data: {
          type: BattleEventsEnum.RecoverShield,
          battleId,

          characterId,
          shield: shield * multi,
        },
      },
      'All',
    );

    return battle;
  }

  async trigger({
    battle,
    event,
    value,
    characterId,
    targetId,
  }: {
    battle: string;
    event: BattleEventsEnum;
    characterId?: string;
    targetId?: string;
    value?: any;
  }): Promise<DocumentBattle>;
  async trigger({
    battle,
    event,
    value,
    characterId,
    targetId,
  }: {
    battle: DocumentBattle;
    event: BattleEventsEnum;
    characterId?: string;
    targetId?: string;
    value?: any;
  }): Promise<DocumentBattle>;

  async trigger({
    battle: _battle,
    event,
    value,
    characterId,
    targetId,
  }: {
    battle: string | DocumentBattle;
    event: BattleEventsEnum;
    characterId?: string; // Событие вызвал
    targetId?: string; // Событие направлено на
    value?: any; // Значени с которым был вызван триггер
  }) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    const battleRules = battle.rules;

    for (const battleRule of battleRules) {
      const {
        wallet: ruleWallet,
        characterId: ruleCharacterId,
        targetId: ruleTargetId,
        name: ruleName,
        rules,
        steps: ruleSteps,
        value: ruleValue, // Значение вызванного правила
      } = battleRule;

      const sources = this.getCharacters(battle);
      const targets = this.getCharacters(battle);

      function getValue(ruleValue: number | string, eventValue?: any): number {
        if (typeof ruleValue == 'number') {
          return ruleValue;
        } else {
          return eval(ruleValue)({ eventValue, triggerValue: value });
        }
      }

      const executeRules = async ({
        rule,
        triggerSources,
        triggerTargets,
        eventValue,
      }: {
        rule: BattleRuleType;
        triggerSources?: IBattleCharacter[];
        triggerTargets?: IBattleCharacter[];
        eventValue?: any;
      }) => {
        let _value: any;

        if (rule.value) {
          _value = getValue(rule.value, eventValue);
        }

        if (rule.type == 'Trigger') {
          // Trigger

          // triggerSources
          if (rule.source) {
            if (rule.source == EventSourceEnum.Self && ruleCharacterId) {
              triggerSources = triggerSources.filter((triggerSource) => {
                return CompareOID(triggerSource._id, ruleCharacterId);
              });
            }

            if (rule.source == EventSourceEnum.Friendly && ruleCharacterId) {
              const ruleCharacter = await this.getCharacter(
                battle,
                ruleCharacterId,
              );
              const characters = await this.getUserCharacters(
                battle,
                ruleCharacter.userWallet,
              );

              triggerSources = triggerSources.filter((triggerSource) => {
                return characters.find((character) => {
                  return CompareOID(triggerSource._id, character._id);
                });
              });
            }

            if (rule.source == EventSourceEnum.Enemies && ruleCharacterId) {
              const ruleCharacter = await this.getCharacter(
                battle,
                ruleCharacterId,
              );

              const enemyCharacters: IBattleCharacter[] = [];
              for (const wallet of Object.keys(battle.battleUsers)) {
                if (wallet != ruleCharacter.userWallet) {
                  enemyCharacters.push(
                    ...(await this.getUserCharacters(battle, wallet)),
                  );
                }
              }

              triggerSources = triggerSources.filter((triggerSource) => {
                return enemyCharacters.find((character) => {
                  return CompareOID(triggerSource._id, character._id);
                });
              });
            }
          } else {
            if (characterId) {
              triggerSources = triggerSources.filter((source) => {
                return CompareOID(source._id, characterId);
              });
            }
          }

          // triggerTargets
          if (rule.target) {
            if (ruleCharacterId) {
              if (rule.target == EventTargetEnum.Self) {
                triggerTargets = triggerTargets.filter((triggerTarget) => {
                  return CompareOID(triggerTarget._id, ruleCharacterId);
                });
              }

              if (rule.target == EventTargetEnum.OtherSelf) {
                triggerTargets = triggerTargets.filter((triggerTarget) => {
                  return !CompareOID(triggerTarget._id, ruleCharacterId);
                });
              }

              if (rule.target == EventTargetEnum.Friendly) {
                const ruleCharacter = await this.getCharacter(
                  battle,
                  ruleCharacterId,
                );
                const characters = await this.getUserCharacters(
                  battle,
                  ruleCharacter.userWallet,
                );

                triggerTargets = triggerTargets.filter((triggerTarget) => {
                  return characters.find((character) => {
                    return CompareOID(triggerTarget._id, character._id);
                  });
                });
              }

              if (rule.target == EventTargetEnum.Enemies) {
                const ruleCharacter = await this.getCharacter(
                  battle,
                  ruleCharacterId,
                );

                const enemyCharacters: IBattleCharacter[] = [];
                for (const wallet of Object.keys(battle.battleUsers)) {
                  if (wallet != ruleCharacter.userWallet) {
                    enemyCharacters.push(
                      ...(await this.getUserCharacters(battle, wallet)),
                    );
                  }
                }

                triggerTargets = triggerTargets.filter((triggerTarget) => {
                  return enemyCharacters.find((character) => {
                    return CompareOID(triggerTarget._id, character._id);
                  });
                });
              }
            }

            if (ruleTargetId) {
              if (rule.target == EventTargetEnum.Target) {
                triggerTargets = triggerTargets.filter((triggerTarget) => {
                  return CompareOID(triggerTarget._id, ruleTargetId);
                });
              }

              if (rule.target == EventTargetEnum.OtherTarget) {
                triggerTargets = triggerTargets.filter((triggerTarget) => {
                  return !CompareOID(triggerTarget._id, ruleTargetId);
                });
              }
            }

            if (
              rule.target == EventTargetEnum.OtherSelfTarget &&
              ruleCharacterId &&
              ruleTargetId
            ) {
              triggerTargets = triggerTargets.filter((triggerTarget) => {
                return (
                  !CompareOID(triggerTarget._id, ruleCharacterId) &&
                  !CompareOID(triggerTarget._id, ruleTargetId)
                );
              });
            }
          } else {
            if (targetId) {
              triggerTargets = triggerTargets.filter((target) => {
                return CompareOID(target._id, targetId);
              });
            }
          }

          //let triggerValue: number;

          if (rule.event == BattleEventsEnum.Count) {
            if (rule.value && typeof rule.value == 'number') {
              rule.value--;
            } else {
              return;
            }
          }

          if (rule.event == BattleEventsEnum.Random) {
            if (Math.random() >= _value) {
              return;
            }
          }

          if (rule.event == BattleEventsEnum.MainRuleBuffsReset) {
            if (_value) {
              await this.resetBuffsRule(battle, rule);
              return;
            }
          }

          if (
            rule.event == BattleEventsEnum.Definition ||
            rule.event == BattleEventsEnum.Attack ||
            rule.event == BattleEventsEnum.PreAttack ||
            rule.event == BattleEventsEnum.PreSubAttack ||
            rule.event == BattleEventsEnum.SubAttack ||
            rule.event == BattleEventsEnum.BaseAttack ||
            rule.event == BattleEventsEnum.HeavyAttack
          ) {

            if (rule.event != event) return;

            if (rule.source) {
              if (
                rule.source == EventSourceEnum.Self &&
                characterId &&
                !triggerSources.find((source) => {
                  return CompareOID(source._id, characterId);
                }) 
              )
                return;

              if (rule.source == EventSourceEnum.Friendly && characterId) {
                if (
                  !triggerSources.find((source) => {
                    return CompareOID(source._id, characterId);
                  })
                )
                  return;
              }

              if (rule.source == EventSourceEnum.Enemies && characterId) {

                if (
                  !triggerSources.find((character) => {
                    return CompareOID(character._id, characterId);
                  })
                )
                  return;
              }
            }

            if (rule.target) {
              if (
                rule.target == EventTargetEnum.Target &&
                targetId &&
                !triggerTargets.find((target) => {
                  return CompareOID(target._id, targetId);
                })
              )
                return;

              if (rule.target == EventTargetEnum.Friendly && targetId) {
                if (
                  !triggerTargets.find((character) => {
                    return CompareOID(character._id, targetId);
                  })
                )
                  return;
              }

              if (rule.target == EventTargetEnum.Enemies && targetId) {

                if (
                  !triggerTargets.find((character) => {
                    return CompareOID(character._id, targetId);
                  })
                )
                  return;
              }
            }

          }


          if (rule.triggers) {
            for (const trigger of rule.triggers) {
              await executeRules({
                rule: trigger,
                triggerSources,
                triggerTargets,
                eventValue,
              });
            }
          }
          if (rule.buffs) {
            for (const buff of rule.buffs) {
              await executeRules({
                rule: buff,
                triggerSources,
                triggerTargets,
                eventValue,
              });
            }
          }
        } else {

          if (!rule.ids) {
            rule.ids = [];
          }

          if (rule.target) {
            triggerTargets = this.getCharacters(battle);

            if (rule.target == BuffTargetEnum.RuleSource && ruleCharacterId) {
              triggerTargets = triggerTargets.filter((triggerTarget) => {
                return CompareOID(triggerTarget._id, ruleCharacterId);
              });
            }

            if (rule.target == BuffTargetEnum.Source && characterId) {
              triggerTargets = triggerTargets.filter((triggerTarget) => {
                return CompareOID(triggerTarget._id, characterId);
              });
            }

            if (ruleTargetId) {
              if (rule.target == BuffTargetEnum.Target) {
                triggerTargets = triggerTargets.filter((triggerTarget) => {
                  return CompareOID(triggerTarget._id, ruleTargetId);
                });
              }

            } else {
              if (rule.target == BuffTargetEnum.Target) {
                triggerTargets = [
                  triggerTargets.find((triggerTarget) => {
                    return CompareOID(triggerTarget._id, targetId);
                  }),
                ];
              }
            }

          }

          for (const triggerTarget of triggerTargets) {
            if (rule.name == BattleBuffsEnum.BaseAttack) {
              if (
                ruleCharacterId &&
                !CompareOID(characterId, ruleCharacterId)
              ) {

                try {
                  await this.attack(
                    battle,
                    ruleCharacterId,
                    triggerTarget._id,
                    AttackTypeEnum.BaseAttack,
                    undefined,
                    false,
                    true,
                  );
                } catch (err) {
                  console.log(err);
                }
                //}
              }
            } else {
              if (rule.individual != true) {
                rule.ids.push({
                  targetId: triggerTarget._id,
                  buffId: battle.buffId,
                });
              }

              await this.applyBuff(
                battle,
                triggerTarget,
                rule.name,
                rule.steps,
                rule.individual,
                _value, 
              );
            }

          }
        }
      };

      for (const rule of rules) {
        await executeRules({
          rule,
          triggerSources: sources,
          triggerTargets: targets,
          //eventValue: value,
        });
      }
    }

    if (type == 'string') {
      await this.battleModel.updateOne({ _id: battle._id }, battle);
    }

    return battle;
  }

  async applyBuff(
    battle: DocumentBattle,
    character: IBattleCharacter,
    name: BattleBuffsEnum,
    steps: number,
    individual: boolean,
    value?: any,
  ) {
    if (
      steps != null ||
      (name != BattleBuffsEnum.Empty &&
        name != BattleBuffsEnum.RecoveryShield &&
        name != BattleBuffsEnum.RecoveryAP)
    ) {
      character.buffs.push({
        buffId: battle.buffId,
        name,
        steps,
        individual,
        value,
      });
    }

    if (name == BattleBuffsEnum.MultiDamage) {
      character.weaponStats[AttackTypeEnum.BaseAttack].modAttack *= value;
      const heavyAttack = character.weaponStats[AttackTypeEnum.HeavyAttack];
      if (heavyAttack) {
        heavyAttack.modAttack *= value;
      }
    }
    if (name == BattleBuffsEnum.AddAccuracy) {
      character.weaponStats[AttackTypeEnum.BaseAttack].accuracy += value;

      const heavyAttack = character.weaponStats[AttackTypeEnum.HeavyAttack];
      if (heavyAttack) {
        heavyAttack.accuracy += value;
      }
    }

    if (name == BattleBuffsEnum.RecoveryAP) {
      character.energy += value;
    }

    if (name == BattleBuffsEnum.RecoveryHealth) {
      await this.recoveryHealth(battle, character._id, value);
    }
    if (name == BattleBuffsEnum.RecoveryArmor) {
      await this.recoveryArmor(battle, character._id, value);
    }
    if (name == BattleBuffsEnum.RecoveryShield) {
      await this.recoveryShield(battle, character._id, value);
    }

    if (name == BattleBuffsEnum.MultiRecoveryShield) {
    }

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battle,
        title: `empty`,
        data: {
          type: BattleEventsEnum.BuffCreate,
          battleId: battle._id,

          characterId: character._id,
          name,
          steps,
          individual,
          value,
        },
      },
      'All',
    );

    battle.buffId++;
  }

  async applyRule({
    battle,
    wallet,
    name,
    rule,
    characterId,
    targetId,
    steps,
    value,
  }: {
    battle: string;
    wallet: string;
    name: BattleBuffsEnum | AbilityEnum | RuleEnum;
    rule: BattleRuleType | BattleRuleType[];

    characterId: string;
    targetId?: string;

    steps?: number;
    value?: number | string; 
  }): Promise<DocumentBattle>;
  async applyRule({
    battle,
    wallet,
    name,
    rule,
    characterId,
    targetId,
    steps,
    value,
  }: {
    battle: DocumentBattle;
    wallet: string;
    name: BattleBuffsEnum | AbilityEnum | RuleEnum;
    rule: BattleRuleType | BattleRuleType[];

    characterId: string;
    targetId?: string;

    steps?: number;
    value?: number | string; 
  }): Promise<DocumentBattle>;

  async applyRule(
    {
      battle: _battle,
      wallet,
      name,
      rule,
      characterId,
      targetId,
      steps,
      value,
    }: {
      battle: string | DocumentBattle;
      wallet: string;
      name: BattleBuffsEnum | AbilityEnum | RuleEnum;
      rule: BattleRuleType | BattleRuleType[];

      characterId: string;
      targetId?: string;

      steps?: number;
      value?: number | string; 
    } ,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    let _rules: BattleRuleType[];
    if (Array.isArray(rule)) {
      _rules = rule;
    } else {
      _rules = [rule];
    }

    battle.rules.push({
      wallet,
      characterId,
      targetId,

      name,
      rules: _rules,

      steps,
      value,
    });

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battle,
        title: `empty`,
        data: {
          type: BattleEventsEnum.CreateRule,
          battleId,

          name,
          rule: _rules,
          characterId,
          targetId,
          steps,
          value,
        },
      },
      'All',
    );
    //});

    if (type == 'string') {
      await this.battleModel.updateOne(
        { _id: battleId },
        { $set: { rules: battle.rules } },
      );
    }

    return battle;
  }

  async tickAbilities(
    battle: DocumentBattle,
    wallet?: string,
  ): Promise<DocumentBattle>;
  async tickAbilities(
    battleId: string,
    wallet?: string,
  ): Promise<DocumentBattle>;

  async tickAbilities(_battle: string | DocumentBattle, wallet?: string) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    let battleWallet: string;
    if (wallet) {
      battleWallet = wallet;
    } else {
      battleWallet = (await this.getStep(battle)).battleWallet;
    }

    const characters = await this.getUserCharacters(battle, battleWallet);

    for (const character of characters) {
      const characterWeaponStats = character.weaponStats;

      if (characterWeaponStats.baseAttack) {
        if (characterWeaponStats.baseAttack.remaining > 0)
          characterWeaponStats.baseAttack.remaining--;
      }

      if (characterWeaponStats.heavyAttack) {
        if (characterWeaponStats.heavyAttack.remaining > 0)
          characterWeaponStats.heavyAttack.remaining--;
      }

      const characterAbilities = character.abilities;
      if (characterAbilities) {
        for (const characterAbility of Object.values(characterAbilities)) {
          if (characterAbility.remaining > 0) characterAbility.remaining--;
        }
      }
    }

    return battle;
  }

  async tickBuffs(
    battle: DocumentBattle,
    wallet?: string,
  ): Promise<DocumentBattle>;
  async tickBuffs(battleId: string, wallet?: string): Promise<DocumentBattle>;

  async tickBuffs(_battle: string | DocumentBattle, wallet?: string) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    let battleWallet: string;
    if (wallet) {
      battleWallet = wallet;
    } else {
      battleWallet = (await this.getStep(battle)).battleWallet;
    }

    let rules = battle.rules;

    rules = rules.filter((rule, index) => {
      if (rule.wallet === battleWallet && rule.steps !== null) {
        rule.steps -= 1;
      }
      if (rule.steps > 0 || rule.steps === null) {
        return true;
      } else {
        this.endRule(battle, rule, index);
        return false;
      }
    });

    const characters = await this.getUserCharacters(battle, battleWallet);

    for (const character of characters) {
      character.buffs.forEach((buff) => {
        if (buff.steps) {
          buff.steps -= 1;
        }
      });

      await this.endBuff(
        battle,
        character,
        character.buffs.filter((buff) => {
          return buff.steps <= 0 && buff.steps != null;
        }),
      );
    }

    battle.rules = rules.filter((rule) => {
      return rule.steps > 0 || rule.steps === null;
    });

    if (typeof _battle == 'string') {
      await this.battleModel.updateOne({ _id: battleId }, battle); 
    }

    return battle;
  }

  async resetBuffsRule(battle: DocumentBattle, rule: BattleRuleType) {

    const executeRules = async (rule: BattleRuleType) => {
      if (rule.type == 'Trigger') {
        if (rule.triggers) {
          for (const ruleTrigger of rule.triggers) {
            await executeRules(ruleTrigger);
          }
        }
        if (rule.buffs) {
          for (const ruleBuffs of rule.buffs) {
            await executeRules(ruleBuffs);
          }
        }
      } else {
        if (rule.ids) {
          for (const { buffId, targetId } of rule.ids) {
            const target = await this.getCharacter(battle, targetId);

            const removeBuff = target.buffs.find((buff) => {
              return buff.buffId == buffId && !buff.individual;
            });

            if (removeBuff) {
              await this.endBuff(
                battle,
                target,
                [removeBuff],
              );
            }
          }
          rule.ids = [];
        }
      }
    };

    await executeRules(rule);

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battle,
        title: `empty`,
        data: {
          type: BattleEventsEnum.MainRuleBuffsReset,
          battleId: battle._id,

          rule,
        },
      },
      'Self',
    );

    return battle;

  }

  async endRule(
    battle: DocumentBattle,
    battleRules: BattleMainRule,
    index: number,
  ) {
    const rules = battleRules.rules;

    const executeRules = async (rule: BattleRuleType) => {
      if (rule.type == 'Trigger') {
        if (rule.triggers) {
          for (const ruleTrigger of rule.triggers) {
            await executeRules(ruleTrigger);
          }
        }
        if (rule.buffs) {
          for (const ruleBuffs of rule.buffs) {
            await executeRules(ruleBuffs);
          }
        }
      } else {
        if (rule.ids) {
          for (const { buffId, targetId } of rule.ids) {
            const target = await this.getCharacter(
              battle,
              targetId,
              undefined,
              undefined,
              false,
            );

            if (target) {
              await this.endBuff(
                battle,
                target,
                target.buffs.filter((buff) => {
                  return buff.buffId != buffId || buff.individual;
                }),
              );
            } else {
              console.log('endBuffError');
              console.log(battle, battleRules, index);
            }
          }
        }
      }
    };

    for (const rule of rules) {
      await executeRules(rule);
    }

    rules.splice(index, 1);

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battle,
        title: `empty`,
        data: {
          type: BattleEventsEnum.RemoveRule,
          battleId: battle._id,

          battleRules,
          index,
        },
      },
      'All',
    );

    return true;

  }

  async endBuff(
    battle: DocumentBattle,
    character: IBattleCharacter,
    buffs: BattleCharacterBuffType[],
  ) {
    if (buffs.length == 0) return character;

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battle,
        title: `empty`,
        data: {
          type: BattleEventsEnum.BuffExpired,
          battleId: battle._id,

          characterId: character._id,
          buffs,
        },
      },
      'All',
    );

    for (const buff of buffs) {
      if (buff.name == BattleBuffsEnum.MultiDamage) {

        character.weaponStats[AttackTypeEnum.BaseAttack].modAttack /=
          buff.value;

        const heavyAttack = character.weaponStats[AttackTypeEnum.HeavyAttack];
        if (heavyAttack) {
          heavyAttack.modAttack /= buff.value;
        }
      }
      if (buff.name == BattleBuffsEnum.AddAccuracy) {
        character.weaponStats[AttackTypeEnum.BaseAttack].accuracy -= buff.value;

        const heavyAttack = character.weaponStats[AttackTypeEnum.HeavyAttack];
        if (heavyAttack) {
          heavyAttack.accuracy -= buff.value;
        }
      }

      if (buff.name == BattleBuffsEnum.AddHealth) {
        character.health -= buff.value;
        if (character.health < 1) {
          character.health = 1;
        }
      }
      if (buff.name == BattleBuffsEnum.AddArmor) {
        character.armor -= buff.value;
      }
      if (buff.name == BattleBuffsEnum.AddShield) {
        character.shield -= buff.value;
      }
    }

    character.buffs = character.buffs.filter((removeBuff) => {
      return !buffs.find((buff) => {
        return buff.buffId == removeBuff.buffId;
      });
    });

    return character;
  }
  //#endregion

  //#region ACTION
  async skip(
    battleId: string,
    characterId: string,
    wallet?: string,
  ): Promise<{ battleId: string; characterId: string }>;
  async skip(
    battle: DocumentBattle,
    characterId: string,
    wallet?: string,
  ): Promise<{ battleId: string; characterId: string }>;

  async skip(
    _battle: string | DocumentBattle,
    characterId: string,
    wallet?: string,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    if (wallet && !this.stepIs(battle, wallet))
      throw new ConflictException(`Now it's anouther player's turn`);

    const character = await this.getCharacter(battle, characterId);

    if (character.health == 0)
      throw new ConflictException(`Character id '${characterId}' is death`);

    if (character.state != BattleCharacterStateEnum.default)
      throw new ConflictException(`The character has already made his move`);

    if (
      character.energy >=
      this.env.BattleMaxEnergy - this.env.BattleRegenerationEnergy
    )
      throw new ImATeapotException(`It won't have any effect`);

    character.state = BattleCharacterStateEnum.skip;

    if (type == 'string') {
      await this.battleModel.updateOne(
        { _id: battleId },
        {
          $set: {
            [this.getField(character.userWallet, character._id, 'state')]:
              character.state,
          },
        },
      );
    }

    await this.trigger({
      battle,
      event: BattleEventsEnum.Skip,
      characterId,
    });

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battle,
        title: `empty`,
        data: {
          type: BattleEventsEnum.Skip,
          battleId: battle._id,

          characterId,
        },
      },
      'All',
    );

    return { battleId, characterId };
  }

  async getCharacterWeapon(
    character: IBattleCharacter,
    typeAttack: AttackTypeEnum,
  ): Promise<IBattleWeaponStateAttack>;
  async getCharacterWeapon(
    character: IBattleCharacter,
  ): Promise<IBattleWeaponState>;

  async getCharacterWeapon(
    character: IBattleCharacter,
    typeAttack?: AttackTypeEnum,
  ) {
    if (typeAttack) {
      const weapon = character.weaponStats;

      const weaponAttack = weapon[typeAttack];

      return weaponAttack;
    }
    if (!typeAttack) {
      const weapon = character.weaponStats;

      return weapon;
    }
  }

  async attack(
    battleId: string,
    characterId: string,
    targetId: string,
    typeAttack: AttackTypeEnum,

    wallet?: string,
    save?: boolean,
    energyNoRequired?: boolean,
  ): Promise<BattleResponseAttack>;
  async attack(
    battle: DocumentBattle,
    characterId: string,
    targetId: string,
    typeAttack: AttackTypeEnum,

    wallet?: string,
    save?: boolean,
    energyNoRequired?: boolean,
  ): Promise<BattleResponseAttack>;

  async attack(
    _battle: string | DocumentBattle,
    characterId: string,
    targetId: string,
    typeAttack: AttackTypeEnum,

    wallet?: string,
    save = false,
    energyNoRequired: boolean = false,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    if (!isValidObjectId(characterId))
      throw new BadRequestException(
        `Error value characterId ${characterId} as _id`,
      );

    if (!isValidObjectId(targetId))
      throw new BadRequestException(`Error value targetId ${targetId} as _id`);

    if (wallet) {
      const { battleWallet } = await this.getStep(battle);

      if (wallet !== battleWallet)
        throw new ForbiddenException(
          'Wallet address does not match current step address',
        );
    }

    const character = await this.getCharacter(battle, characterId, wallet);
    if (!character)
      throw new NotFoundException(`Character id '${characterId}' is not found`);

    if (character.health <= 0)
      throw new ConflictException(`Character id '${characterId}' is death`);

    if (wallet) {
      if (character.pet)
        throw new ConflictException(`Character id '${characterId}' is pet`);
    }

    if (character.state == BattleCharacterStateEnum.skip)
      throw new ConflictException(`Character id '${characterId} is skipped`);

    const target = await this.getCharacter(battle, targetId);
    if (!target)
      throw new NotFoundException(`Target id '${targetId}' is not found`);

    if (target.pet)
      throw new ConflictException(`Target id '${targetId}' is pet`);

    if (target.health <= 0)
      throw new ConflictException(`Target id '${targetId}' is death`);

    if (energyNoRequired === false) {
      if (typeAttack == AttackTypeEnum.BaseAttack) {
        if (character.energy < 2)
          throw new ConflictException(
            `Not enough energy '${characterId}' ${typeAttack}`,
          );
        character.energy -= 2;
      } else {
        if (character.energy < 3)
          throw new ConflictException(
            `Not enough energy '${characterId}' ${typeAttack}`,
          );
        character.energy -= 3;
      }
    }

    character.state = BattleCharacterStateEnum.step;

    const weapon = character.weaponStats;

    let full = 0;
    let fullDamageHealth = 0;
    let fullDamageArmor = 0;
    let fullDamageShield = 0;
    const attacks: SubAttackType[] = [];

    const weaponAttack = { ...weapon[typeAttack] };

    if (weaponAttack.remaining > 0)
      throw new ConflictException(`Attack "${typeAttack}" is on cooldown`);

    weapon[typeAttack].remaining = weaponAttack.reload;

    if (weaponAttack.buffs) {
      for (const buffName of weaponAttack.buffs) {
        const rule = Rules[buffName];

        await this.applyRule({
          battle,
          wallet,
          name: buffName,
          rule,
          characterId,
          targetId,
          steps: 1,
        });
      }
    }

    await this.trigger({
      battle,
      event: BattleEventsEnum.PreAttack,
      characterId,
      targetId,
      value: weaponAttack,
    });

    for (let i = 0; i < weaponAttack.count; i++) {
      const subattack = {
        accuracy: weaponAttack.accuracy,
        damage: character.attack * weaponAttack.modAttack,
      };

      await this.trigger({
        battle,
        event: BattleEventsEnum.PreSubAttack,
        characterId,
        targetId,
        value: subattack,
      });

      const accuracy = subattack.accuracy;
      let damage = subattack.damage;

      const subAttack: SubAttackType = {
        miss: false,
        damage: 0,
        damageHealth: 0,
        damageArmor: 0,
        damageShield: 0,
      };

      if (Math.random() < accuracy) {
        if (target.shield > 0) {
          if (target.shield > damage) {
            target.shield -= damage;

            subAttack.damage += damage;
            subAttack.damageShield += damage;

            full += damage;
            fullDamageShield += damage;

            damage = 0;
          } else {
            damage -= target.shield;

            subAttack.damage += target.shield;
            subAttack.damageShield += target.shield;

            full += target.shield;
            fullDamageShield += target.shield;

            target.shield = 0;
          }
        }

        if (target.armor > 0) {
          if (target.armor > damage) {
            target.armor -= damage;

            subAttack.damage += damage;
            subAttack.damageArmor += damage;

            full += damage;
            fullDamageArmor += damage;

            damage = 0;
          } else {
            damage -= target.armor;

            subAttack.damage += target.armor;
            subAttack.damageArmor += target.armor;

            full += target.armor;
            fullDamageArmor += target.armor;

            target.armor = 0;
          }
        }

        if (target.health > 0) {
          if (target.health > damage) {
            target.health -= damage;

            subAttack.damage += damage;
            subAttack.damageHealth += damage;

            full += damage;
            fullDamageHealth += damage;

            damage = 0;
          } else {
            damage -= target.health;

            subAttack.damage += target.health;
            subAttack.damageHealth += target.health;

            full += target.health;
            fullDamageHealth += target.health;

            target.health = 0;
          }
        }
      } else {
        subAttack.miss = true;
      }

      attacks.push(subAttack);

      await this.trigger({
        battle,
        event: BattleEventsEnum.SubAttack,
        characterId,
        targetId,
        value: subAttack,
      });
    }

    const response: BattleResponseAttack = {
      battleId,
      type: typeAttack,

      characterId,
      targetId,

      full,
      fullDamageHealth,
      fullDamageArmor,
      fullDamageShield,
      attacks,

      energy: character.energy,
    };

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battle,
        title: `${character.archetype} attack ${target.archetype}`,
        data: response,
      },
      'All',
    );

    const triggerValue: TriggerValueAttack = {
      typeAttack,
      full,
      fullDamageHealth,
      fullDamageArmor,
      fullDamageShield,
      attacks,
    };

    await this.trigger({
      battle,
      event: BattleEventsEnum.Attack,
      characterId,
      targetId,
      value: triggerValue,
    });

    await this.trigger({
      battle,
      event: BattleEventsEnum.TakeDamage,
      characterId: targetId,
      targetId: characterId,
      value: triggerValue,
    });

    if (fullDamageHealth) {
      let damage = fullDamageHealth;

      const buffs = target.buffs;
      for (const buff of buffs) {
        if (buff.name == BattleBuffsEnum.AddHealth) {
          if (damage > buff.value) {
            damage -= buff.value;
            buff.value = 0;
          } else {
            buff.value -= damage;
            damage = 0;
            break;
          }
        }
      }

      await this.trigger({
        battle,
        event: BattleEventsEnum.TakeDamageHealth,
        characterId: targetId,
        targetId: characterId,
        value: triggerValue,
      });
    }
    if (fullDamageArmor) {
      let damage = fullDamageArmor;

      const buffs = target.buffs;
      for (const buff of buffs) {
        if (buff.name == BattleBuffsEnum.AddArmor) {
          if (damage > buff.value) {
            damage -= buff.value;
            buff.value = 0;
          } else {
            buff.value -= damage;
            damage = 0;
            break;
          }
        }
      }

      await this.trigger({
        battle,
        event: BattleEventsEnum.TakeDamageArmor,
        characterId: targetId,
        targetId: characterId,
        value: triggerValue,
      });
    }
    if (fullDamageShield) {
      let damage = fullDamageShield;

      const buffs = target.buffs;
      for (const buff of buffs) {
        if (buff.name == BattleBuffsEnum.AddShield) {
          if (damage > buff.value) {
            damage -= buff.value;
            buff.value = 0;
          } else {
            buff.value -= damage;
            damage = 0;
            break;
          }
        }
      }

      await this.trigger({
        battle,
        event: BattleEventsEnum.TakeDamageShield,
        characterId: targetId,
        targetId: characterId,
        value: triggerValue,
      });
    }

    if (save) {
      await this.battleModel.updateOne({ _id: battleId }, battle);
    }

    return response;
  }

  async defence(
    battleId: string,
    characterId: string,
    wallet?: string,
  ): Promise<{ battleId: string; characterId: string; shield: number }>;
  async defence(
    battle: DocumentBattle,
    characterId: string,
    wallet?: string,
  ): Promise<{ battleId: string; characterId: string; shield: number }>;

  async defence(
    _battle: string | DocumentBattle,
    characterId: string,
    wallet?: string,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    if (wallet && !this.stepIs(battle, wallet))
      //wallet !== battleWallet)
      throw new ForbiddenException(
        'Wallet address does not match current step address',
      );

    const character = await this.getCharacter(battle, characterId);
    if (!character)
      throw new NotFoundException(
        `Character id '${characterId}' is not found or death`,
      );

    if (character.health == 0)
      throw new ConflictException(`Character id '${characterId}' is death`);

    if (character.energy < 1) throw new ConflictException(`Not enough energy`);

    if (character.state == BattleCharacterStateEnum.skip)
      throw new ConflictException(`Character id '${characterId} is skipped`);

    if (character.original.health * character.modArmor == 0)
      throw new ImATeapotException(`It won't have any effect`);

    const shield = character.original.health * character.modArmor;

    //character.state = BattleCharacterStateEnum.defence;
    character.state = BattleCharacterStateEnum.step;
    character.shield += shield;
    character.energy -= 1;

    await this.applyBuff(
      battle,
      character,
      BattleBuffsEnum.AddShield,
      1,
      false,
      shield,
    );

    await this.trigger({
      battle,
      event: BattleEventsEnum.Defence,
      characterId,
    });

    if (type == 'string') {
      await this.battleModel.updateOne(
        { _id: battleId },
        battle,
      );
    }

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battle,
        title: `empty`,
        data: {
          type: BattleEventsEnum.Defence,
          battleId,
          characterId,
          shield,
        },
      },
      'All',
    );

    return { battleId, characterId, shield };
  }

  async abilityArchytype(
    wallet: string,
    battleId: string,
    abilityName: AbilityEnum,
    characterId: string,
    targetId?: string,
    passiveIgnore?: boolean,
  ): Promise<{
    battleId: string;

    abilityName: string;
    characterId: string;
    targetId: string;
  }>;
  async abilityArchytype(
    wallet: string,
    battle: DocumentBattle,
    abilityName: AbilityEnum,
    characterId: string,
    targetId?: string,
    passiveIgnore?: boolean,
  ): Promise<{
    battleId: string;

    abilityName: string;
    characterId: string;
    targetId: string;
  }>;

  async abilityArchytype(
    wallet: string,
    _battle: string | DocumentBattle,
    abilityName: AbilityEnum,
    characterId: string,
    targetId?: string,
    passiveIgnore = false,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);

    if (!isValidObjectId(characterId))
      throw new BadRequestException(
        `Error value characterId ${characterId} as _id`,
      );

    if (targetId) {
      if (!isValidObjectId(targetId))
        throw new BadRequestException(
          `Error value targetId ${targetId} as _id`,
        );
    }

    const character = await this.getCharacter(battle, characterId, wallet);

    if (character.state == BattleCharacterStateEnum.skip)
      throw new ConflictException(`Character id '${characterId} is skipped`);

    character.state = BattleCharacterStateEnum.step;

    const characterAbilities: BattleCharacterAbilities = character.abilities;

    if (!characterAbilities)
      throw new NotFoundException(`Character has no ability`);

    const characterAbility = Object.values(characterAbilities).find(
      (characterAbility) => {
        return characterAbility.name == abilityName;
      },
    );

    if (!characterAbility)
      throw new NotFoundException(
        `Ability "${abilityName}" character not found`,
      );

    if (characterAbility.passive == true && passiveIgnore == false)
      throw new ConflictException(`Ability "${abilityName}" is passive`);

    if (characterAbility.remaining > 0)
      throw new ConflictException(`Ability "${abilityName}" is on cooldown`);

    const ability = Abilities[abilityName];

    if (!ability)
      throw new NotFoundException(
        `Descriptions ability not found  "${abilityName}"`,
      );

    const targetEnergy = await (async () => {
      if (character.ownerId) {
        return this.getCharacter(battle, character.ownerId);
      } else {
        return character;
      }
    })();

    if (targetEnergy.energy < characterAbility.energy)
      throw new ConflictException(
        `Not enough energy to use the ability ${abilityName}`,
      );

    targetEnergy.energy -= characterAbility.energy;

    await this.battleModel.updateOne(
      { _id: battleId },
      {
        [this.getField(targetEnergy.userWallet, targetEnergy._id, 'energy')]:
          targetEnergy.energy,
      },
    );

    characterAbility.remaining = characterAbility.reload;

    const rules = ability.leveling[characterAbility.level - 1];

    await this.applyRule({
      battle,
      wallet,

      characterId,
      targetId,

      name: abilityName,
      rule: rules,

      steps: ability.steps,
    });

    await this.trigger({
      battle,
      event: BattleEventsEnum.UseAbility,
      characterId,
      targetId,
      value: { abilityName },
    });

    if (type == 'string') {
      await this.battleModel.updateOne({ _id: battleId }, battle);
    }

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battle,
        title: `empty`,
        data: {
          type: BattleEventsEnum.UseAbility,
          battleId,

          abilityName,
          characterId,
          targetId,
        },
      },
      'All',
    );

    return {
      battleId,

      abilityName,
      characterId,
      targetId,
    }; //battle;
  }

  async isAvailableAttack(
    _battle: string | DocumentBattle,
    wallet?: string,
    characterId?: string,
    typeAttack?: AttackTypeEnum,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);
    if (!wallet) wallet = (await this.getStep(battle)).battleWallet;

    const checkAttack = async (
      character: IBattleCharacter,
      typeAttack?: AttackTypeEnum,
    ) => {
      if (character.state == BattleCharacterStateEnum.skip) return false;

      const energy = character.energy;

      const weapon = await this.getCharacterWeapon(character);
      if (typeAttack) {
        return (
          weapon[typeAttack] &&
          energy >= weapon[typeAttack].energy &&
          weapon[typeAttack].remaining == 0
        );
      }
      if (!weapon.baseAttack && !weapon.heavyAttack) return false;
      return (
        (!weapon.baseAttack ||
          (energy >= weapon.baseAttack.energy &&
            weapon.baseAttack.remaining == 0)) &&
        (!weapon.heavyAttack ||
          (energy >= weapon.heavyAttack.energy &&
            weapon.heavyAttack.remaining == 0))
      );
    };

    if (characterId) {
      const character = await this.getCharacter(battle, characterId);

      return checkAttack(character, typeAttack);
    }
    const characters = await this.getUserCharacters(battle, wallet);

    return (
      await Promise.all(
        characters.map((character) => {
          return checkAttack(character, typeAttack);
        }),
      )
    ).every((checkedAttack) => checkedAttack);
  }

  async isAvailableAbility(
    _battle: string | DocumentBattle,
    wallet?: string,
    characterId?: string,
    abilityName?: AbilityEnum,
    handlerError?: boolean,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);
    if (!wallet) wallet = (await this.getStep(battle)).battleWallet;

    const checkAbility = async (
      character: IBattleCharacter,
      abilityName?: AbilityEnum,
    ) => {
      if (character.state == BattleCharacterStateEnum.skip) return false;

      const abilities = character.abilities;

      if (!abilities) return false;

      if (abilityName) {
        const ability = abilities[abilityName];

        if (!ability) {
          if (handlerError) {
            throw new NotFoundException(`Not found ability: "${abilityName}"`);
          } else {
            return;
          }
        }

        return (
          character.energy >= ability.energy &&
          ability.remaining == 0 &&
          ability.passive == false
        );
      } else {
        return Object.values(abilities).every((ability) => {
          return (
            character.energy >= ability.energy &&
            ability.remaining == 0 &&
            ability.passive == false
          );
        });
      }
    };

    if (characterId) {
      const character = await this.getCharacter(battle, characterId);

      return checkAbility(character, abilityName);
    }
    const characters = await this.getUserCharacters(battle, wallet);

    return characters.every((character) => {
      return checkAbility(character, abilityName);
    });
  }

  async isAvailableEvents(battleId: string, wallet?: string): Promise<Boolean>;
  async isAvailableEvents(
    battle: DocumentBattle,
    wallet?: string,
  ): Promise<Boolean>;

  async isAvailableEvents(
    _battle: string | DocumentBattle,
    wallet?: string,
    characterId?: string,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);
    if (!wallet) wallet = (await this.getStep(battle)).battleWallet;

    return (
      (await this.isAvailableAttack(battle, wallet, characterId)) &&
      (await this.isAvailableAbility(battle, wallet, characterId))
    );
  }

  async getAvailableEvents(_battle, wallet?: string, characterId?: string) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);
    if (!wallet) wallet = (await this.getStep(battle)).battleWallet;

    const events: { type: AttackTypeEnum | AbilityEnum }[] = [];

    if (
      await this.isAvailableAttack(
        battle,
        wallet,
        characterId,
        AttackTypeEnum.BaseAttack,
      )
    ) {
      events.push({ type: AttackTypeEnum.BaseAttack });
    }
    if (
      await this.isAvailableAttack(
        battle,
        wallet,
        characterId,
        AttackTypeEnum.HeavyAttack,
      )
    ) {
      events.push({ type: AttackTypeEnum.HeavyAttack });
    }

    const character = await this.getCharacter(battle, characterId, wallet);

    const abilities = character.abilities;
    if (abilities) {
      await Promise.all(
        Object.values(abilities).map((ability) => {
          return new Promise<void>(async (resolve, reject) => {
            try {
              if (
                await this.isAvailableAbility(
                  battle,
                  wallet,
                  characterId,
                  ability.name,
                )
              ) {
                events.push({ type: ability.name });
              }

              resolve();
            } catch (e) {
              reject(e);
            }
          });
        }),
      );
    }

    return events;
  }

  async isAvailableSkipUse(
    _battle: string | DocumentBattle,
    wallet: string,
    characterId: string,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);
    if (!wallet) wallet = (await this.getStep(battle)).battleWallet;

    const character = await this.getCharacter(battle, characterId, wallet);

    if (character.state == BattleCharacterStateEnum.skip) return false;

    return (
      character.health > 0 &&
      character.state == BattleCharacterStateEnum.default &&
      character.energy <
        this.env.BattleMaxEnergy - this.env.BattleRegenerationEnergy
    );
  }

  async isAvailableDefenceUse(
    _battle: string | DocumentBattle,
    wallet: string,
    characterId: string,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);
    if (!wallet) wallet = (await this.getStep(battle)).battleWallet;

    const character = await this.getCharacter(battle, characterId, wallet);

    if (character.state == BattleCharacterStateEnum.skip) return false;

    return (
      character.health > 0 &&
      character.energy >= 1 &&
      character.original.health * character.modArmor > 0
    );
  }

  async isAvailableAttackUse(
    _battle: string | DocumentBattle,
    wallet: string,
    characterId: string,
    targetId: string,
    typeAttack: AttackTypeEnum,
  ) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);
    if (!wallet) wallet = (await this.getStep(battle)).battleWallet;

    const character = await this.getCharacter(battle, characterId, wallet);

    if (character.state == BattleCharacterStateEnum.skip) return false;

    const target = await this.getCharacter(battle, targetId);

    const energy = character.energy;

    const weapon = character.weaponStats[typeAttack];
    return (
      character.health > 0 &&
      character.pet == false &&
      target.health > 0 &&
      target.pet == false &&
      character.userWallet != target.userWallet &&
      energy >= weapon.energy &&
      weapon.remaining == 0
    );
  }

  async isAvailableAbilityUse(
    _battle,
    wallet: string,
    abilityName: AbilityEnum,
    characterId: string,
    targetId?: string,
  ) {
    const { battle, battleId } = await this.overloadBattle(_battle);
    if (!wallet) wallet = (await this.getStep(battle)).battleWallet;

    const character = await this.getCharacter(battle, characterId, wallet);

    if (character.state == BattleCharacterStateEnum.skip) return false;

    const abilities = character.abilities;

    if (!abilities) return false;

    const ability = Object.values(abilities).find((ability) => {
      return ability.name == abilityName;
    });

    const targetEnergy = await (async () => {
      if (character.ownerId) {
        return this.getCharacter(battle, character.ownerId);
      } else {
        return character;
      }
    })();

    if (
      character.health > 0 &&
      targetEnergy.energy >= ability.energy &&
      ability.passive == false &&
      ability.remaining == 0
    ) {
      if (ability.target == true) {
        const target = await this.getCharacter(battle, targetId);

        if (target.health > 0) return 2;
      } else {
        if (!targetId || targetId == characterId) return 1;
      }
    }

    return 0;
  }

  async getAvailableEventsUse(_battle, wallet?: string, characterId?: string) {
    const { battle, battleId } = await this.overloadBattle(_battle);
    if (!wallet) wallet = (await this.getStep(battle)).battleWallet;

    const character = await this.getCharacter(battle, characterId, wallet);

    const events = await this.getAvailableEvents(battle, wallet, characterId);

    const eventsUse: {
      eventGroup: 'skip' | 'defence' | 'attack' | 'ability';
      eventType: AttackTypeEnum | BattleCharacterStateEnum | AbilityEnum;
      characterId: string;
      targetId?: string;
    }[] = [];

    if (
      (await this.isAvailableSkipUse(battle, wallet, characterId)) &&
      !character.pet
    ) {
      eventsUse.push({
        eventGroup: 'skip',
        eventType: BattleCharacterStateEnum.skip,
        characterId,
      });
    }

    if (
      (await this.isAvailableDefenceUse(battle, wallet, characterId)) &&
      !character.pet
    ) {
      eventsUse.push({
        eventGroup: 'defence',
        eventType: BattleCharacterStateEnum.defence,
        characterId,
      });
    }

    const targets = this.getCharacters(battle);

    for (const event of events) {
      const eventType = event.type;

      if (
        eventType == AttackTypeEnum.BaseAttack ||
        eventType == AttackTypeEnum.HeavyAttack
      ) {
        if (!character.pet) {
          for (const target of targets) {
            if (
              await this.isAvailableAttackUse(
                battle,
                wallet,
                characterId,
                target._id,
                eventType,
              )
            ) {
              eventsUse.push({
                eventGroup: 'attack',
                eventType,
                characterId,
                targetId: target._id,
              });
            }
          }
        }
      } else {
        const abilities = character.abilities;
        if (abilities) {
          const ability = abilities[eventType];

          if (ability.target) {
            for (const target of targets) {
              const availableAbility = await this.isAvailableAbilityUse(
                battle,
                wallet,
                eventType,
                characterId,
                target._id,
              );

              if (availableAbility) {
                eventsUse.push({
                  eventGroup: 'ability',
                  eventType,
                  characterId,
                  targetId: target._id,
                });
              }
            }
          } else {
            const availableAbility = await this.isAvailableAbilityUse(
              battle,
              wallet,
              eventType,
              characterId,
            );

            if (availableAbility) {
              eventsUse.push({ eventGroup: 'ability', eventType, characterId });
            }
          }
        }
      }
    }

    return eventsUse;
  }

  async getAvailableEventsUseUser(_battle, wallet?: string) {
    const { battle, battleId } = await this.overloadBattle(_battle);
    if (!wallet) wallet = (await this.getStep(battle)).battleWallet;

    const characters = await this.getUserCharacters(battle, wallet, true);

    const charactersEventsUse: //Record<
    //string,
    {
      eventGroup: 'skip' | 'defence' | 'attack' | 'ability';
      eventType: AttackTypeEnum | BattleCharacterStateEnum | AbilityEnum;
      characterId: string;
      targetId?: string;
    }[] = [];
    //> = {};

    for (const character of characters) {
      charactersEventsUse.push(
        ...(await this.getAvailableEventsUse(battle, wallet, character._id)),
      );
    }

    return charactersEventsUse;
  }

  async aiRandomEvent(_battle: string | DocumentBattle, wallet?: string) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);
    if (!wallet) wallet = (await this.getStep(battle)).battleWallet;

    const characterAvailableEvents = (
        await this.getAvailableEventsUseUser(battle, wallet)
      ).filter((event) => {
        return (
          event.eventGroup == 'attack' &&
          event.eventType == AttackTypeEnum.BaseAttack
        );
      });

    if (!characterAvailableEvents.length) return false;

    const event = getRandomArrayItem(characterAvailableEvents);

    if (event.eventGroup == 'skip') {
      await this.skip(battle, event.characterId, wallet);
      return true;
    }
    if (event.eventGroup == 'defence') {
      await this.defence(battle, event.characterId, wallet);
      return true;
    }
    if (event.eventGroup == 'attack') {
      await this.attack(
        battle,
        event.characterId,
        event.targetId,
        <AttackTypeEnum>event.eventType,
        wallet,
      );
      return true;
    }
    if (event.eventGroup == 'ability') {
      await this.abilityArchytype(
        wallet,
        battle,
        <AbilityEnum>event.eventType,
        event.characterId,
        event.targetId,
      );
      return true;
    }
  }

  async aiAutoRandomEvent(_battle: string | DocumentBattle, wallet?: string) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);
    if (!wallet) wallet = (await this.getStep(battle)).battleWallet;

    while (await this.aiRandomEvent(battle, wallet)) {}

    if (type == 'string')
      await this.battleModel.updateOne({ _id: battleId }, battle);

    return battle;
  }

  async autoAttack(battleId: string, wallet?: string): Promise<DocumentBattle>;
  async autoAttack(
    battle: DocumentBattle,
    wallet?: string,
  ): Promise<DocumentBattle>;

  async autoAttack(_battle: string | DocumentBattle, wallet?: string) {
    const { battle, battleId, type } = await this.overloadBattle(_battle);
    if (!wallet) wallet = (await this.getStep(battle)).battleWallet;

    const characters = await this.getUserCharacters(battle, wallet, true);
    for (const character of characters) {
      const events = await this.getAvailableEvents(
        battle,
        wallet,
        character._id,
      );
    }

    return battle;
  }

  async neutralStep(battleId: string): Promise<DocumentBattle>;
  async neutralStep(battle: DocumentBattle): Promise<DocumentBattle>;

  async neutralStep(_battle: string | DocumentBattle): Promise<DocumentBattle> {
    let { battle, battleId, type } = await this.overloadBattle(_battle);

    const wallet = Neutral;

    // Если сейчас ход противника
    if (await this.stepIs(battle, wallet)) {

      const battleUsers = battle.battleUsers;

      const battleUser = battleUsers[wallet];
      if (!battleUser) {
        return;
      }

      await this.aiAutoRandomEvent(battle, wallet);
    }

    return battle;
  }

  async endStep(
    battleId: string,
    wallet?: string,
  ): Promise<{ end: boolean; winnerWallet: string }>;
  async endStep(
    battle: DocumentBattle,
    wallet?: string,
  ): Promise<{ end: boolean; winnerWallet: string }>;

  async endStep(_battle: string | DocumentBattle, wallet?: string) {
    let { battle, battleId, type } = await this.overloadBattle(_battle);
    
    if (!battle) {
      console.log(`endStep: Not exists battle ${battleId}`);
      return;
    }

    const { step, stepCycle, battleWallet } = await this.getStep(battle);

    if (wallet && wallet != battleWallet)
      throw new ForbiddenException('Step wallet does not match user');

    this.refreshInaction(battleId);

    const lived = [];

    for (const userWallet of Object.keys(battle.battleUsers)) {
      if (await this.isUserLive(userWallet, battle)) {
        lived.push(userWallet);
      }
    }

    await this.battleModel.updateOne(
      { _id: battleId },
      { $set: { step: step + 1 } },
    );

    battle.step++;

    const {
      step: nextStep,
      stepCycle: nextStepCycle,
      battleWallet: nextBattleWallet,
    } = await this.getStep(battle);

    await this.tickAbilities(battle, nextBattleWallet);
    await this.tickBuffs(battle, nextBattleWallet);

    // Кто-то выиграл
    if (lived.length == 1) {
      const { winnerWallet } = await this.endBattle(battle, lived[0]);
      return { end: true, winnerWallet };
    }

    if (battle.step >= Object.keys(battle.battleUsers).length) {
      await this.userRegenerationEnergy(battle, nextBattleWallet);

      const characters = await this.getUserCharacters(battle, nextBattleWallet);

      for (const character of characters) {
        if (character.state == BattleCharacterStateEnum.skip) {
          character.energy++;
        }
      }
    }

    await this.userClearState(battle, nextBattleWallet);
    const nextBattleUser = battle.battleUsers[nextBattleWallet];

    if (nextBattleUser.autoBattle) {
      await this.aiAutoRandomEvent(battle, nextBattleWallet);

      return this.endStep(battle);
    }

    await this.trigger({
      battle,
      event: BattleEventsEnum.EndStep,
      value: { step, battleWallet },
    });

    await this.battleModel.updateOne({ _id: battleId }, battle);

    await this.battleNotification(
      battle,
      {
        type: NotificationsTypeEnum.battle,
        title: `empty`,
        data: {
          type: BattleEventsEnum.EndStep,
          battleId,

          step,
          stepCycle,
        },
      },
      'All',
    );

    return { end: false };
  }

  //#endregion
}
