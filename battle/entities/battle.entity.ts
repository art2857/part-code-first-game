import { getModelForClass, modelOptions, prop } from '@typegoose/typegoose';
import { BeAnObject } from '@typegoose/typegoose/lib/types';
import { Document } from 'mongoose';
import { BattleMainRule } from '../constants/battle-main-rule.type';
import { BattleStateEnum } from '../constants/battle-state.enum';
import { IBattleUser } from '../constants/battle-user.interface';
import { NotificationsTypeEnum } from '../../../notifications/notifications.constants';
import { Pos } from '../../../map/map.constants';
import { DATABASE_CONNECTION_TOKEN } from '../../../globals/database/constants/database-connection-token';

class BattleData {
  @prop({
    required: true,
    enum: [
      NotificationsTypeEnum.squadReadyCapture,
      NotificationsTypeEnum.squadReadyRob,
      NotificationsTypeEnum.empty,
    ],
  })
  type:
    | NotificationsTypeEnum.squadReadyCapture
    | NotificationsTypeEnum.squadReadyRob
    | NotificationsTypeEnum.empty;

  @prop({ required: true, type: Pos })
  position: Pos;
}

@modelOptions({ schemaOptions: { collection: 'battles', versionKey: false } })
export class BattleEntity {
  @prop({
    required: true,
    enum: Object.values(BattleStateEnum),
    default: () => BattleStateEnum.Battle,
  })
  state: BattleStateEnum;

  @prop({ required: false, type: String, default: () => null })
  winnerWallet: string;

  @prop({ required: true, type: Number, default: 0 })
  step: number;

  @prop({ required: true, type: Number, default: 0 })
  buffId: number;

  @prop({ required: true, type: [BattleMainRule], default: [], _id: false })
  rules: BattleMainRule[];

  @prop({ required: true, type: Object })
  battleUsers: Record<string, IBattleUser>;

  @prop({ required: false, type: BattleData, _id: false })
  data: BattleData;
}

export const BattleModel = getModelForClass(BattleEntity);

export type DocumentBattle = Document<any, BeAnObject, BattleEntity> &
  BattleEntity;

export const BattleModelProvider = {
  provide: 'BattleModel',
  useFactory: () => BattleModel,
  inject: [DATABASE_CONNECTION_TOKEN],
};
