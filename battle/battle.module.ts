import { forwardRef, Module } from '@nestjs/common';
import { BattleServiceController } from './battle-service/battle-controller.service';
import { EnvironmentModule } from '../../globals/environment/environment.module';
import { NotificationsLogicModule } from '../../notifications/logics/notifications-logic.module';
import { BattleControler } from './battle.controller';
import { MapLogicModule } from '../../map/logics/map-logic.module';
import { SquadModule } from '../squad/squad.module';
import { CharacterModule } from '../character/character.module';

@Module({
  imports: [
    NotificationsLogicModule,
    forwardRef(() => CharacterModule),
    forwardRef(() => SquadModule),
    forwardRef(() => MapLogicModule),
    EnvironmentModule,
  ],
  controllers: [BattleControler],
  providers: [BattleServiceController],
  exports: [BattleServiceController],
})
export class BattleModule {}
