import {
  applyDecorators,
  Body,
  Controller,
  Get,
  Post,
} from '@nestjs/common';
import { ApiBody, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Role } from '../../../../../admin/src/models/employee.model';
import { Auth, AuthBusy } from '../../../../auth/src/logics/auth.decorator';
import { User } from '../../../../auth/src/logics/user.decorator';
import { BattleServiceController } from './battle-service/battle-controller.service';

import {
  AbilityArchytypeDTO,
  AttackDTO,
  BattleTestDTO,
  DefenceDTO,
} from './battle.dto';
import { AttackTypeEnum } from './constants/attack-type.enum';
import { Neutral } from './constants/battle.constants';

@ApiTags('battle')
@Controller('battle')
export class BattleControler {
  constructor(private readonly battleService: BattleServiceController) {}

  @applyDecorators(
    ApiOperation({ summary: 'Тестовая схватка(только для тестов)' }),
    ApiBody({ type: BattleTestDTO }),
  )
  @Post('/test')
  @AuthBusy(Role.tester)
  async battleTest(
    @Body() { attacker, defensive, autoEndStep }: BattleTestDTO,
  ) {
    return this.battleService.battleTest(attacker, defensive, autoEndStep);
  }

  @applyDecorators(
    ApiOperation({ summary: 'Тестовая схватка - обучение' }),
    ApiBody({ type: BattleTestDTO }),
  )
  @Post('/learn')
  @AuthBusy()
  async battleLearn(
    @User('wallet') wallet: string,
    @Body() { attacker, defensive, autoEndStep }: BattleTestDTO,
  ) {
    attacker.wallet = wallet;
    // attacker.characters.forEach((character) => {
    //   character.level--;
    // });

    defensive.wallet = Neutral;
    // defensive.characters.forEach((character) => {
    //   character.level--;
    // });

    if (autoEndStep < 5 * 1000) {
      autoEndStep = 5 * 1000;
    }
    if (autoEndStep > 1 * 60 * 1000) {
      autoEndStep = 1 * 60 * 1000;
    }

    return this.battleService.battleTest(attacker, defensive, autoEndStep);
  }

  @applyDecorators(ApiOperation({ summary: 'Возвращает текущее сражение' }))
  @Get('/get')
  @Auth()
  async getBattle(@User('wallet') wallet: string) {
    return this.battleService.controllerBattle(
      await this.battleService.getCurrentBattle(wallet, false),
    );
  }

  @applyDecorators(ApiOperation({ summary: 'Возвращает все текущие сражения' }))
  @Get('/getBattles')
  @Auth()
  async getBattles(@User('wallet') wallet: string) {
    return (await this.battleService.getBattles(wallet)).map((battle) => {
      return this.battleService.controllerBattle(battle);
    });
  }

  @applyDecorators(ApiOperation({ summary: 'Возвращает историю сражений' }))
  @Get('/getHistory')
  @Auth()
  async getHistory(@User('wallet') wallet: string) {
    return (await this.battleService.getHistoryBattles(wallet)).map((battle) =>
      this.battleService.controllerBattle(battle),
    );
  }

  @applyDecorators(
    ApiOperation({ summary: 'Возвращает ход текущего пользователя в бою' }),
  )
  @Get('/getStep')
  @Auth()
  async getStep(@User('wallet') wallet: string) {
    return this.battleService.controllerGetStep(wallet);
  }

  @applyDecorators(ApiOperation({ summary: 'Возвращает все доступные ходы' }))
  @Get('/getAvailableEventsUseUser')
  @Auth()
  async getAvailableEventsUseUser(@User('wallet') wallet: string) {
    const battle = await this.battleService.getCurrentBattle(wallet, false);
    return this.battleService.getAvailableEventsUseUser(battle._id, wallet);
  }

  @applyDecorators(ApiOperation({ summary: 'Возвращает все доступные ходы' }))
  @Get('/getCountAvailableEventsUseUser')
  @Auth()
  async getCountAvailableEventsUseUser(@User('wallet') wallet: string) {
    const battle = await this.battleService.getCurrentBattle(wallet, false);
    return {
      count: (
        await this.battleService.getAvailableEventsUseUser(battle._id, wallet)
      )?.length,
    };
  }

  @applyDecorators(
    ApiOperation({
      summary: 'Автоматический ход игрока',
    }),
  )
  @Get('/autoStep')
  @Auth()
  async aiRandomEvent(@User('wallet') wallet: string) {
    const battle = await this.battleService.getCurrentBattle(wallet, false);
    await this.battleService.aiAutoRandomEvent(battle._id, wallet);
    return this.battleService.endStep(battle._id, wallet);
  }

  @applyDecorators(
    ApiOperation({ summary: 'Обычная атака' }),
    ApiBody({ type: AttackDTO }),
  )
  @Post('/baseAttack')
  @AuthBusy()
  async baseAttack(
    @User('wallet') wallet: string,
    @Body() { characterId, targetId }: AttackDTO,
  ) {
    const battle = await this.battleService.getCurrentBattle(wallet, false);
    const battleId = battle._id;
    return this.battleService.attack(
      battleId,
      characterId,
      targetId,
      AttackTypeEnum.BaseAttack,

      wallet,
      true,
    );
  }

  @applyDecorators(
    ApiOperation({ summary: 'Сильная атака' }),
    ApiBody({ type: AttackDTO }),
  )
  @Post('/heavyAttack')
  @AuthBusy()
  async heavyAttack(
    @User('wallet') wallet: string,
    @Body() { characterId, targetId }: AttackDTO,
  ) {
    const battle = await this.battleService.getCurrentBattle(wallet, false);
    const battleId = battle._id;
    return this.battleService.attack(
      battleId,
      characterId,
      targetId,
      AttackTypeEnum.HeavyAttack,

      wallet,
      true,
    );
  }

  @applyDecorators(
    ApiOperation({ summary: 'Принять стойку защиты' }),
    ApiBody({ type: DefenceDTO }),
  )
  @Post('/defence')
  @AuthBusy()
  async defence(
    @User('wallet') wallet: string,
    @Body() { characterId }: DefenceDTO,
  ) {
    const battle = await this.battleService.getCurrentBattle(wallet, false);
    const battleId = battle._id;
    return this.battleService.defence(battleId, characterId, wallet);
  }

  @applyDecorators(
    ApiOperation({ summary: 'Атаковать противника особой способностью' }),
    ApiBody({ type: AbilityArchytypeDTO }),
  )
  @Post('/useAbility')
  @AuthBusy()
  async abilityArchytype(
    @User('wallet') wallet: string,
    @Body() { characterId, targetId, ability }: AbilityArchytypeDTO,
  ) {
    const battle = await this.battleService.getCurrentBattle(wallet, false);
    const battleId = battle._id;
    return this.battleService.abilityArchytype(
      wallet,
      battleId,
      ability,
      characterId,
      targetId,
    );
  }

  @applyDecorators(
    ApiOperation({ summary: 'Пропустить ход' }),
    ApiBody({ type: DefenceDTO }),
  )
  @Post('/skip')
  @AuthBusy()
  async skip(
    @User('wallet') wallet: string,
    @Body() { characterId }: DefenceDTO,
  ) {
    const battle = await this.battleService.getCurrentBattle(wallet, false);
    const battleId = battle._id;
    return this.battleService.skip(battleId, characterId, wallet);
  }

  @applyDecorators(
    ApiOperation({ summary: 'Закончить ход' }),
    //ApiBody({ type: EndStepDTO }),
  )
  @Post('/endStep')
  @AuthBusy()
  async endStep(
    @User('wallet') wallet: string,
    //@Body() { battleId }: EndStepDTO,
  ) {
    const battle = await this.battleService.getCurrentBattle(wallet, false);
    const battleId = battle._id;
    return this.battleService.endStep(battleId, wallet);
  }

  @applyDecorators(
    ApiOperation({ summary: 'Закончить ход' }),
    //ApiBody({ type: EndStepDTO }),
  )
  @Post('/exitBattle')
  @AuthBusy()
  async exitBattle(
    @User('wallet') wallet: string,
    //@Body() { battleId }: EndStepDTO,
  ) {
    const battle = await this.battleService.getCurrentBattle(wallet, false);
    const battleId = battle._id;
    // const { battleWallet } = await this.battleService.getStep(battle);
    // if (wallet != battleWallet)
    //   throw new ForbiddenException(`Wallet != battleWalletStep`);

    return this.battleService.endBattle(battleId);
  }
}
