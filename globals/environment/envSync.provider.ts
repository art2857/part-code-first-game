export const EnvSync = 'EnvSync';

export const EnvSyncProvider = (sync: boolean = true) => ({
  provide: EnvSync,
  useValue: sync,
});
