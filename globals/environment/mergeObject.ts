import { Merge } from 'ts-essentials';

function typeOf(value: any) {
  if (value === null) return 'null';
  return value?.constructor?.name || typeof value;
}

/* Глубокое объединение объектов */
export function mergeObject<T extends Object, S extends Object>(
  target: T,
  source: S,
): Merge<T, S> {
  const sourceEntries = Object.entries(source);

  sourceEntries.forEach(([sourceKey, sourceValue]) => {
    const sourceType = typeOf(sourceValue);

    if (
      target[sourceKey] === undefined ||
      sourceType === 'undefined' ||
      sourceType === 'null' ||
      sourceType === 'Number' ||
      sourceType === 'String' ||
      sourceType !== typeOf(target[sourceKey])
    ) {
      target[sourceKey] = sourceValue;
    } else {
      mergeObject(target[sourceKey], sourceValue);
    }
  });

  return target as T & S;
}
