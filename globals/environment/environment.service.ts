import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { EnvironmentEntity, EnvironmentModel } from './environment.entity';
import { mergeObject } from './mergeObject';
import { EnvSync } from './envSync.provider';

const DefaultEnvironment: EnvironmentEntity = Object.freeze({
  /* Различные параметры */
});

@Injectable()
export class EnvironmentService implements OnModuleInit {
  environment: EnvironmentEntity = DefaultEnvironment;

  constructor(
    @Inject('EnvironmentModel')
    private environmentModel: typeof EnvironmentModel,
    @Inject(EnvSync) private envSync: boolean,
  ) {}

  /* Синхронизирует параметры с бд, если они отсутствуют, то устанавливает стандартные */
  async sync() {
    if ((await this.environmentModel.count({})) === 0) {
      await this.environmentModel.create(DefaultEnvironment);
    } else {
      const environment = await this.environmentModel.findOne({});

      this.environment = mergeObject(
        { ...DefaultEnvironment },
        environment.toObject(),
      );

      await this.environmentModel.updateOne({}, this.environment);
    }

    return this.environment;
  }

  async onModuleInit() {
    if (this.envSync) {
      await this.sync();

      setInterval(async () => {
        await this.sync();
      }, 15000);
    }
  }

  async get() {
    this.environment = await this.environmentModel.findOne({});
    return this.environment;
  }
}
