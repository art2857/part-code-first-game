import { DynamicModule, Global, Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { EnvironmentService } from './environment.service';
import { EnvSyncProvider } from './envSync.provider';

export function EnvironmentModuleRegister(sync: boolean) {
  return {
    imports: [DatabaseModule],
    providers: [EnvironmentService, EnvSyncProvider(sync)],
    exports: [EnvironmentService],
  };
}

@Global()
@Module({})
export class EnvironmentModule {
  static register(sync: boolean): DynamicModule {
    return Object.assign(
      { module: EnvironmentModule },
      EnvironmentModuleRegister(sync),
    );
  }
}
