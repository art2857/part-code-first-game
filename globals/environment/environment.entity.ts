import { getModelForClass, modelOptions, prop } from '@typegoose/typegoose';
import { DATABASE_CONNECTION_TOKEN } from '../database/constants/database-connection-token';

@modelOptions({
  schemaOptions: {
    collection: 'environment',
    versionKey: false,
    minimize: false,
  },
})
export class EnvironmentEntity {
  /* Различного рода параметры */
  //@prop({ type: Object }) Abilities: typeof Abilities;
}

export const EnvironmentModel = getModelForClass(EnvironmentEntity);

export const EnvironmentModelProvider = {
  provide: 'EnvironmentModel',
  useFactory: () => EnvironmentModel,
  inject: [DATABASE_CONNECTION_TOKEN],
};
