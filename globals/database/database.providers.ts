import { config } from '../../../../../libs/config';
import * as mongoose from 'mongoose';
import { Provider } from '@nestjs/common';
import { DATABASE_CONNECTION_TOKEN } from './constants/database-connection-token';

import { UserModelProvider } from '../../user/entities/user.entity';
import { ReliableAlarmModelProvider } from '@app/reliable-alarm';

config();

export const DatabaseURI = `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}${process.env.MONGO_OPTIONS}`;

export const MongoConnectionProvider: Provider = {
  provide: DATABASE_CONNECTION_TOKEN,
  useFactory: (): Promise<typeof mongoose> => mongoose.connect(DatabaseURI),
};

/* Собираем всех провайдеров бд */
export const MongoModelsProviders = [
  // UserModelProvider,
  // ReliableAlarmModelProvider,
];
