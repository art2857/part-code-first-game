import { Global, Module } from '@nestjs/common';
import {
  MongoConnectionProvider,
  MongoModelsProviders,
} from './database.providers';
import { DatabaseService } from './database.service';

@Global()
@Module({
  providers: [
    MongoConnectionProvider,
    DatabaseService,
    ...MongoModelsProviders,
  ],
  exports: [MongoConnectionProvider, DatabaseService, ...MongoModelsProviders],
})
export class DatabaseModule {}
