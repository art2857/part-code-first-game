import { ReliableAlarmModel } from '@app/reliable-alarm';
import { EmployeeModel } from 'admin/src/models/employee.model';
import { LauncherModel } from 'admin/src/models/launcher.model';
import { StatisticModel} from 'apps/cc-game-back/src/statistics/entities/statistics.entity';
import { TimeInGameModel } from 'apps/cc-game-back/src/statistics/time-in-game/time-in-game.entity';
import { BuffsModel } from '../../../buffs/entities/global-buffs.entity';
import { BattleModel } from '../../../csb/battle/entities/battle.entity';
import { CharacterModel } from '../../../csb/character/entities/character.entity';
import { SquadModel } from '../../../csb/squad/entities/squad.entity';
import { LoggingModel } from '../../../logging/logging.entity';
import { CellModel } from '../../../map/entities/cell.entity';
import { NotificationsModel } from '../../../notifications/entities/notifications.entity';
import { UserModel } from '../../../user/entities/user.entity';
import { WhitelistModel } from '../../../whitelist/whitelist.entity';
import { EnvironmentModel } from '../../environment/environment.entity';

export type ModelsType = {
  employee: typeof EmployeeModel;
  user: typeof UserModel;
  cell: typeof CellModel;
  buffs: typeof BuffsModel;
  character: typeof CharacterModel;
  squad: typeof SquadModel;
  battle: typeof BattleModel;
  notifications: typeof NotificationsModel;
  launcher: typeof LauncherModel;
  reliableAlarm: typeof ReliableAlarmModel;
  logging: typeof LoggingModel;
  whitelist: typeof WhitelistModel;
  environment: typeof EnvironmentModel;
  statistic: typeof StatisticModel;
  timeingame: typeof TimeInGameModel;
};
