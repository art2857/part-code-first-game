export enum SquadStateEnum {
  default = 'default',
  move = 'move',
  defence = 'defence',
  battle = 'battle',
}
