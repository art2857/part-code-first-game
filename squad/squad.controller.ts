import {
  applyDecorators,
  Body,
  Controller,
  Get,
  Post,
  Query,
} from '@nestjs/common';
import { ApiBody, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Role } from '../../../../../admin/src/models/employee.model';
import { Auth, AuthBusy } from '../../../../auth/src/logics/auth.decorator';
import { User } from '../../../../auth/src/logics/user.decorator';
import {
  AcceptBattleDTO,
  RefreshEnergyDTO,
  SquadCreateDTO,
  SquadMoveDTO,
  SquadStopDTO,
} from './squad.dto';
import { SquadService } from './squad.service';

@ApiTags('squad')
@Controller('squad')
export class SquadController {
  constructor(private readonly squadService: SquadService) {}

  @applyDecorators(
    ApiOperation({ summary: 'Создание группы' }),
    ApiBody({ type: SquadCreateDTO }),
  )
  @Post('create')
  @AuthBusy()
  async create(
    @User('wallet') wallet: string,
    @Body() { ids }: SquadCreateDTO,
  ) {
    return this.squadService.create(wallet, ids);
  }

  @applyDecorators(ApiOperation({ summary: 'Получение сведений о группе' }))
  @Get('get')
  @Auth()
  async get(@User('wallet') wallet: string, @Query('squadId') squadId: string) {
    return this.squadService.get(squadId, wallet);
  }

  @applyDecorators(
    ApiOperation({ summary: 'Получение всех групп пользователя' }),
  )
  @Get('getSquads')
  @Auth()
  async getSquads(@User('wallet') wallet: string) {
    return { squads: await this.squadService.getSquads(wallet) };
  }

  @applyDecorators(ApiOperation({ summary: 'Возвращает энергию группы' }))
  @Get('getEnergy')
  @Auth()
  async getEnergy(
    @User('wallet') wallet: string,
    @Query('squadId') squadId: string,
  ) {
    return { delay: await this.squadService.getEnergy(wallet, squadId) };
  }

  @applyDecorators(ApiOperation({ summary: 'Восстанавливает энергию группы' }))
  @Post('refreshEnergy')
  @Auth(Role.tester)
  async refreshEnergy(
    @User('wallet') wallet: string,
    @Body() { squadId }: RefreshEnergyDTO,
  ) {
    return this.squadService.changeEnergy(wallet, squadId, 1000, true);
  }

  @applyDecorators(
    ApiOperation({
      summary: 'Возвращает время последнего обновления энергии групп (мс)',
    }),
  )
  @Get('getLastChangeEnergy')
  @AuthBusy()
  async getLastChangeEnergy() {
    return this.squadService.getLastChangeEnergy();
  }

  @applyDecorators(
    ApiOperation({ summary: 'Перемещение группы по карте' }),
    ApiBody({ type: SquadMoveDTO }),
  )
  @Post('move')
  @AuthBusy()
  async move(
    @User('wallet') wallet: string,
    @Body() { squadId, x, y }: SquadMoveDTO,
  ) {
    return { delay: await this.squadService.move(squadId, { x, y }, wallet) };
  }

  @applyDecorators(
    ApiOperation({ summary: 'Захват группой ячейки' }),
    ApiBody({ type: SquadMoveDTO }),
  )
  @Post('capture')
  @AuthBusy()
  async capture(
    @User('wallet') wallet: string,
    @Body() { squadId, x, y }: SquadMoveDTO,
  ) {
    return {
      delay: await this.squadService.capture(squadId, { x, y }, wallet),
    };
  }

  @applyDecorators(
    ApiOperation({ summary: 'Грабёж группой ячейки' }),
    ApiBody({ type: SquadMoveDTO }),
  )
  @Post('rob')
  @AuthBusy()
  async rob(
    @User('wallet') wallet: string,
    @Body() { squadId, x, y }: SquadMoveDTO,
  ) {
    return { delay: await this.squadService.rob(squadId, { x, y }, wallet) };
  }

  @applyDecorators(
    ApiOperation({ summary: 'Защиты группой ячейки' }),
    ApiBody({ type: SquadMoveDTO }),
  )
  @Post('defence')
  @AuthBusy()
  async defence(
    @User('wallet') wallet: string,
    @Body() { squadId, x, y }: SquadMoveDTO,
  ) {
    return {
      delay: await this.squadService.defence(squadId, { x, y }, wallet),
    };
  }

  @applyDecorators(
    ApiOperation({ summary: 'Остановить текущий приказ группы' }),
    ApiBody({ type: SquadStopDTO }),
  )
  @Post('stop')
  @AuthBusy()
  async stop(
    @User('wallet') wallet: string,
    @Body() { squadId }: SquadStopDTO,
  ) {
    return this.squadService.stop(squadId, wallet);
  }

  @applyDecorators(
    ApiOperation({ summary: 'Принять сражение' }),
    ApiBody({ type: AcceptBattleDTO }),
  )
  @Post('acceptBattle')
  @AuthBusy()
  async acceptBattle(
    @User('wallet') wallet: string,
    @Body() { notificationId }: AcceptBattleDTO,
  ) {
    return this.squadService.acceptBattle(notificationId, wallet);
  }

  @applyDecorators(
    ApiOperation({
      summary:
        'Принять сражение на защиту ячейки(иначе за игрока бой будет идти автоматически)',
    }),
    ApiBody({ type: AcceptBattleDTO }),
  )
  @Post('acceptBattleDefence')
  @AuthBusy()
  async acceptBattleDefence(
    @User('wallet') wallet: string,
    @Body() { notificationId }: AcceptBattleDTO,
  ) {
    return this.squadService.acceptBattleDefence(notificationId, wallet);
  }
}
