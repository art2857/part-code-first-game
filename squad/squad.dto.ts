import { ApiProperty } from '@nestjs/swagger';

export class SquadCreateDTO {
  @ApiProperty({ type: Array<string> }) ids: Array<string>;
}

export class SquadGetDTO {
  @ApiProperty({ required: false }) squadId?: string;
}

export class SquadMoveDTO {
  @ApiProperty() squadId: string;
  @ApiProperty() x: number;
  @ApiProperty() y: number;
}

export class SquadStopDTO {
  @ApiProperty() squadId: string;
}

export class AcceptBattleDTO {
  @ApiProperty() notificationId: string;
}

export class RefreshEnergyDTO {
  @ApiProperty() squadId: string;
}
