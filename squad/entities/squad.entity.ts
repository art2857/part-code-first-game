import { getModelForClass, modelOptions, prop } from '@typegoose/typegoose';
import { BeAnObject } from '@typegoose/typegoose/lib/types';
import { DATABASE_CONNECTION_TOKEN } from '../../../globals/database/constants/database-connection-token';
import { Document } from 'mongoose';
import { Pos } from '../../../map/map.constants';
import { SquadStateEnum } from '../squad.constants';

@modelOptions({ schemaOptions: { collection: 'squads', versionKey: false } })
export class SquadEntity {
  @prop({ required: true, type: String })
  userWallet: string;

  @prop({ required: false, type: String })
  name?: string;

  @prop({ required: true, type: [String], default: () => [] })
  charactersIds: Array<string>;

  @prop({
    required: true,
    enum: Object.values(SquadStateEnum),
    default: SquadStateEnum.default,
  })
  state: SquadStateEnum;

  @prop({ required: true, type: Pos })
  position: Pos;

  @prop({ required: false, type: Pos })
  move?: Pos;
}

export const SquadModel = getModelForClass(SquadEntity);

export type DocumentSquad = Document<any, BeAnObject, SquadEntity> &
  SquadEntity;

export const SquadModelProvider = {
  provide: 'SquadModel',
  useFactory: () => SquadModel,
  inject: [DATABASE_CONNECTION_TOKEN],
};
