import { forwardRef, Module } from '@nestjs/common';
import { ReliableAlarmModule } from '../../../../../libs/reliable-alarm/src';
import { BattleModule } from '../../csb/battle/battle.module';
import { MapLogicModule } from '../../map/logics/map-logic.module';
import { NotificationsLogicModule } from '../../notifications/logics/notifications-logic.module';
import { CharacterModule } from '../character/character.module';
import { SquadController } from './squad.controller';
import { SquadService } from './squad.service';

@Module({
  imports: [
    ReliableAlarmModule,
    forwardRef(() => MapLogicModule),
    BattleModule,
    NotificationsLogicModule,
    forwardRef(() => CharacterModule),
  ],
  controllers: [SquadController],
  providers: [SquadService],
  exports: [SquadService],
})
export class SquadModule {}
