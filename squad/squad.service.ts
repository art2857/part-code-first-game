import {
  BadRequestException,
  ConflictException,
  ForbiddenException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { BeAnObject } from '@typegoose/typegoose/lib/types';
import {
  AlarmEnum,
  ReliableAlarmService,
} from '../../../../../libs/reliable-alarm/src';
import { Document, isValidObjectId } from 'mongoose';
import { Pos } from '../../map/map.constants';
import { MapLogicService } from '../../map/logics/map-logic.service';
import {
  NotificationsTypeEnum,
  NotificationTypeSendEnum,
} from '../../notifications/notifications.constants';
import { NotificationsLogicService } from '../../notifications/logics/notifications-logic.service';

import { SquadStateEnum } from './squad.constants';
import { BattleServiceController } from '../../csb/battle/battle-service/battle-controller.service';
import { EnvironmentService } from '../../globals/environment/environment.service';
import { EnvironmentEntity } from '../../globals/environment/environment.entity';
import { BattleModel } from '../../csb/battle/entities/battle.entity';
import { ArchetypeEnum } from '../character/constants/character/archetype.enum';
import { CharacterStateEnum } from '../character/constants/character/character-state.enum';
import { CharacterModel } from '../character/entities/character.entity';
import {
  SquadModel,
  DocumentSquad,
  SquadEntity,
} from './entities/squad.entity';
import { CharacterService } from '../character/character.service';
import { sentryLoggerError } from '../../../../../libs/sentry';

@Injectable()
export class SquadService {
  lastChangeEnergy: number = Date.parse(Date());

  env: EnvironmentEntity;
  constructor(
    @Inject('SquadModel')
    private squadModel: typeof SquadModel,
    @Inject('CharacterModel')
    private characterModel: typeof CharacterModel,
    @Inject('BattleModel')
    private battleModel: typeof BattleModel,
    @Inject(forwardRef(() => MapLogicService))
    private readonly mapService: MapLogicService,
    private readonly battleService: BattleServiceController,
    private readonly reliableAlarm: ReliableAlarmService,
    private readonly notificationsService: NotificationsLogicService,
    @Inject(forwardRef(() => CharacterService))
    private readonly characterService: CharacterService,
    private readonly environmentService: EnvironmentService,
  ) {
    //this.init();
    this.env = this.environmentService.environment;
  }

  init() {
    this.reliableAlarm.reliableAlarmLoad('squad', this);

    (async () => {
      if (
        !(await this.reliableAlarm.reliableAlarmFindOne({
          data: 'changeEnergy',
        }))
      ) {
        this.reliableAlarm.reliableSetAlarm({
          context: this,
          name: 'squad',
          type: AlarmEnum.interval,
          callFunction: `this.changeEnergyAll()
      .then(()=>{})`,
          duration: (60 * 60 * 1000) / this.env.MapRegenerationEnergy,
          data: 'changeEnergy',
        });
      }
    })();
  }

  async create(
    wallet: string,
    ids: Array<string>,
    pos: Pos = { x: 0, y: 0 },
    name?: string,
  ): Promise<DocumentSquad> {
    if (ids.length == 0) throw new BadRequestException(`Characters not listed`);

    const _ids = Array.from(new Set(ids.map((id) => id.toString())));
    const characters = [];

    console.log(`_ids`);
    console.log(_ids);

    _ids.forEach((id) => {
      characters.push(this.characterModel.findOne({ _id: id }));
    });

    return new Promise(async (resolve, reject) => {
      await Promise.all(characters)
        .then(async (characters) => {
          console.log('characters');
          console.log(characters);

          for (const character of characters) {
            console.log('character.state');
            console.log(character.state);

            if (character.state === CharacterStateEnum.available) {
              await this.characterModel.updateOne(
                { _id: character._id },
                { $set: { state: CharacterStateEnum.notavailable } },
              );
            } else {
              reject(
                new NotFoundException(
                  `Character not available id: ${character._id}`,
                ),
              );
              return;
            }
          }

          const squad = new SquadModel();
          squad.userWallet = wallet;
          squad.charactersIds = _ids;
          squad.position = pos;
          if (name) {
            squad.name = name;
          }

          for (const character of characters) {
            await this.characterModel.updateOne(
              { _id: character._id },
              { $set: { squadId: squad._id } },
            );
          }

          const squadSaved = squad.save();

          resolve(squadSaved);

          console.log('squad.save');
          console.log();
        })
        .catch((reason) => {
          sentryLoggerError(reason, `Character not exists`, `SquadService`);
          reject(new NotFoundException('Character not exists.', reason));
        });
    });
  }

  async createTurel(pos: Pos, level = 1) {
    const turel = await this.characterService.create(
      'neutral',
      ArchetypeEnum.Turel,
      level,
    );

    console.log(`createTurel`);
    console.log(turel);

    console.log(await this.characterModel.find({}));

    return <Promise<Document<any, BeAnObject, SquadEntity> & SquadEntity>>(
      this.create('neutral', [turel._id], pos, ArchetypeEnum.Turel)
    );
  }

  async remove(squadId: string, charactersDelete = false): Promise<any> {
    if (charactersDelete) {
      const squad = await this.squadModel.findOne({ _id: squadId });

      const ids = squad.charactersIds;

      ids.forEach(async (id) => {
        await this.characterModel.deleteOne({ _id: id });
      });
    }
    return this.squadModel.deleteOne({ _id: squadId });
  }

  async getSquads(wallet: string) {
    const squads = await this.squadModel.find({ userWallet: wallet });

    const returnSquads = [];

    for (const squad of squads) {
      const returnSquad = { ...squad.toObject() };
      returnSquad['characters'] = [];

      for (const characterId of squad.charactersIds) {
        returnSquad['characters'].push(
          await this.characterModel.findOne({ _id: characterId }),
        );
      }

      returnSquads.push(returnSquad);
    }

    return returnSquads;
  }

  async get(squadId?: string, wallet?: string) {
    if (squadId) {
      if (wallet) {
        const squad = await this.squadModel.findOne({
          _id: squadId,
          userWallet: wallet,
        });
        if (!squad) throw new NotFoundException(`Not found squad ${squadId}`);

        const returnSquad = { ...squad.toObject() };
        returnSquad['characters'] = [];

        for (const characterId of squad.charactersIds) {
          returnSquad['characters'].push(
            await this.characterModel.findOne({ _id: characterId }),
          );
        }

        return returnSquad;
      }

      const squad = this.squadModel.findOne({ _id: squadId });
      if (!squad) throw new NotFoundException(`Not found squad ${squadId}`);

      const returnSquad = { ...squad.toObject() };
      returnSquad['characters'] = [];

      for (const characterId of squad.charactersIds) {
        returnSquad['characters'].push(
          await this.characterModel.findOne({ _id: characterId }),
        );
      }

      return returnSquad;
    }
  }

  async getAverageLevel(squadId?: string) {
    const squad = await this.squadModel.findOne({ _id: squadId });
    const charactersIds = squad.charactersIds;

    let charactersCount = 0;
    let avgLvl = 0;

    for (const characterId of charactersIds) {
      const character = await this.characterModel.findOne({ _id: characterId });

      if (!character.ownerId) {
        charactersCount++;
        avgLvl += character.level;
      }
    }

    avgLvl /= charactersCount;

    return avgLvl;
  }

  async squadIsUser(squadId: string, wallet: string) {
    return this.squadModel.findOne({ _id: squadId, userWallet: wallet });
  }

  async setState(
    squadId: string,
    state: SquadStateEnum,
    wallet?: string,
  ): Promise<any> {
    if (wallet) {
      return this.squadModel.updateOne(
        { _id: squadId, wallet },
        { $set: { state } },
      );
    }
    return this.squadModel.updateOne({ _id: squadId }, { $set: { state } });
  }

  async getEnergy(wallet: string, squadId: string) {
    const squad = await this.squadModel.findOne({
      _id: squadId,
      userWallet: wallet,
    });
    const charactersIds = squad.charactersIds;

    let characterMinEnergy = this.env.MapMaxEnergy;
    for (const characterId of charactersIds) {
      const character = await this.characterModel.findOne({ _id: characterId });
      const energy = character.characterMap.energy;
      if (energy < characterMinEnergy) {
        characterMinEnergy = energy;
      }
    }

    return characterMinEnergy;
  }

  async changeEnergy(
    wallet: string,
    squadId: string,
    energy: number,
    set?: boolean,
  ): Promise<any> {
    const squad = await this.squadModel.findOne({
      _id: squadId,
      userWallet: wallet,
    });
    const charactersIds = squad.charactersIds;

    for (const characterId of charactersIds) {
      const character = await this.characterModel.findOne({ _id: characterId });

      if (set) {
        character.characterMap.energy = energy;
      } else {
        character.characterMap.energy += energy;
      }

      await this.characterModel.updateOne(
        { _id: characterId },
        {
          $set: {
            'characterMap.energy': Math.min(
              character.characterMap.energy,
              this.env.MapMaxEnergy,
            ),
          },
        },
      );
    }
    return true;
  }

  async changeEnergyAll() {
    const characters = await this.characterModel.find();

    for (const character of characters) {
      await this.characterModel.updateOne(
        { _id: character._id },
        {
          $set: {
            'characterMap.energy': Math.min(
              (character?.characterMap?.energy
                ? character?.characterMap?.energy
                : 0) + 1,
              this.env.MapMaxEnergy,
            ),
          },
        },
      );
    }

    this.lastChangeEnergy = Date.parse(Date());

    return true;
  }

  getLastChangeEnergy() {
    return this.lastChangeEnergy;
  }

  async alarmMove(squadId: string, wallet: string, pos: Pos) {
    const set = {};

    if (pos.x != undefined) {
      set['position.x'] = pos.x;
    }
    if (pos.y != undefined) {
      set['position.y'] = pos.y;
    }

    await this.squadModel.updateOne(
      {
        _id: squadId,
        userWallet: wallet,
      },
      {
        $set: set,
      },
    );

    return pos;
  }

  async move(squadId: string, pos: Pos, wallet?: string) {
    const { x, y } = pos;

    if (!isValidObjectId(squadId))
      throw new ConflictException(`Is invalid objectId squadId: "${squadId}"`);

    const squad = await this.squadModel.findOne({ _id: squadId });
    if (!squad) throw new NotFoundException(`Not fount squad id: ${squadId}`);
    if (squad.userWallet !== wallet)
      throw new ForbiddenException(`Squad not belongs you`);

    if (
      squad.state == SquadStateEnum.move ||
      squad.state == SquadStateEnum.battle
    )
      throw new ConflictException('Squad already busy');

    await this.clearReadyBattle(squadId, wallet);

    await this.squadModel.updateOne({ _id: squadId }, { move: pos });

    const distx = Math.abs(x - squad.position.x);
    const disty = Math.abs(y - squad.position.y);

    const dirx = Math.sign(x - squad.position.x);
    const diry = Math.sign(y - squad.position.y);

    const dist = distx + disty;

    await this.setState(squadId, SquadStateEnum.move, wallet);

    const movex = async (offset = 0) => {
      for (let stepx = 1; stepx <= distx; stepx++) {
        await this.reliableAlarm.reliableSetAlarm({
          context: this,
          name: 'squad',
          callFunction: `this.alarmMove('${squadId}', '${wallet}', { x: ${squad.position.x} + ${dirx} * ${stepx} })
          .then(()=>{})`,
          duration: (stepx + offset) * this.env.SquadTimestep,
          data: `squad:${squadId}`,
        });
      }
      return distx;
    };

    const movey = async (offset = 0) => {
      for (let stepy = 1; stepy <= disty; stepy++) {
        await this.reliableAlarm.reliableSetAlarm({
          context: this,
          name: 'squad',
          callFunction: `this.alarmMove('${squadId}', '${wallet}', { y: ${squad.position.y} + ${diry} * ${stepy} })
          .then(()=>{})`,
          duration: (stepy + offset) * this.env.SquadTimestep,
          data: `squad:${squadId}`,
        });
      }
      return disty;
    };

    if (distx > disty) {
      movey(await movex());
    } else {
      movex(await movey());
    }

    await this.reliableAlarm.reliableSetAlarm({
      context: this,
      name: 'squad',
      callFunction: `await this.setState("${squadId}","${SquadStateEnum.default}","${wallet}")`,
      duration: dist * this.env.SquadTimestep,
      data: `squad:${squadId}`,
    });

    return dist;
  }

  async stop(squadId: string, wallet?: string) {
    const squad = await this.squadIsUser(squadId, wallet);
    if (!squad) throw new NotFoundException('Not fount squad');

    const alarms = await this.reliableAlarm.reliableAlarmFindMany({
      data: `squad:${squadId}`,
    });

    const promises = [];
    for (const alarm of alarms) {
      promises.push(this.reliableAlarm.reliableClearAlarm(alarm._id));
    }

    return Promise.all([
      ...promises,
      this.squadModel.updateOne(
        { _id: squadId, userWallet: wallet },
        { $set: { state: SquadStateEnum.default, move: {} } },
      ),
    ]);
  }

  async alarmSquadReadyBattle(
    squadId: string,
    wallet: string,
    pos: Pos,
    type:
      | NotificationsTypeEnum.squadReadyCapture
      | NotificationsTypeEnum.squadReadyRob,
  ) {
    const cell = await this.mapService.getCell(pos);

    return this.notificationsService.send({
      typeSend: NotificationTypeSendEnum.Self,
      wallet,
      save: true,
      message: {
        type,
        title: `Squad ready attack`,
        data: {
          squadId,
          wallet,
          pos,
          tier: cell.tier,
        },
      },
    });
  }

  async clearReadyBattle(squadId: string, wallet: string) {
    await this.notificationsService.remove(
      {
        userWallet: wallet,
        'data.squadId': squadId,
      },
      NotificationTypeSendEnum.Self,
      wallet,
    );

    await this.squadModel.updateOne(
      { _id: squadId, userWallet: wallet },
      { $set: { move: {} } },
    );

    return true;
  }

  // Захват
  async capture(squadId: string, pos: Pos, wallet?: string) {
    const cell = await this.mapService.getCell(pos);

    if (cell.userWallet == wallet)
      throw new ConflictException(`You can't take over your cage`);

    const dist = await this.move(squadId, pos, wallet);

    if (dist <= this.env.SquadDistanceInstantEvent) {
      await this.alarmSquadReadyBattle(
        squadId,
        wallet,
        pos,
        NotificationsTypeEnum.squadReadyCapture,
      );
      return 0;
    } else {
      await this.reliableAlarm.reliableSetAlarm({
        context: this,
        name: 'squad',
        callFunction: `this.alarmSquadReadyBattle("${squadId}", "${wallet}", ${JSON.stringify(
          pos,
        )}, "squadReadyCapture").then(()=>{})`,
        duration: dist * this.env.SquadTimestep,
        data: `squad:${squadId}`,
      });

      return dist * this.env.SquadTimestep;
    }
  }

  // Грабёж
  async rob(squadId: string, pos: Pos, wallet?: string) {
    const cell = await this.mapService.getCell(pos);

    if (cell.userWallet == wallet)
      throw new ConflictException(`You can't rob your cell`);

    const dist = await this.move(squadId, pos, wallet);

    if (dist <= this.env.SquadDistanceInstantEvent) {
      await this.alarmSquadReadyBattle(
        squadId,
        wallet,
        pos,
        NotificationsTypeEnum.squadReadyRob,
      );
      return 0;
    } else {
      await this.reliableAlarm.reliableSetAlarm({
        context: this,
        name: 'squad',
        callFunction: `this.alarmSquadReadyBattle("${squadId}", "${wallet}", ${JSON.stringify(
          pos,
        )}, "squadReadyRob").then(()=>{})`,
        duration: dist * this.env.SquadTimestep,
        data: `squad:${squadId}`,
      });

      return dist * this.env.SquadTimestep;
    }
  }

  async defence(squadId: string, pos: Pos, wallet?: string) {
    const dist = await this.move(squadId, pos, wallet);

    await this.reliableAlarm.reliableSetAlarm({
      context: this,
      name: 'squad',
      callFunction: `await this.setState("${squadId}","${SquadStateEnum.defence}","${wallet}")`,
      duration: dist * this.env.SquadTimestep,
      data: `squad:${squadId}`,
    });

    return dist;
  }

  async getSquadStatePos(
    pos: Pos,
    state: SquadStateEnum,
    wallet?: string | Object,
  ) {
    const options: {
      'position.x': number;
      'position.y': number;
      userWallet?: string | Object;
      state: SquadStateEnum;
    } = {
      'position.x': pos.x,
      'position.y': pos.y,
      state,
    };

    if (wallet != undefined) {
      options.userWallet = wallet;
    }

    return this.squadModel.findOne(options);
  }

  async acceptBattle(notificationId: string, wallet: string) {
    const notification = await this.notificationsService.findOne({
      _id: notificationId,
      userWallet: wallet,
    });

    if (!notification)
      throw new NotFoundException(
        `Not exists notification id: "${notificationId}"`,
      );

    if (await this.battleService.getCurrentBattle(wallet, false, false))
      throw new ConflictException(`You are already in a battle`);

    if (
      notification.type != NotificationsTypeEnum.squadReadyCapture &&
      notification.type != NotificationsTypeEnum.squadReadyRob
    )
      throw new ConflictException(`Invalid notification type`);

    const data = <{ pos: Pos; tier: number; squadId: string; wallet: string }>(
      notification.data
    );

    const cost =
      notification.type == NotificationsTypeEnum.squadReadyCapture
        ? this.env.MapCaptureEnergy
        : this.env.MapRobEnergy;

    const energy = await this.getEnergy(data.wallet, data.squadId);
    if (energy < cost)
      throw new ConflictException(`Not enough squad energy. Required ${cost}`);

    await this.changeEnergy(wallet, data.squadId, -cost);

    if (wallet != data.wallet)
      throw new ForbiddenException('Squad is not belongs your');

    if (await this.getSquadStatePos(data.pos, SquadStateEnum.battle))
      throw new ConflictException(`There is already a battle on this cell`);

    let enemySquad: DocumentSquad = await this.getSquadStatePos(
      data.pos,
      SquadStateEnum.defence,
      {
        $ne: `${data.wallet}`,
      },
    );
    let enemySquadId: string = enemySquad?._id;

    let neutralEnemy = false;
    if (!enemySquadId) {
      neutralEnemy = true;

      const minLvl = (data.tier - 1) * 5 + 1;
      const maxLvl = data.tier * 5 + 1;

      const squadAvgLvl = await this.getAverageLevel(data.squadId);
      const level = Math.min(Math.max(Math.round(squadAvgLvl), minLvl), maxLvl);

      const enemy = await this.createTurel(
        data.pos,
        level,
      );
      enemySquad = enemy;
      enemySquadId = enemy._id;
    }

    await this.squadModel.updateMany(
      { $or: [{ _id: data.squadId }, { _id: enemySquadId }] },
      { $set: { state: SquadStateEnum.battle } },
    );

    const battle = await this.battleService.create(
      [
        { _id: data.squadId, autoBattle: false },
        { _id: enemySquadId, autoBattle: true },
      ],
      notification.type,
      data.pos,
    );

    await this.clearReadyBattle(data.squadId, data.wallet);

    if (neutralEnemy) {
      await this.remove(enemySquadId, true);
    } else {
      if (enemySquad) {
        await this.notificationsService.send({
          typeSend: NotificationTypeSendEnum.Self,
          wallet: enemySquad.userWallet,
          save: true,
          message: {
            type: {
              [NotificationsTypeEnum.squadReadyCapture]:
                NotificationsTypeEnum.squadDefenceCapture,
              [NotificationsTypeEnum.squadReadyRob]:
                NotificationsTypeEnum.squadDefenceRob,
            }[notification.type],
            data: {
              battleId: battle._id,
              squadId: enemySquad._id,
              wallet: enemySquad.userWallet,
              pos: data.pos,
              tier: data.tier,
            },
          },
        });
      } else {
        console.log(`!enemySquad ${enemySquadId}`);
      }
    }

    return this.battleService.controllerBattle(battle);
  }

  async acceptBattleDefence(notificationId: string, wallet: string) {
    const notification = await this.notificationsService.findOne({
      _id: notificationId,
      userWallet: wallet,
    });

    if (!notification)
      throw new NotFoundException(
        `Not exists notification id: "${notificationId}"`,
      );

    if (await this.battleService.getCurrentBattle(wallet, false, false))
      throw new ConflictException(`You are already in a battle`);

    if (
      notification.type != NotificationsTypeEnum.squadDefenceCapture &&
      notification.type != NotificationsTypeEnum.squadDefenceRob
    )
      throw new ConflictException(`Invalid notification type`);

    const data = <
      {
        battleId: string;
        squadId: string;
        wallet: string;
        pos: Pos;
        tier: number;
      }
      >notification.data;

    const battle = await this.battleService.getBattle(data.battleId);

    battle.battleUsers[wallet].autoBattle = false;

    await this.battleModel.updateOne({ _id: battle._id }, battle);

    return this.battleService.controllerBattle(battle);
  }
}
